/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2000-2010 University of Utah and the Flux Group.
 * All rights reserved.
 */

/*
 * Log defs.
 */
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

int	ps_loginit(int usesyslog, char *name);
void	ps_logsyslog(void);
void	ps_logflush(void);
void	ps_info(const char *fmt, ...);
void	ps_warning(const char *fmt, ...);
void	ps_error(const char *fmt, ...);
void	ps_errorc(const char *fmt, ...);
void	ps_fatal(const char *fmt, ...) __attribute__((noreturn));
void	ps_pwarning(const char *fmt, ...);
void	ps_pfatal(const char *fmt, ...) __attribute__((noreturn));

#ifdef __cplusplus
}
#endif
