#! /bin/sh

PINGERS="g1 g2 g3"
AGENT_COUNT=5

trap 'kill 0' EXIT

./pubsubd &

for pg in $PINGERS; do
    echo "Starting pinger: $pg"
    ./pinger -d $pg &
    i=0
    while test $i -lt $AGENT_COUNT; do
	./pingagent $pg &
	i=`expr $i + 1`
    done
done

wait
