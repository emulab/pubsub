/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2005-2022 University of Utah and the Flux Group.
 * All rights reserved.
 */
#include <sys/types.h>
#include <sys/time.h>
#include <limits.h>
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#ifdef THREADED
#include <pthread.h>
#include <signal.h> /* for pthread_kill */
#endif
#include "pubsub.h"
#include "network.h"
#include "clientapi.h"
#include "log.h"

int dis = 0;

/*
 * TODO: Wrap various globals in locks for the threaded version of the library.
 */

int		pubsub_debug         = PUBSUB_DEBUG_RECONNECT;
static int	subscription_counter = 1;
static int      sequence_number      = 1;

#ifdef THREADED
/*
 * In the threaded version we keep a private data structure inside the
 * library to avoid header problems in client programs.
 */
typedef struct thread_private {
	pthread_t		mainloop_pid;
	pthread_t		dispatch_pid;
	pthread_t		reconnect_pid;
	int			readpipe_fd;
	int			writepipe_fd;
	pthread_mutex_t		lock;		/* global lock */
	pthread_cond_t		sockcond;
	pthread_cond_t		replycond;
	queue_head_t		packets;	/* Linked list */
} thread_private_t;

/* The internal loopers */
static void *threaded_mainloop(void *arg);
static void *threaded_dispatcher(void *arg);
static void *threaded_reconnect(void *arg);

/*
 * The mainloop needs to be interrupted when changing things. Use a pipe.
 */
static void dispatcher_interrupt(pubsub_handle_t *handle);

static int
pubsub_packet_dequeue(pubsub_handle_t *handle, pubsub_error_t *myerror);

/*
 * Simple global lock for the threaded version of the library.
 */
#define LOCK(handle)	\
	if (handle->priv) { pthread_mutex_lock(&handle->priv->lock); }
#define UNLOCK(handle)  \
	if (handle->priv) { pthread_mutex_unlock(&handle->priv->lock); }
#define DISPATCHER_INTERRUPT(handle) dispatcher_interrupt(handle)

int pubsub_is_threaded[1]; // Marker to tell threaded vs. nonthreaded libs.
#else
#define LOCK(handle)
#define UNLOCK(handle)
#define DISPATCHER_INTERRUPT(handle)
#endif

/* protos */
static int
pubsub_dispatcher(pubsub_handle_t *handle,
		  int blocking, pubsub_error_t *myerror);

static int
pubsub_notification_dispatch(pubsub_handle_t *handle,
			     notification_packet_t *packet,
			     pubsub_error_t *error);
static int
pubsub_iohandler_dispatch(pubsub_handle_t *handle,
			  int fd, pubsub_error_t *error);
static int
pubsub_timeout_dispatch(pubsub_handle_t *handle, pubsub_error_t *error);
static int
pubsub_send_subscription(pubsub_handle_t *handle,
			 pubsub_subscription_t *subscription,
			 int blocking, int toplevel, pubsub_error_t *error);
static int
pubsub_send(pubsub_handle_t *handle, int blocking, int toplevel,
	    packet_t *, reply_packet_t *, pubsub_error_t *error);
static int
pubsub_send_packet(pubsub_handle_t *handle, packet_t *packet, int toplevel,
		   struct timeval *timesent, pubsub_error_t *myerror);
static int
pubsub_readpacket(pubsub_handle_t *handle, pubsub_error_t *myerror);

static int
idle_timeout_callback(pubsub_handle_t *handle,
		      pubsub_timeout_t *timeout,
		      void *data, pubsub_error_t *error);

/*
 * Initialize and Cleanup.
 * XXX nothing to do right now.
 */
int
pubsub_init(void)
{
	return 0;
}
int
pubsub_cleanup(void)
{
	return 0;
}

/*
 * pubsub_connect can take a handle that is preallocated, or it can
 * create one as needed. Some settings (idle, retry) need the handle,
 * but most of the time there is no need for the extra step.
 */
int
pubsub_alloc_handle(pubsub_handle_t **handlep)
{
	pubsub_handle_t *handle;

	if ((handle =
	     (pubsub_handle_t *) calloc(1, sizeof(pubsub_handle_t))) == NULL) {
		ps_error("pubsub_alloc_handle: Out of memory\n");
		return -1;
	}
	handle->sock        = -1;
	handle->isssl       = 0;
	handle->max_retries = INT_MAX;
	handle->last_status = PUBSUB_STATUS_CONNECTION_NOSTATUS;
	handle->next_status = PUBSUB_STATUS_CONNECTION_FOUND;
	handle->this_status = PUBSUB_STATUS_CONNECTION_FOUND;
	handle->failover    = 1;
	handle->send_count  = 0;
	handle->sub_count   = 0;
	queue_init(&handle->subscriptions);
	queue_init(&handle->iohandlers);
	queue_init(&handle->reply_packets);
	queue_init(&handle->timeouts);

	*handlep = handle;
	return 0;
}

/*
 * Undo all the state associated with a handle, optionally freeing the
 * handle itself.  Called from connect to teardown partially setup handles.
 */
static void
teardown_handle(pubsub_handle_t *handle, int freehandle)
{
	if (handle->sock >= 0) {
		ps_ClientDisconnect(handle->sock);
		handle->sock = -1;
		if (handle->server) {
			free(handle->server);
			handle->server = 0;
		}
	}
#ifdef  THREADED
	if (handle->priv) {
		void *ignored;

		if (handle->priv->mainloop_pid)
			pthread_cancel(handle->priv->mainloop_pid);
		if (handle->priv->dispatch_pid)
			pthread_cancel(handle->priv->dispatch_pid);
		if (handle->priv->reconnect_pid)
			pthread_cancel(handle->priv->reconnect_pid);
		if (handle->priv->mainloop_pid)
			pthread_join(handle->priv->mainloop_pid, &ignored);
		if (handle->priv->dispatch_pid)
			pthread_join(handle->priv->dispatch_pid, &ignored);
		if (handle->priv->reconnect_pid)
			pthread_join(handle->priv->reconnect_pid, &ignored);
		pthread_cond_destroy(&handle->priv->sockcond);
		pthread_cond_destroy(&handle->priv->replycond);
		pthread_mutex_destroy(&handle->priv->lock);

		if (handle->priv->readpipe_fd >= 0)
			close(handle->priv->readpipe_fd);
		if (handle->priv->writepipe_fd >= 0)
			close(handle->priv->writepipe_fd);
		free(handle->priv);
	}
#endif
	if (freehandle)
		free(handle);
}

/*
 * Initial connection to server.
 */
int
pubsub_connect_internal(char *server, int portnum, int usessl,
			pubsub_handle_t **handlep)
{
	int		sock, didalloc = 0;
	pubsub_handle_t *handle = *handlep;

	if (handle && handle->sock >= 0) {
		ps_error("pubsub_connect: already connected\n");
		return -1;
	}

	if (handle == NULL) {
		if (pubsub_alloc_handle(&handle) != 0) {
			ps_error("pubsub_connect: could not allocate handle\n");
			return -1;
		}
		didalloc = 1;
	}

	/*
	 * 
	 */
	if (ps_ClientConnect(server, portnum, usessl,
			     handle->max_retries, &sock, &handle->error) < 0) {
		ps_error("pubsub_connect: Could not connect to %s:%d\n",
			 server, portnum);
		teardown_handle(handle, didalloc);
		return -1;
	}
	if (pubsub_debug & PUBSUB_DEBUG_CONNECT) {
		int localport = ps_ClientPort(sock);
		ps_info("Connected to server %s lport=%d,rport=%d on sock %d\n",
			server,localport, portnum, sock);
		ps_logflush();
	}
	handle->sock        = sock;
	handle->isssl       = usessl;
	handle->maxfds      = sock + 1;
	handle->server      = strdup(server);
	handle->portnum     = portnum;

	FD_ZERO(&handle->sfds);
#ifndef THREADED
	FD_SET(handle->sock, &handle->sfds);
#else
	handle->priv = (thread_private_t *)calloc(1, sizeof(thread_private_t));
	if (handle->priv == NULL) {
		ps_error("pubsub_connect: Out of memory\n");
		teardown_handle(handle, didalloc);
		return -1;
	}
	handle->priv->readpipe_fd = handle->priv->writepipe_fd = -1;
	pthread_mutex_init(&handle->priv->lock, NULL);
	pthread_cond_init(&handle->priv->sockcond, NULL);
	pthread_cond_init(&handle->priv->replycond, NULL);
	queue_init(&handle->priv->packets);

	/*
	 * We have to be able to interrupt the mainloop when new things
	 * happen (timeouts, iohandlers, etc). Rather then uses signals
	 * which could interfere with the client program, open up a pipe
	 * to ourself and write to it, which will cause the select down
	 * in the mainloop to break out.
	 */
	{
		int fds[2];

		if (pipe(fds) < 0) {
			ps_error("pubsub_connect: pipe failed!\n");
			teardown_handle(handle, didalloc);
			return -1;
		}
		handle->priv->readpipe_fd  = fds[0];
		handle->priv->writepipe_fd = fds[1];
		FD_SET(handle->priv->readpipe_fd, &handle->sfds);
		if (handle->priv->readpipe_fd >= handle->maxfds)
			handle->maxfds = handle->priv->readpipe_fd + 1;
	}
	/*
	 * The dispatcher is a thread that only reads the packets in; they
	 * are dispatched in their own thread (below).
	 */
	if (pthread_create(&handle->priv->dispatch_pid, NULL,
			   threaded_dispatcher, (void *)handle)) {
		ps_error("pubsub_connect: "
			 "Failed to create dispatcher pthread!\n");
		teardown_handle(handle, didalloc);
		return -1;
	}
	
	/*
	 * Notifications are dispatched via their own thread. This replaces
	 * the mainloop used in the non-threaded version.
	 */
	if (pthread_create(&handle->priv->mainloop_pid, NULL,
			   threaded_mainloop, (void *)handle)) {
		ps_error("pubsub_connect: "
			 "Failed to create mainloop pthread!\n");
		teardown_handle(handle, didalloc);
		return -1;
	}

	/*
	 * Thread to keep the connection alive.
	 */
	if (pthread_create(&handle->priv->reconnect_pid, NULL,
			   threaded_reconnect, (void *)handle)) {
		ps_error("pubsub_connect: "
			 "Failed to create reconnect_pid pthread!\n");
		teardown_handle(handle, didalloc);
		return -1;
	}
#endif
	*handlep = handle;
	return 0;
}
int
pubsub_connect(char *server, int portnum, pubsub_handle_t **handlep)
{
	return pubsub_connect_internal(server, portnum, 0, handlep);
}
int
pubsub_sslconnect(char *server, int portnum, pubsub_handle_t **handlep)
{
	return pubsub_connect_internal(server, portnum, 1, handlep);
}

/*
 * Drop the connection, as for reconnect later.
 *
 * Called with lock taken.
 */
static void
pubsub_dropconnect(pubsub_handle_t *handle, pubsub_error_t *myerror)
{
	reply_packet_t		*pktp;

	if (handle->sock >= 0) {
		if ((pubsub_debug & PUBSUB_DEBUG_CONNECT) && myerror) {
			ps_error("Server %s:%d disconnect:%d port=%d errno=%d\n",
				 handle->server, handle->portnum,
				 handle->sock, ps_ClientPort(handle->sock),
				 myerror->error);
		}
		ps_ClientDisconnect(handle->sock);
		FD_CLR(handle->sock, &handle->sfds);
		handle->sock = -1;
		handle->this_status = PUBSUB_STATUS_CONNECTION_LOST;
		handle->next_status = PUBSUB_STATUS_CONNECTION_LOST;

		/*
		 * Abort anyone waiting for a reply.
		 */
		queue_iterate(&handle->reply_packets,
			      pktp, reply_packet_t *, chain) {
			pktp->header.error = ECONNRESET;
			pktp->aborted      = 1;
		}
#ifdef  THREADED
		pthread_cond_broadcast(&handle->priv->replycond);
#endif
	}
}

/*
 * Look for any overdue reply packets. If we find one,
 * abort the connection (which will abort all outstanding
 * packets
 */
static int
check_overdue(pubsub_handle_t *handle)
{
	int		overdue = 0;
	reply_packet_t	*pktp;
	struct timeval  now;

	gettimeofday(&now, NULL);
	queue_iterate(&handle->reply_packets, pktp, reply_packet_t *, chain) {
		if (!pktp->reply_received && !pktp->aborted &&
		    pktp->timesent.tv_sec > 0 &&
		    (now.tv_sec - pktp->timesent.tv_sec > 5)) {
			if (pubsub_debug & PUBSUB_DEBUG_OVERDUE) {
				ps_info("overdue(m): %d %p %p\n",
				     ntohl(pktp->sequence),
				     pktp->callback, pktp->pktp);
			}
			overdue++;
		}
	}
	return overdue;
}

/*
 * Disconnect from server.
 */
int
pubsub_disconnect(pubsub_handle_t *handle)
{
	LOCK(handle);
	pubsub_dropconnect(handle, NULL);
	UNLOCK(handle);
	
	teardown_handle(handle, 1);
	return 0;
}

/*
 * Try to reconnect to the server. How long should we try? I think our
 * current set of clients are happy to have it try forever?
 */
static int
pubsub_reconnect(pubsub_handle_t *handle, pubsub_error_t *myerror)
{
	pubsub_subscription_t	*subscription;
	int			sock;
	reply_packet_t		*pktp;

	if ((pubsub_debug & PUBSUB_DEBUG_RECONNECT) && handle->sock < 0) {
		ps_info("attempting reconnect to %s:%d\n",
		     handle->server, handle->portnum);
		ps_logflush();
	}

	if (handle->sock >= 0) {
		pubsub_dropconnect(handle, myerror);
		return 0;
	}

	/*
	 * When not doing "failover" (an elvin term), we do not try to
	 * reconnect but let the caller deal with it.
	 */
	if (! handle->failover) {
		myerror->msg   = "pubsub_reconnect: no reconnect attempted";
		myerror->error = ENOTCONN;
		return -1;
	}
	
	/*
	 * Try once without retry, and return to caller so that the main
	 * loop (or thread) can keep processing timeouts and iohandlers.
	 */
	if (ps_ClientConnect(handle->server, handle->portnum,
			     handle->isssl, 1, &sock, myerror) < 0) {
		
		if (pubsub_debug & PUBSUB_DEBUG_RECONNECT) {
			pubsub_error_fprintf(stderr, myerror);
			ps_logflush();
		}
		
		/*
		 * An ECONNREFUSED or ETIMEDOUT error is okay, we will keep
		 * trying again.  Other errors are reported back.
		 */
		if (myerror->error == ECONNREFUSED ||
		    myerror->error == ETIMEDOUT) {
			return 0;
		}
		return -1;
	}
	if (pubsub_debug & PUBSUB_DEBUG_RECONNECT) {
		int localport = ps_ClientPort(sock);
		ps_info("reconnected to %s lport=%d, rport=%d on sock %d\n",
			handle->server, localport, handle->portnum, sock);
		ps_logflush();
	}
	
	handle->sock = sock;
#ifndef	THREADED
	FD_SET(sock, &handle->sfds);
	if (sock >= handle->maxfds)
		handle->maxfds = sock + 1;
#endif
#ifdef  THREADED
	pthread_cond_broadcast(&handle->priv->sockcond);
#endif

	/*
	 * Have to watch for a connection breakdown while trying to restore
	 * a connection. Just return and let the previous reconnect finish.
	 */
	if (handle->reconnecting) {
		return 0;
	}
	handle->reconnecting = 1;
	
	/*
	 * Restore our subscriptions
	 */
	queue_iterate(&handle->subscriptions,
		      subscription, pubsub_subscription_t *, chain) {
		if (!subscription->busy &&
		    pubsub_send_subscription(handle, subscription,
					     1, 0, myerror) < 0) {
			ps_error("Failed to readd subscription!\n");
			goto bad;
		}
	}

	/*
	 * Look for any async packets that need to be resent. Sync packets
	 * will be resent after we leave here and the aborted packets are
	 * noticed in pubsub_send() below.
	 */
	queue_iterate(&handle->reply_packets,
		      pktp, reply_packet_t *, chain) {
		if (pktp->aborted && pktp->pktp) {
			pktp->header.error = 0;
			pktp->aborted = 0;
			if (pubsub_send_packet(handle,
					       (packet_t *) pktp->pktp, 0,
					       &pktp->timesent,
					       myerror) < 0) {
				ps_error("Failed to resend packets!\n");
				goto bad;
			}
		}
	}
	
	handle->this_status  = PUBSUB_STATUS_CONNECTION_FOUND;
	handle->next_status  = PUBSUB_STATUS_CONNECTION_FOUND;
	handle->idle_waiting = 0;
	handle->reconnecting = 0;
	return 0;
 bad:
	handle->reconnecting = 0;
	return -1;
}

/*
 * Check the connection.
 */
int
pubsub_check_connection(pubsub_handle_t *handle, pubsub_error_t *myerror)
{
	if (handle->sock >= 0 &&
	    handle->this_status == PUBSUB_STATUS_CONNECTION_FOUND) {
		return 0;
	}
#ifndef THREADED
	/*
	 * Try once. Critical when not using the mainloop, which is
	 * where we would normally try to reconnect.
	 */
	pubsub_reconnect(handle, myerror);
#endif
	if (handle->sock < 0 ||
	    handle->this_status != PUBSUB_STATUS_CONNECTION_FOUND) {
		return -1;
	}
	return 0;
}

/*
 * Test the connection periodically
 */
int
pubsub_set_idle_period(pubsub_handle_t *handle,
		       int seconds, pubsub_error_t *myerror)

{
	/*
	 * Use a timeout to launch an idle ping.
	 */
	if (! pubsub_add_timeout(handle, NULL, seconds * 1000,
				 idle_timeout_callback, NULL, myerror)) {
		return -1;
	}
	handle->idle_period  = seconds;
	handle->idle_waiting = 0;
	return 0;
}

/*
 * Compat with elvin. This does all kinds of stuff in elvin, but for
 * pubsub all it does is turn off reconnect and lets caller do it.
 */
int
pubsub_set_failover(pubsub_handle_t *handle,
		    int failover, pubsub_error_t *myerror)
{
	handle->failover = failover;
	return 0;
}

/*
 * Compat with elvin. In elvin, 0 means try once. Makes sense I guess.
 */
int
pubsub_set_connection_retries(pubsub_handle_t *handle,
			      int retry_count, pubsub_error_t *myerror)
{
	handle->max_retries = retry_count;
	return 0;
}

/*
 * Allocate and return an error structure
 */
pubsub_error_t *
pubsub_error_alloc(pubsub_handle_t *handle)
{
	pubsub_error_t	*myerror;
	
	LOCK(handle);
	if ((myerror = (pubsub_error_t *)
	     calloc(1, sizeof(pubsub_error_t))) == NULL) {
		UNLOCK(handle);
		return (pubsub_error_t *) NULL;
	}
	UNLOCK(handle);
	return myerror;
}

/*
 * Return the numeric error code associated with the error struct.
 */
int
pubsub_error_get_code(pubsub_error_t *error)
{
	return error->error;
}

/*
 * Format an error message from the error structure.
 */
int
pubsub_error_fprintf(FILE *fp, pubsub_error_t *myerror)
{
	fprintf(fp, "PubSub Error:%d - %s\n", myerror->error, myerror->msg);
	return 0;
}

/*
 * Insert an error message into the message structure.
 */
int
pubsub_error_error(pubsub_error_t *myerror, int error, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	
	myerror->error = error;
	myerror->msg = myerror->buf;
	vsnprintf(myerror->buf, sizeof(myerror->buf), fmt, args);
	va_end(args);
	return 0;
}

/*
 * Register for a callback if the status of the connection to the server
 * changes.
 */
int
pubsub_set_status_callback(pubsub_handle_t *handle, 
			   pubsub_status_callback_t status_cb,
			   void *arg,
			   pubsub_error_t *myerror)
{
	LOCK(handle);
	handle->status_callback     = status_cb;
	handle->status_callback_arg = arg;
	UNLOCK(handle);
	return 0;
}

/*
 * Return the current status of the connection.
 *
 * In addition to connect/disconnect, it may also return an internal
 * failure status.  This is intended to catch failures of the internal
 * threads used in the multi-threaded API.
 */
pubsub_status_t
pubsub_get_status(pubsub_handle_t *handle)
{
	if (handle == NULL || handle->server == NULL)
		return PUBSUB_STATUS_CONNECTION_NOSTATUS;
#ifdef THREADED
	/*
	 * Make sure all the internal threads are still alive
	 */
	if (handle->priv == NULL)
		return PUBSUB_STATUS_CONNECTION_NOSTATUS;
	if (pthread_kill(handle->priv->mainloop_pid, 0) != 0 ||
	    pthread_kill(handle->priv->dispatch_pid, 0) != 0 ||
	    pthread_kill(handle->priv->reconnect_pid, 0) != 0)
		return PUBSUB_STATUS_INTERNAL_FAILURE;
#endif
	return handle->this_status;
}

/*
 * Register a subscription with the server, returning a handle for
 * this subscription that can later be used for unsubscribe.
 *
 * TODO: Do not hardwire packet buffers.
 *       Network errors.
 */
pubsub_subscription_t *
pubsub_add_subscription(pubsub_handle_t *handle, char *expression,
			pubsub_notify_callback_t notify_cb, void *notify_arg,
			pubsub_error_t *myerror)
{
	pubsub_subscription_t	*subscription;
	int			exprlen = strlen(expression) + 1;

	if (pubsub_check_connection(handle, myerror) != 0) {
		myerror->msg   = "pubsub_add_subscription: Not Ready";
		myerror->error = ENOTCONN;
		return (pubsub_subscription_t *) NULL;
	}

	LOCK(handle);
	if ((subscription = (pubsub_subscription_t *)
	     calloc(1, sizeof(pubsub_subscription_t) + exprlen)) == NULL) {
		myerror->msg   = "pubsub_add_subscription: Out of memory";
		myerror->error = ENOMEM;
		UNLOCK(handle);
		return (pubsub_subscription_t *) NULL;
	}
	subscription->token      = subscription_counter++;
	subscription->expression = strdup(expression);
	subscription->notify_callback = notify_cb;
	subscription->notify_arg = notify_arg;

	if (pubsub_send_subscription(handle, subscription, 1, 1, myerror) < 0)
		goto bad;

	queue_enter(&handle->subscriptions,
		    subscription, pubsub_subscription_t *, chain);
	handle->sub_count++;

	UNLOCK(handle);
	return (pubsub_subscription_t *) subscription;
 bad:
	free(subscription->expression);
	free(subscription);
	UNLOCK(handle);
	return (pubsub_subscription_t *) NULL;
}

/*
 * This is the subscribe variant that returns immediately and then
 * issues a callback when the server acks it.
 */
int
pubsub_add_subscription_async(pubsub_handle_t *handle, char *expression,
			      pubsub_notify_callback_t notify_cb,
			      void *notify_arg,
			      pubsub_subscribe_callback_t subscribe_cb,
			      void *subscribe_arg,
			      pubsub_error_t *myerror)
{
	pubsub_subscription_t	*subscription;
	int			exprlen = strlen(expression) + 1;

	if (pubsub_check_connection(handle, myerror) != 0) {
		myerror->msg   = "pubsub_add_subscription_async: Not Ready";
		myerror->error = ENOTCONN;
		return -1;
	}

	LOCK(handle);
	if ((subscription = (pubsub_subscription_t *)
	     calloc(1, sizeof(pubsub_subscription_t) + exprlen)) == NULL) {
		myerror->msg   = "pubsub_add_subscription: Out of memory";
		myerror->error = ENOMEM;
		UNLOCK(handle);
		return -1;
	}
	subscription->token      = subscription_counter++;
	subscription->expression = strdup(expression);
	subscription->notify_callback = notify_cb;
	subscription->notify_arg = notify_arg;
	subscription->subscribe_callback = subscribe_cb;
	subscription->subscribe_arg = subscribe_arg;

	if (pubsub_send_subscription(handle, subscription, 0, 1, myerror) < 0)
		goto bad;

	UNLOCK(handle);
	return 0;
 bad:
	free(subscription->expression);
	free(subscription);
	UNLOCK(handle);
	return -1;
}

/*
 * Packet callback for async subscription and unsubscribe
 */
static void
subscribe_packet_callback(pubsub_handle_t *handle,
			  struct reply_packet *pktp, void *arg)
{
	pubsub_error_t		myerror;
	int			result = 0;
	pubsub_subscription_t	*subscription =
		(pubsub_subscription_t *) pktp->callback_arg;
	
	if (pubsub_debug & PUBSUB_DEBUG_CALLBACK) {
		ps_info("subscribe packet callback: %d\n", pktp->header.error);
	}
	myerror.error = pktp->header.error;
	if (myerror.error) {
		myerror.msg = "Operation failed";
		result = -1;
	}
	else {
		LOCK(handle);
		queue_enter(&handle->subscriptions,
			    subscription, pubsub_subscription_t *, chain);
		handle->sub_count++;
		UNLOCK(handle);

	}
	subscription->subscribe_callback(handle, result, subscription, 
					 subscription->subscribe_arg,
					 &myerror);

	/* Kill this in case we end up doing a resubscribe */
	subscription->subscribe_callback = NULL;
}
static void
unsubscribe_packet_callback(pubsub_handle_t *handle,
			    struct reply_packet *pktp, void *arg)
{
	pubsub_error_t		myerror;
	int			result = 0;
	pubsub_subscription_t	*subscription =
		(pubsub_subscription_t *) pktp->callback_arg;
	
	if (pubsub_debug & PUBSUB_DEBUG_CALLBACK) {
		ps_info("unsubscribe packet callback: %d\n",pktp->header.error);
	}
	myerror.error = pktp->header.error;
	if (myerror.error) {
		myerror.msg = "Operation failed";
		result = -1;
	}
	if (subscription->subscribe_callback) {
	    subscription->subscribe_callback(handle, result, subscription, 
					     subscription->subscribe_arg,
					     &myerror);
	}

	/*
	 * If the unsubscribe was successful, remove and free.
	 */
	LOCK(handle);
	queue_remove(&handle->subscriptions,
		     subscription, pubsub_subscription_t *, chain);
	handle->sub_count--;
	free(subscription->expression);
	free(subscription);
	UNLOCK(handle);
}

/*
 * Send the subscription packet.
 *
 * Lock is taken when called.
 */
static int
pubsub_send_subscription(pubsub_handle_t *handle,
			 pubsub_subscription_t *subscription, 
			 int blocking, int toplevel, pubsub_error_t *myerror)
{
	maxpacket_t		maxpacket;	/* XXX */
	subscription_packet_t	*pktp;
	reply_packet_t		*reply = &subscription->reply_packet;
	int			exprlen;

	/* Leave room for null byte */
	exprlen = strlen(subscription->expression) + 1;

	/* Make sure the expression fits in the static packet buffer */
	if (exprlen >= (sizeof(maxpacket) - sizeof(*pktp))) {
		myerror->msg   = "pubsub_add_subscription: expression too big";
		myerror->error = E2BIG;
		return -1;
	}

	if (blocking) {
		pktp = (subscription_packet_t *) &maxpacket;
	}
	else {
		/*
		 * Need to save this packet in case it has to be resent.
		 */
		if ((pktp = (subscription_packet_t *)
		     malloc(sizeof(maxpacket_t))) == NULL) {
		     myerror->msg   = "pubsub_add_subscription: Out of memory";
		     myerror->error = ENOMEM;
		     return -1;
		}
	}

	/* Init the packet here. */
	pktp->header.len   = htonl(sizeof(*pktp) + exprlen);
	pktp->header.type  = htons(PUBSUB_PKT_SUBSCRIBE);
	pktp->header.flags = 0;
	pktp->header.seq   = htonl(sequence_number++);
	pktp->header.error = 0;
	pktp->header.vers  = htonl(PUBSUB_CUR_VERSION);
	pktp->token        = htonl(subscription->token);
	strcpy(pktp->expression, subscription->expression);

	if (blocking) {
		reply->callback     = NULL;
		reply->callback_arg = NULL;
		reply->pktp         = NULL;
	}
	else {
		reply->callback     = subscribe_packet_callback;
		reply->callback_arg = subscription;
		reply->pktp         = (packet_t *) pktp;
	}

	/*
	 * Send and then wait for the reply packet in the dispatcher.
	 */
	if (pubsub_send(handle, blocking, toplevel, (packet_t *) pktp,
			&subscription->reply_packet, myerror) < 0) {
		return -1;
	}
	return 0;
}

/*
 * Remove a subscription.
 */
int
pubsub_rem_subscription(pubsub_handle_t *handle, 
			pubsub_subscription_t *subscription,
			pubsub_error_t *myerror)
{
	char			buf[PUBSUB_MAX_PACKET];	/* XXX */
	subscription_packet_t	*pktp = (subscription_packet_t *) buf;
	pubsub_subscription_t	*sub;

	if (pubsub_check_connection(handle, myerror) != 0) {
		myerror->msg   = "pubsub_rem_subscription: Not Ready";
		myerror->error = ENOTCONN;
		return -1;
	}

	/*
	 * Make sure subscription is on our list.
	 * This is primarily for the THREADED interface to avoid races.
	 */
	LOCK(handle);
	queue_iterate(&handle->subscriptions,
		      sub, pubsub_subscription_t *, chain) {
		if (sub == subscription)
			break;
	}
	if (sub != subscription) {
		UNLOCK(handle);
		myerror->msg = "pubsub_rem_subscription: Invalid Subscription";
		myerror->error = EINVAL;
		return -1;
	}

	/* Init the packet here. */
	pktp->header.len   = htonl(sizeof(*pktp));
	pktp->header.type  = htons(PUBSUB_PKT_UNSUBSCRIBE);
	pktp->header.flags = 0;
	pktp->header.seq   = htonl(sequence_number++);
	pktp->header.error = 0;
	pktp->header.vers  = htonl(PUBSUB_CUR_VERSION);
	pktp->token        = htonl(subscription->token);

	/* Clear async call back handler stuff */
	subscription->subscribe_callback        = NULL;
	subscription->subscribe_arg             = NULL;
	subscription->reply_packet.callback     = NULL;
	subscription->reply_packet.callback_arg = NULL;
	subscription->reply_packet.pktp         = NULL;
	subscription->busy  = 1;

	/*
	 * Send and then wait for the reply packet in the dispatcher.
	 */
	if (pubsub_send(handle, 1, 1, (packet_t *) pktp,
			       &subscription->reply_packet, myerror) < 0) {
		UNLOCK(handle);
		return -1;
	}
	queue_remove(&handle->subscriptions,
		     subscription, pubsub_subscription_t *, chain);
	handle->sub_count--;
	free(subscription->expression);
	free(subscription);
	UNLOCK(handle);
	return 0;
}


/*
 * Remove a subscription, the async variant.
 */
int
pubsub_rem_subscription_async(pubsub_handle_t *handle, 
			      pubsub_subscription_t *subscription,
			      pubsub_subscribe_callback_t unsubscribe_cb,
			      void *unsubscribe_arg,
			      pubsub_error_t *myerror)
{
	subscription_packet_t	*pktp;
	pubsub_subscription_t	*sub;

	if (pubsub_check_connection(handle, myerror) != 0) {
		myerror->msg   = "pubsub_rem_subscription_async: Not Ready";
		myerror->error = ENOTCONN;
		return -1;
	}

	/*
	 * Need to save this packet in case it has to be resent.
	 */
	if ((pktp = (subscription_packet_t *)
	     malloc(sizeof(subscription_packet_t))) == NULL) {
		myerror->msg   = "pubsub_rem_subscription: Out of memory";
		myerror->error = ENOMEM;
		return -1;
	}

	/*
	 * Make sure subscription is on our list.
	 * This is primarily for the THREADED interface to avoid races.
	 */
	LOCK(handle);
	queue_iterate(&handle->subscriptions,
		      sub, pubsub_subscription_t *, chain) {
		if (sub == subscription)
			break;
	}
	if (sub != subscription) {
		UNLOCK(handle);
		myerror->msg   = "pubsub_rem_subscription_async: Invalid Subscription";
		myerror->error = EINVAL;
		free(pktp);
		return -1;
	}

	/* Init the packet here. */
	pktp->header.len   = htonl(sizeof(*pktp));
	pktp->header.type  = htons(PUBSUB_PKT_UNSUBSCRIBE);
	pktp->header.flags = 0;
	pktp->header.seq   = htonl(sequence_number++);
	pktp->header.error = 0;
	pktp->header.vers  = htonl(PUBSUB_CUR_VERSION);
	pktp->token        = htonl(subscription->token);

	/* Async Call back handler */
	subscription->subscribe_callback        = unsubscribe_cb;
	subscription->subscribe_arg             = unsubscribe_arg;
	subscription->reply_packet.callback     = unsubscribe_packet_callback;
	subscription->reply_packet.callback_arg = subscription;
	subscription->reply_packet.pktp         = (packet_t *) pktp;
	subscription->busy                      = 1;

	/*
	 * Send and then wait for the reply packet in the dispatcher.
	 */
	if (pubsub_send(handle, 0, 1, (packet_t *) pktp,
			&subscription->reply_packet, myerror) < 0) {
		UNLOCK(handle);
		return -1;
	}
	UNLOCK(handle);
	return 0;
}

/*
 * Allocate a notification
 */
pubsub_notification_t *
pubsub_notification_alloc(pubsub_handle_t *handle, pubsub_error_t *myerror)
{
	pubsub_notification_t *notification;

	LOCK(handle);
	if ((notification = (pubsub_notification_t *)
	     calloc(1, sizeof(pubsub_notification_t))) == NULL) {
		myerror->msg   = "pubsub_notification_alloc: Out of memory";
		myerror->error = ENOMEM;
		UNLOCK(handle);
		return (pubsub_notification_t *) NULL;
	}
	UNLOCK(handle);
#ifdef  ELVIN_COMPAT
	pubsub_notification_add_int32(notification, "___PUBSUB___", 1,myerror);
#endif
	return notification;
}

/*
 * Clear a notification.
 */
void
pubsub_notification_clear(pubsub_notification_t *notification,
			  pubsub_error_t *myerror)
{
	notification->used = 0;
#ifdef  ELVIN_COMPAT
	pubsub_notification_add_int32(notification, "___PUBSUB___", 1,myerror);
#endif
}

/*
 * Free a notification
 */
void
pubsub_notification_free(pubsub_handle_t *handle,
			 pubsub_notification_t *notification,
			 pubsub_error_t *myerror)
{
	LOCK(handle);
	free(notification);
	UNLOCK(handle);
}

/*
 * Clone a notification
 */
pubsub_notification_t *
pubsub_notification_clone(pubsub_handle_t *handle,
			  pubsub_notification_t *pn, pubsub_error_t *myerror)
{
	pubsub_notification_t *notification;

	assert(pn->used <= sizeof(pn->data));

	LOCK(handle);
	if ((notification = (pubsub_notification_t *)
	     calloc(1, sizeof(pubsub_notification_t))) == NULL) {
		myerror->msg   = "pubsub_notification_clone: Out of memory";
		myerror->error = ENOMEM;
		UNLOCK(handle);
		return (pubsub_notification_t *) NULL;
	}
	UNLOCK(handle);

	memcpy(notification, pn, sizeof(pubsub_notification_t));
	
	return notification;
}

#define SIZEOF_INT sizeof(int32_t)
#define ALIGN_INT(size) (((size) + (SIZEOF_INT - 1)) & ~(SIZEOF_INT - 1))

/*
 * Add a value to the notification
 */
static void
notification_add_tlv(pubsub_notification_t *notification,
		     pubsub_type_t type, int len, const char *value)
{
	int32_t *typep = (int32_t *)&notification->data[notification->used];
	int32_t *lenp = (int32_t *)&notification->data[notification->used +
						       SIZEOF_INT];

	assert(notification != NULL);
	assert(notification->used <= sizeof(notification->data));

	*typep = htonl(type);
	*lenp = htonl(len);
	if (value != NULL) {
		memcpy(&notification->data[notification->used + SIZEOF_INT*2],
		       value,
		       len);
	}
	notification->used += SIZEOF_INT + SIZEOF_INT + ALIGN_INT(len);
}

/*
 * Get the next value from the notification
 */
static int
notification_next_tlv(pubsub_notification_t *notification, int *index,
		      pubsub_type_t *type_out, int *len_out, char **value_out)
{
	assert(notification != NULL);
	assert(notification->used <= sizeof(notification->data));
	assert(index != NULL);
	assert(len_out != NULL);
	assert(value_out != NULL);

	if (((*index) + SIZEOF_INT * 2) >= notification->used) {
		return 0;
	}

#define EXTRACT(type, buf, ix) ({ union foo { type tp; char cp; } *foo = (union foo *)&(buf)[ix]; foo->tp; })

	*type_out = ntohl(EXTRACT(pubsub_type_t, notification->data, *index));
	*index += SIZEOF_INT;
	*len_out = ntohl(EXTRACT(int32_t, notification->data, *index));
	*index += SIZEOF_INT;
	if (*len_out < 0 || *len_out > (notification->used - *index))
		return 0;
	*value_out = &notification->data[*index];
	*index += ALIGN_INT(*len_out);

#undef EXTRACT

	return 1;
}

static int
notification_add_pair(pubsub_notification_t *notification,
		      const char *name,
		      pubsub_type_t type, const char *value, int len,
		      pubsub_error_t *myerror)
{
	int total_len;

	total_len = SIZEOF_INT * 2 + ALIGN_INT(strlen(name) + 1) +
		SIZEOF_INT * 2 + ALIGN_INT(len);

	if (total_len > sizeof(notification->data) - notification->used) {
		myerror->msg   = "notification_add_pair: notification too big";
		myerror->error = ENOMEM;
		return -1;
	}

	notification_add_tlv(notification,
			     STRING_TYPE, strlen(name) + 1, name);
	notification_add_tlv(notification, type, len, value);

	return 0;
}

static int
notification_get_pair(pubsub_notification_t *notification,
		      const char *name,
		      pubsub_type_t type, char **val_out, int *len_out,
		      pubsub_error_t *myerror)
{
	pubsub_type_t npt, vpt;
	int index = 0, klen;
	char *key;

	while (notification_next_tlv(notification, &index,
				     &npt, &klen, &key) &&
	       npt == STRING_TYPE &&
	       notification_next_tlv(notification, &index,
				     &vpt, len_out, val_out)) {
		key[klen - 1] = '\0';
		if (strcmp(name, key) == 0 && vpt == type) {
			return 0;
		}
	}

	myerror->msg   = "notification_get_pair: No such key";
	myerror->error = ENOENT;
	
	return -1;
}

/*
 * Add an opaque byte array to a notification
 */
int
pubsub_notification_add_opaque(pubsub_notification_t *notification,
			       const char *name, const char *value, int len,
			       pubsub_error_t *myerror)
{
	return notification_add_pair(notification, name,
				     OPAQUE_TYPE, value, len,
				     myerror);
}

/*
 * Get an opaque byte array from a notification
 */
int
pubsub_notification_get_opaque(pubsub_notification_t *notification,
			       const char *name, char **val_out, int *len_out,
			       pubsub_error_t *myerror)
{
	return notification_get_pair(notification,
				     name,
				     OPAQUE_TYPE, val_out, len_out,
				     myerror);
}

/*
 * Get a string from a notification
 */
int
pubsub_notification_get_string(pubsub_notification_t *notification,
			       const char *name, char **val_out,
			       pubsub_error_t *myerror)
{
	int len, rc;

	rc = notification_get_pair(notification,
				   name,
				   STRING_TYPE, val_out, &len,
				   myerror);

	if (rc == 0)
		(*val_out)[len - 1] = '\0';
	
	return rc;
}

/*
 * Remove a key from a notification
 */
int
pubsub_notification_remove(pubsub_notification_t *notification,
			   const char *name, pubsub_error_t *myerror)
{
	int index = 0, last_index = 0, klen, vlen;
	pubsub_type_t type;
	char *key, *value;

	while (notification_next_tlv(notification, &index,
				     &type, &klen, &key) &&
	       type == STRING_TYPE &&
	       notification_next_tlv(notification, &index,
				     &type, &vlen, &value)) {
		key[klen - 1] = '\0';
		if (strcmp(name, key) == 0) {
			memmove(&notification->data[last_index],
				&notification->data[index],
				notification->used - index);
			notification->used -= (index - last_index);
			return 0;
		}
		last_index = index;
	}

	myerror->msg   = "pubsub_notification_remove: No such key";
	myerror->error = ENOENT;
	
	return -1;
}

/*
 * Add a string to a notification
 */
int
pubsub_notification_add_string(pubsub_notification_t *notification,
			       const char *name, const char *value,
			       pubsub_error_t *myerror)
{
	return notification_add_pair(notification, name,
				     STRING_TYPE, value, strlen(value) + 1,
				     myerror);
}

/*
 * Add/extract a 32-bit integer to/from a notification
 * XXX not compatible with elvin
 */
int
pubsub_notification_add_int32(pubsub_notification_t *notification,
			      const char *name, int32_t value,
			      pubsub_error_t *myerror)
{
	value = htonl(value);
	
	return notification_add_pair(notification, name,
				     INT32_TYPE, (char *)&value, sizeof(value),
				     myerror);
}

int
pubsub_notification_get_int32(pubsub_notification_t *notification,
			      const char *name, int32_t *value_out,
			      pubsub_error_t *myerror)
{
	int32_t *nvalue;
	int nlen;

	assert(value_out != NULL);

	if (notification_get_pair(notification,
				  name,
				  INT32_TYPE, (char **)&nvalue, &nlen,
				  myerror) == -1 ||
	    nlen != sizeof(int32_t)) {
		return -1;
	}

	*value_out = ntohl(*nvalue);

	return 0;
}

#define ntohll(x) (((int64_t)(ntohl((int)((x << 32) >> 32))) << 32) | \
                     (unsigned int)ntohl(((int)(x >> 32))))
#define htonll(x) ntohll(x)

/*
 * Add/extract a 64-bit integer to/from a notification
 * XXX not compatible with elvin
 */
int
pubsub_notification_add_int64(pubsub_notification_t *notification,
			      const char *name, int64_t value,
			      pubsub_error_t *error)
{
	value = htonll(value);

	return notification_add_pair(notification,
				     name,
				     INT64_TYPE, (char *)&value, sizeof(value),
				     error);
}

int
pubsub_notification_get_int64(pubsub_notification_t *notification,
			      const char *name, int64_t *value_out,
			      pubsub_error_t *error)
{
	int64_t *nval;
	int nlen;
	
	assert(value_out != NULL);

	if (notification_get_pair(notification,
				  name,
				  INT64_TYPE, (char **)&nval, &nlen,
				  error) == -1 ||
	    nlen != sizeof(int64_t)) {
		return -1;
	}

	*value_out = ntohll(*nval);

	return 0;
}

/*
 * Add/extract a double to/from a notification
 * XXX not compatible with elvin and COMPLETELY BOGUS!
 */
int
pubsub_notification_add_real64(pubsub_notification_t *notification,
			       const char *name, double value,
			       pubsub_error_t *error)
{
	int64_t ival, *foo = (int64_t *)&value;

	ival = htonll(*foo);
	return notification_add_pair(notification,
				     name,
				     REAL64_TYPE,
				     (char *)&ival, sizeof(int64_t),
				     error);
}

int
pubsub_notification_get_real64(pubsub_notification_t *notification,
			       const char *name, double *value_out,
			       pubsub_error_t *error)
{
	int64_t *nval;
	int64_t *foo = (int64_t *)value_out;
	int nlen;
	
	assert(value_out != NULL);

	if (notification_get_pair(notification,
				  name,
				  REAL64_TYPE, (char **)&nval, &nlen,
				  error) == -1 ||
	    nlen != sizeof(int64_t)) {
		return -1;
	}

	*foo = ntohll(*nval);

	return 0;
}

/*
 * Traverse the contents of a notification
 */
int
pubsub_notification_traverse(pubsub_notification_t *notification,
			     pubsub_traverse_func_t func,
			     void *arg, pubsub_error_t *myerror)
{
	int index = 0, klen, vlen;
	pubsub_type_t kpt, vpt;
	char *key, *value;

	while (notification_next_tlv(notification, &index,
				     &kpt, &klen, &key) &&
	       kpt == STRING_TYPE &&
	       notification_next_tlv(notification, &index,
				     &vpt, &vlen, &value)) {
		pubsub_value_t pv;

		key[klen - 1] = '\0';
		switch (vpt) {
		case INT32_TYPE:
			if (vlen != sizeof(int32_t))
				continue;
			
			pv.pv_int32 = ntohl(*((int32_t *)value));
			break;
		case INT64_TYPE:
		case REAL64_TYPE:
			if (vlen != sizeof(int64_t))
				continue;
			
			pv.pv_int64 = ntohll(*((int64_t *)value));
			break;
		case STRING_TYPE:
			if (vlen < 1)
				continue;
			
			value[vlen - 1] = '\0';
			pv.pv_string = value;
			break;
		case OPAQUE_TYPE:
			if (vlen < 0)
				continue;
			
			pv.pv_opaque.data = value;
			pv.pv_opaque.length = vlen;
			break;
		default:
			continue;
		}
		
		if (func(arg, key, vpt, pv, myerror) == 0)
			return 0;
	}

	return 1;
}

/*
 * Send a notification
 */ 
int
pubsub_notify(pubsub_handle_t *handle,
	      pubsub_notification_t *notification,
	      pubsub_error_t *myerror)
{
	char			buf[PUBSUB_MAX_PACKET];	/* XXX */
	notification_packet_t	*pktp = (notification_packet_t *) buf;

	if (pubsub_check_connection(handle, myerror) != 0) {
		myerror->msg   = "pubsub_notify: Not Ready";
		myerror->error = ENOTCONN;
		return -1;
	}

	/* Init the packet here. */
	assert(sizeof(*pktp) + notification->used <= sizeof(buf)); /* XXX */
	pktp->header.len   = htonl(sizeof(*pktp) + notification->used);
	pktp->header.type  = htons(PUBSUB_PKT_NOTIFY);
	pktp->header.flags = 0;
	pktp->header.seq   = htonl(sequence_number++);
	pktp->header.error = 0;
	pktp->header.vers  = htonl(PUBSUB_CUR_VERSION);

	pktp->token = htonl(notification->token);
	pktp->used  = htonl(notification->used);
	memcpy(pktp->data, notification->data, notification->used);

	if (pubsub_notify_internal(handle, (packet_t *) pktp, myerror) < 0) {
		return -1;
	}
	return 0;
}

/*
 * Internal function we can use from clusterd.
 */
int
pubsub_notify_internal(pubsub_handle_t *handle, packet_t *pktp,
		       pubsub_error_t *myerror)
{
	reply_packet_t		reply_packet;

	/* Stack allocated. */
	reply_packet.callback     = NULL;
	reply_packet.callback_arg = NULL;
	reply_packet.pktp         = NULL;

	LOCK(handle);
	/*
	 * Send packet and wait for the reply packet in the dispatcher.
	 */
	if (pubsub_send(handle, 1, 1, (packet_t *) pktp,
			&reply_packet, myerror) < 0) {
		UNLOCK(handle);
		return -1;
	}
	UNLOCK(handle);
	return 0;
}

/*
 * Mainloop for non-threaded programs. Read and dispatch packets from the
 * server until we get an error we do not like.
 */
int
pubsub_mainloop(pubsub_handle_t *handle,
		int *keep_looping, pubsub_error_t *myerror)
{
#ifdef	THREADED
	if (1) {
		myerror->msg   = "pubsub_mainloop: Improper use of mainloop";
		myerror->error = EPERM;
		return -1;
	}
#endif
	while (*keep_looping) {
		if (pubsub_dispatcher(handle, 1, myerror) < 0) {
			return -1;
		}
		if (handle->sock < 0) {
			if (pubsub_reconnect(handle, myerror) < 0) {
				return -1;
			}
		}
	}
	return 0;
}

#ifndef	THREADED
/*
 * This variant runs the dispatcher, checking for errors and disconnect.
 * Returns when/if things are normal or we get a fatal error.
 */
static int
pubsub_checker(pubsub_handle_t *handle, int blocking, pubsub_error_t *myerror)
{
	if (pubsub_dispatcher(handle, blocking, myerror) < 0) {
		return -1;
	}
	while (handle->sock < 0) {
		if (pubsub_reconnect(handle, myerror) < 0) {
			return -1;
		}
		if (handle->sock < 0 &&
		    pubsub_dispatcher(handle, 1, myerror) < 0) {
			return -1;
		}
	}
	return 0;
}
#endif

#ifdef THREADED
/*
 * Read packets from the wire and queue them.
 */
static void *
threaded_mainloop(void *arg)
{
	pubsub_handle_t *handle = (pubsub_handle_t *) arg;
	pubsub_error_t  myerror;
	fd_set		fds;
	int		i, dodrop, sock, maxfd;
	struct timeval  timeout;

	FD_ZERO(&fds);

	while (1) {
		pthread_testcancel();

		LOCK(handle);
	again:
		dodrop = 0;
		
		if (handle->sock >= 0) {
			sock  = handle->sock;
			maxfd =  sock + 1;
			FD_SET(handle->sock, &fds);
			timeout.tv_sec  = 5;
			timeout.tv_usec = 0;
			errno  = 0;
			UNLOCK(handle);
			i = select(maxfd, &fds, NULL, NULL, &timeout);
			LOCK(handle);
		
			if (i < 0 && errno != EINTR) {
				/* connection got dropped after UNLOCK */
				if (errno == EBADF && handle->sock != sock) {
					ps_error("pubsub_dispatcher: "
						 "select returns EBADF: "
						 "sock=%d, handle->sock=%d\n",
						 sock, handle->sock);
					FD_CLR(sock, &fds);
					goto again;
				}
				ps_fatal("pubsub_dispatcher: select failed: "
					 "errno:%d\n", errno);
			}
			if (i <= 0) {
				dodrop = check_overdue(handle);
			}
			else if (FD_ISSET(sock, &fds)) {
				if (pubsub_readpacket(handle, &myerror) < 0)
					dodrop = 1;
			}
			/* In case reconnect changes socket */
			FD_CLR(sock, &fds);
		}
		else {
			UNLOCK(handle);
			sleep(1);
			LOCK(handle);
			dodrop = check_overdue(handle);
		}
		if (dodrop) {
			pubsub_dropconnect(handle, &myerror);
			goto again;
		}
		if (! queue_empty(&handle->priv->packets)) {
			DISPATCHER_INTERRUPT(handle);
		}
		UNLOCK(handle);
		sched_yield();
	}
	return NULL;
}

/*
 * Dequeue packets queued by the mainloop above.
 */
static void *
threaded_dispatcher(void *arg)
{
	pubsub_handle_t *handle = (pubsub_handle_t *) arg;
	pubsub_error_t  myerror;

	while (1) {
		pthread_testcancel();

		LOCK(handle);
		if (!queue_empty(&handle->priv->packets)) {
			pubsub_packet_dequeue(handle, &myerror);
		}
		UNLOCK(handle);
		pubsub_dispatcher(handle, 1, &myerror);
	}
	return NULL;
}

/*
 * Keep the connection open. Easier if we do this from one place.
 */
static void *
threaded_reconnect(void *arg)
{
	pubsub_handle_t *handle = (pubsub_handle_t *) arg;
	pubsub_error_t  myerror;

	while (1) {
		pthread_testcancel();

		LOCK(handle);
		if (handle->sock < 0) {
			pubsub_reconnect(handle, &myerror);
		}
		UNLOCK(handle);
		sleep(5);
	}
	return NULL;
}

static void
dispatcher_interrupt(pubsub_handle_t *handle)
{
	if (handle->priv->writepipe_fd) {
		char	foo = 0;
		int	rv;

		rv = write(handle->priv->writepipe_fd, &foo, sizeof(foo));
		if (rv != sizeof(foo))
			/* we don't care */;
	}
}
#endif

/*
 * A variant of the dispatcher that is available to clients.
 * Do not return errors to the caller until I figure out a better plan.
 */
int
pubsub_dispatch(pubsub_handle_t *handle,
		int blocking, pubsub_error_t *myerror)
{
#ifdef	THREADED
	if (1) {
		myerror->msg   = "pubsub_dispatch: Improper use of dispatch";
		myerror->error = EPERM;
		return -1;
	}
#endif
	if (handle->sock < 0) {
		if (pubsub_reconnect(handle, myerror) < 0) {
			return -1;
		}
	}
	return pubsub_dispatcher(handle, blocking, myerror);
}

/*
 * Function for mainloop, read packets and dispatch them. Also available
 * to outside clients.
 */
static int
pubsub_dispatcher(pubsub_handle_t *handle,
		  int blocking, pubsub_error_t *myerror)
{
	fd_set			fds;
	int			i, j;
	struct timeval		timeout, now;
	reply_packet_t		*pktp;
	
	timerclear(&timeout);

	/*
	 * Check to see if we need to dispatch a status changed callback.
	 */
	LOCK(handle);
	if (handle->status_callback &&
	    handle->last_status != handle->next_status) {
		handle->last_status = handle->next_status;
		UNLOCK(handle);
		handle->status_callback(handle, handle->next_status,
					handle->status_callback_arg, myerror);
		LOCK(handle);
	}

	/*
	 * Dispatch any callbacks for reply packets. This funky loop is cause
	 * the packet might be deallocated, but thats not a good thing to do
	 * while inside queue_iterate().
	 */
 again:
	queue_iterate(&handle->reply_packets, pktp, reply_packet_t *, chain) {
		if (pubsub_debug & PUBSUB_DEBUG_WAYDETAIL) {
			ps_info("reply: %d %d %x len:%d type:%d seq:%d vers:%d\n",
			     pktp->reply_received, ntohl(pktp->sequence),
			     pktp->callback,
			     ntohl(pktp->header.len),
			     ntohs(pktp->header.type),
			     ntohl(pktp->header.seq),
			     ntohl(pktp->header.vers));
		}
		if (pktp->reply_received && pktp->callback) {
			queue_remove(&handle->reply_packets,
				     pktp, reply_packet_t *, chain);
			if (pktp->pktp)
				free(pktp->pktp);
			UNLOCK(handle);
			pktp->callback(handle, pktp, pktp->callback_arg);
			LOCK(handle);
			goto again;
		}
	}

	/*
	 * Look for any overdue reply packets. If we find one, abort the
	 * connection (which will abort all outstanding packets).
	 */
	if (check_overdue(handle)) {
		pubsub_dropconnect(handle, myerror);
		UNLOCK(handle);
		return 0;
	}
	
	if (blocking) {
		if (!queue_empty(&handle->timeouts)) {
			pubsub_timeout_t *pt;

			pt = (pubsub_timeout_t *)
				queue_first(&handle->timeouts);
			gettimeofday(&now, NULL);
			/* Watch for negative values */
			if (timercmp(&now, &pt->timeout, <)) {
				timersub(&pt->timeout, &now, &timeout);
			}
			else {
				/* Quick poll, but watch for sock test below */
				timeout.tv_sec  = 0;
				timeout.tv_usec = 10;
			}
		}
		else {
			timeout.tv_sec  = 60; /* Periodic debug */
			timeout.tv_usec = 0;
		}
	}

	/*
	 * If the connection to the server was lost, do not sleep
	 * more then 5 seconds; want to retry periodically. Ditto if
	 * there are outstanding reply packets since we want to time them
	 * out sooner.
	 */
	if (handle->sock < 0) {
		if ((timeout.tv_sec > 5) ||
		    (timeout.tv_sec == 0 && timeout.tv_usec == 0)) {
			timeout.tv_sec = 5;
		}
	}
	else if (! queue_empty(&handle->reply_packets)) {
		if (timeout.tv_sec > 5)
			timeout.tv_sec = 5;
	}
	
	if ((pubsub_debug & PUBSUB_DEBUG_DETAIL) &&
	    (timeout.tv_sec > 0 || timeout.tv_usec > 0)) { 
		ps_info("dispatcher: timeout %ld:%ld socket %d\n",
		     timeout.tv_sec, timeout.tv_usec, handle->sock);
	}

	fds = handle->sfds;
	UNLOCK(handle);
	errno = 0;
	i = select(handle->maxfds, &fds, NULL, NULL, &timeout);
	if (i < 0) {
		if (errno != EINTR) {
			myerror->msg   = "pubsub_dispatcher: select failure";
			myerror->error = errno;

			if (pubsub_debug & PUBSUB_DEBUG_DISPATCHER) {
				ps_error("pubsub_dispatcher: select failed: "
				      "errno:%d\n", errno);
			}
			return -1;
		}
	}
	LOCK(handle);
	
	/*
	 * Look for a timeout to dispatch.
	 */
	if (blocking && !queue_empty(&handle->timeouts)) {
		pubsub_timeout_t *pt;
			
		pt = (pubsub_timeout_t *) queue_first(&handle->timeouts);
		gettimeofday(&now, NULL);
		if (timercmp(&now, &pt->timeout, >=)) {
			pubsub_timeout_dispatch(handle, myerror);
		}
	}
	UNLOCK(handle);

	/*
	 * Caller gets control on any non-EINTR error.
	 */
	if (i <= 0)
		return 0;

#ifndef	THREADED
	if (handle->sock >= 0 && FD_ISSET(handle->sock, &fds)) {
		i -= 1;
		if (pubsub_readpacket(handle, myerror) < 0) {
			if (pubsub_debug & PUBSUB_DEBUG_DISPATCHER) {
				ps_error("pubsub_dispatcher: readpacket failed: "
				      "errno:%d\n", myerror->error);
			}
			/*
			 * Caller gets reconnect status.
			 */
			return pubsub_reconnect(handle, myerror);
		}
	}
#endif
	/* Check other FDs for IO handler callbacks. */
	for (j = 0; i > 0; j++) {
		if (FD_ISSET(j, &fds)) {
			i -= 1;
#ifdef	THREADED
			/*
			 * This exists simply to make sure we can
			 * breakout of the select. Just need to read
			 * the data to clear it.
			 */
			if (j == handle->priv->readpipe_fd) {
				char	foo;
				int	rv;

				rv = read(handle->priv->readpipe_fd,
					  &foo, sizeof(foo));
				if (rv != sizeof(foo))
					/* we don't care */;
				continue;
			}
#endif
			pubsub_iohandler_dispatch(handle, j, myerror);
		}
	}
	return 0;
}

/*
 * Send a packet. Called with lock taken.
 */
static int
pubsub_send_packet(pubsub_handle_t *handle, packet_t *packet, int toplevel,
		   struct timeval *timesent, pubsub_error_t *myerror)
{
	int	tmp;

	while (1) {
		if (handle->sock >= 0) {
			UNLOCK(handle);
			tmp = ps_PacketSend(handle->sock,
					    packet, (packet_t *) NULL);
			LOCK(handle);
			if (tmp < 0) {
				/*
				 * Drop the connection so that the
				 * mainloop notices.
				 */
#ifdef	THREADED
				pubsub_dropconnect(handle, myerror);
				if (!toplevel)
					return -1;
#endif
			}
			else
				break;
		}
#ifdef	THREADED
		/*
		 * The main loop will then catch the closed connection.
		 * Wait to be signaled that things are running again.
		 */
		while (handle->sock < 0) {
			pthread_cond_wait(&handle->priv->sockcond,
					  &handle->priv->lock);
		}
#else
		/*
		 * If the server went away, we want to reconnect.
		 */
		if (pubsub_checker(handle, 0, myerror) < 0) {
			return -1;
		}
#endif
	}
	if (pubsub_debug & PUBSUB_DEBUG_DETAIL) {
		ps_info("packet sent: %d len:%d type:%d seq:%d vers:%d\n",
		     ntohl(packet->header.seq),
		     ntohl(packet->header.len),
		     ntohs(packet->header.type),
		     ntohl(packet->header.seq),
		     ntohl(packet->header.vers));
	}
	
	/* Remember time for timeout check in dispatcher */
	gettimeofday(timesent, NULL);	

	return 0;
}

/*
 * Internal function that uses the dispatcher to wait for a reply packet
 * from the server. This allows us to service other packets while waiting.
 *
 * This function is called with the lock taken.
 */
static int
pubsub_send(pubsub_handle_t *handle, int blocking, int toplevel, 
	    packet_t *packet, reply_packet_t *reply, pubsub_error_t *myerror)
{
	/*
	 * We init the sequence number and then drop it into the queue
	 * before we send the packet out (cause of the threaded version).
	 */
	bzero(&reply->header, sizeof(reply->header));
	reply->sequence = packet->header.seq;
	reply->reply_received = 0;
	reply->aborted = 0;
	/* clear for timeout check in dispatcher */
	reply->timesent.tv_sec = reply->timesent.tv_usec = 0;

#ifndef	THREADED
	/*
	 * Use the dispatcher to check for a closed connection, which is
	 * a pretty ugly approach. But, writing to a disconnected socket
	 * might not tell me its disconnected for a while, but read does
	 * a better job of catching it, and I would rather catch it now
	 * rather then later when there is no reply to the packet.
	 */
	if (handle->send_count && pubsub_checker(handle, 0, myerror) < 0) {
		return -1;
	}
#endif
	queue_enter(&handle->reply_packets,
		    reply, reply_packet_t *, chain);
 again:
	if (pubsub_send_packet(handle, packet,
			       toplevel, &reply->timesent, myerror) < 0) {
		ps_info("pubsub_send_packet failed\n");
		goto bad;
	}

	handle->send_count++;
	if (!blocking)
		return 0;
	
	while (! reply->reply_received &&
	       ! reply->aborted) {
#ifdef	THREADED
		/*
		 * No need to call the dispatcher since there is a thread
		 * running it all the time, and it will signal us when the
		 * reply comes in so we can check.
		 */
		pthread_cond_wait(&handle->priv->replycond,
				  &handle->priv->lock);
#else
		/*
		 * Spin in the checker, which will dispatch and keep
		 * the connection open until we get a reply.
		 */
		if (pubsub_checker(handle, 1, myerror) < 0) {
			goto bad;
		}
#endif
	}
	/*
	 * Look for no reply timeout and attempt resend.
	 */
	if (reply->aborted && reply->header.error == ECONNRESET) {
#ifdef	THREADED
		if (!toplevel)
			goto bad;
#endif
		reply->header.error = 0;
		reply->aborted = 0;
		/* clear for timeout check in dispatcher */
		reply->timesent.tv_sec = reply->timesent.tv_usec = 0;
		goto again;
	}
	queue_remove(&handle->reply_packets,
		     reply, reply_packet_t *, chain);
	if (reply->pktp)
		free(reply->pktp);
	if (reply->header.error) {
		myerror->msg   = "Server error in reply";
		myerror->error = reply->header.error;
		return -1;
	}
	return 0;
 bad:
	myerror->msg   = "Server error";
	myerror->error = EIO;
	queue_remove(&handle->reply_packets, reply, reply_packet_t *, chain);
	if (reply->pktp)
		free(reply->pktp);
	return -1;
}

/*
 * Dispatch a notification to its subscriber.
 */
static int
pubsub_notification_dispatch(pubsub_handle_t *handle,
			     notification_packet_t *packet,
			     pubsub_error_t *myerror)
{
	pubsub_subscription_t	*subscription;
	pubsub_notification_t	*notification = (pubsub_notification_t *)
		(((char *) packet) + sizeof(pheader_t));
	int			body_len;
#ifdef THREADED
	struct sublist {
		pubsub_subscription_t *subptr;
		void (*subfunc)(pubsub_handle_t *,
				struct pubsub_subscription *,
				pubsub_notification_t *,
				void *);
		void *subarg;
	} *list;
	int count, i;
#endif

	body_len = ntohl(packet->header.len);
	if (body_len < sizeof(*packet) ||
	    body_len < sizeof(*packet) + ntohl(notification->used)) {
		myerror->msg = "notification_dispatch: invalid packet";
		myerror->error = EINVAL;
		return -1;
	}
	
	notification->used = ntohl(packet->used);

#ifdef THREADED
	/*
	 * In the threaded world, we need to protect against subscriptions
	 * that are added or removed while we are delivering notifications.
	 * So, while the handle is locked, we make a list of all current
	 * subscriptions which should be notified and use that list for the
	 * iteration.
	 */
	list = calloc(handle->sub_count, sizeof(struct sublist));
	if (list == NULL) {
		myerror->msg = "notification_dispatch: no memory";
		myerror->error = ENOMEM;
		return -1;
	}

	count = 0;
	queue_iterate(&handle->subscriptions,
		      subscription, pubsub_subscription_t *, chain) {
		if (subscription->token == ntohl(packet->token)) {
			list[count].subptr = subscription;
			list[count].subfunc = subscription->notify_callback;
			list[count].subarg = subscription->notify_arg;
			count++;
		}
	}

	UNLOCK(handle);
	for (i = 0; i < count; i++) {
		list[i].subfunc(handle, list[i].subptr, notification,
				list[i].subarg);
	}
	LOCK(handle);
	free(list);
#else
	queue_iterate(&handle->subscriptions,
		      subscription, pubsub_subscription_t *, chain) {
		if (subscription->token == ntohl(packet->token)) {
			subscription->notify_callback(handle,
					      subscription,
					      notification,
					      subscription->notify_arg);
		}
	}
#endif
	return 0;
}

#ifdef	THREADED
/*
 * Dequeue a packet and dispatch it.
 *
 * Called with lock taken.
 */
static int
pubsub_packet_dequeue(pubsub_handle_t *handle, pubsub_error_t *myerror)
{
	queued_packet_t		*ptmp;

	if (queue_empty(&handle->priv->packets)) {
		return 0;
	}
	queue_remove_first(&handle->priv->packets,
			   ptmp, queued_packet_t *, chain);

	pubsub_notification_dispatch(handle,
				     (notification_packet_t *)&ptmp->packet,
				     myerror);
	free(ptmp);
	return 0;
}

/*
 * Queue a packet for processing by the dispatcher loop.
 *
 * Called with lock taken.
 */
static int
pubsub_packet_enqueue(pubsub_handle_t *handle,
		      packet_t *packet, pubsub_error_t *myerror)
{
	queued_packet_t		*ptmp;
	int			plen = ntohl(packet->header.len);

	if ((ptmp = (queued_packet_t *)
	     malloc(sizeof(queued_packet_t))) == NULL) {
		myerror->msg   = "pubsub_packet_queue: Out of memory";
		myerror->error = ENOMEM;
		return -1;
	}
	assert(plen <= sizeof(ptmp->packet));
	
	memcpy(&ptmp->packet, packet, plen);
	queue_enter(&handle->priv->packets, ptmp, queued_packet_t *, chain);
	return 0;
}
#endif

/*
 * Packet read and dispatch (or queue).
 *
 * Called with lock taken.
 */
static int
pubsub_readpacket(pubsub_handle_t *handle, pubsub_error_t *myerror)
{
	int		cc;
	maxpacket_t	packet;
	reply_packet_t	*pktp;

	UNLOCK(handle);
	cc = ps_PacketRead(handle->sock, (packet_t *) &packet, sizeof(packet));
	LOCK(handle);

	if (cc < 0) {
		myerror->error = errno;
		return -1;
	}
	if (pubsub_debug & PUBSUB_DEBUG_DETAIL) {
		ps_info("pubsub_readpacket: len:%d type:%d seq:%d vers:%d\n",
		     ntohl(packet.header.len),
		     ntohs(packet.header.type),
		     ntohl(packet.header.seq),
		     ntohl(packet.header.vers));
	}

	switch (ntohs(packet.header.type)) {
	case PUBSUB_PKT_NOTIFICATION:
#ifdef	THREADED
		return pubsub_packet_enqueue(handle,
					     (packet_t *)&packet, myerror);
#else
		pubsub_notification_dispatch(handle,
					     (notification_packet_t *)&packet,
					     myerror);
#endif
		break;
	case PUBSUB_PKT_SUBSCRIBE:
	case PUBSUB_PKT_UNSUBSCRIBE:
	case PUBSUB_PKT_NOTIFY:
	case PUBSUB_PKT_PING:
		/*
		 * These are reply packets. Find matching
		 * packet and copy data in. 
		 */
		queue_iterate(&handle->reply_packets,
			      pktp, reply_packet_t *, chain) {
			if (pktp->sequence == packet.header.seq) {
				memcpy(&pktp->header,
				       &packet.header, sizeof(packet.header));
				pktp->reply_received = 1;
#ifdef	THREADED
				pthread_cond_broadcast(&handle->priv->replycond);
#endif
				break;
			}
		}
		break;
	}
	return 0;
}

/* 
 * Add a timeout to be called from the main dispatcher loop.
 */
pubsub_timeout_t *
pubsub_add_timeout(pubsub_handle_t *handle,
		   void *context, unsigned int msdelay,
		   pubsub_timeout_callback_t callback,
		   void *callback_arg, pubsub_error_t *myerror)
{
	pubsub_timeout_t	*timeout;
	struct timeval		tv;

	LOCK(handle);
	if ((timeout = (pubsub_timeout_t *)
		  calloc(1, sizeof(pubsub_timeout_t))) == NULL) {
		myerror->msg   = "pubsub_add_timeout: Out of memory";
		myerror->error = ENOMEM;
		UNLOCK(handle);
		return (pubsub_timeout_t *) NULL;
	}
	/*
	 * The elvin API specifies that the timeout happens msdelay from the
	 * time of registration, which seems silly since that could be in the
	 * past by the time the dispatcher it called. But we are tracking the
	 * elvin API as closely as possible, so do it this way.
	 */
	gettimeofday(&timeout->timeout, NULL);
	tv.tv_sec  = msdelay / 1000;
	tv.tv_usec = (msdelay % 1000) * 1000;
	timeradd(&timeout->timeout, &tv, &timeout->timeout);

	timeout->callback     = callback;
	timeout->callback_arg = callback_arg;

	if (queue_empty(&handle->timeouts)) {
		queue_enter(&handle->timeouts, timeout,
			    pubsub_timeout_t *, chain);
	}
	else {
		pubsub_timeout_t *curr_timeout;
		
		queue_iterate(&handle->timeouts, curr_timeout,
			      pubsub_timeout_t *, chain) {
			if (timercmp(&curr_timeout->timeout,
				     &timeout->timeout,
				     >)) {
				break;
			}
		}
		queue_enter_before(&handle->timeouts, curr_timeout, timeout,
				   pubsub_timeout_t *, chain);
	}
	UNLOCK(handle);
	DISPATCHER_INTERRUPT(handle);
	return timeout;
}

/*
 * And remove the timeout
 */
int
pubsub_remove_timeout(pubsub_handle_t *handle,
		      pubsub_timeout_t *timeout, pubsub_error_t *myerror)
{
	LOCK(handle);
	queue_remove(&handle->timeouts, timeout, pubsub_timeout_t *, chain);
	free(timeout);
	UNLOCK(handle);
	DISPATCHER_INTERRUPT(handle);
	return 0;
}

/*
 * Dispatch a timeout.
 */
static int
pubsub_timeout_dispatch(pubsub_handle_t *handle, pubsub_error_t *myerror)
{
	struct timeval now;

	gettimeofday(&now, NULL);
	while (!queue_empty(&handle->timeouts) &&
	       timercmp(&now,
			&((pubsub_timeout_t *)
			  queue_first(&handle->timeouts))->timeout,
			>)) {
		pubsub_timeout_t *ptimeout;

		queue_remove_first(&handle->timeouts, ptimeout,
				   pubsub_timeout_t *, chain);
		UNLOCK(handle);
		if (ptimeout->callback) {
			ptimeout->callback(handle, ptimeout,
					   ptimeout->callback_arg, myerror);
		}
		LOCK(handle);
		free(ptimeout);
	}
	return 0;
}

/*
 * Add IO handlers for specific FDs so that client programs can find out
 * when there is something to do on them (added to the select set in the
 * dispatch loop).
 *
 * TODO: Do something with the mask argument ...
 */
pubsub_iohandler_t *
pubsub_add_iohandler(pubsub_handle_t *handle,
		     void *context, int fd, int mask,
		     pubsub_iohandler_callback_t callback,
		     void *callback_arg, pubsub_error_t *myerror)
{
	pubsub_iohandler_t	*iohandler;

	LOCK(handle);
	if ((iohandler = (pubsub_iohandler_t *)
	     calloc(1, sizeof(pubsub_iohandler_t))) == NULL) {
		myerror->msg   = "pubsub_add_iohandler: Out of memory";
		myerror->error = ENOMEM;
		UNLOCK(handle);
		return (pubsub_iohandler_t *) NULL;
	}
	iohandler->fd           = fd;
	iohandler->select_mask  = mask;
	iohandler->callback     = callback;
	iohandler->callback_arg = callback_arg;

	queue_enter(&handle->iohandlers,
		    iohandler, pubsub_iohandler_t *, chain);

	FD_SET(fd, &handle->sfds);
	if (fd >= handle->maxfds)
		handle->maxfds = fd + 1;
	
	UNLOCK(handle);
	DISPATCHER_INTERRUPT(handle);
	return iohandler;
}

/*
 * And remove the IO handler
 */
int
pubsub_remove_iohandler(pubsub_handle_t *handle,
			pubsub_iohandler_t *iohandler, pubsub_error_t *myerror)
{
	LOCK(handle);
	FD_CLR(iohandler->fd, &handle->sfds);
	queue_remove(&handle->iohandlers,
		     iohandler, pubsub_iohandler_t *, chain);
	free(iohandler);
	UNLOCK(handle);
	DISPATCHER_INTERRUPT(handle);
	return 0;
}

static int
pubsub_iohandler_dispatch(pubsub_handle_t *handle,
			  int fd, pubsub_error_t *myerror)
{
	pubsub_iohandler_t	*iohandler;
	
	/*
	 * A linear search is obviously a bad approach!
	 */
	LOCK(handle);
	queue_iterate(&handle->iohandlers, iohandler,
		      pubsub_iohandler_t *, chain) {
		if (iohandler->fd == fd) {
			/*
			 * This is cause of my braindead locking ...
			 * Need to call the handler with system unlocked,
			 * since the handler might dealloc itself!
			 */
			void	*callback_arg = iohandler->callback_arg;
			pubsub_iohandler_callback_t callback =
				iohandler->callback;
			UNLOCK(handle);
			
			if (callback) {
				callback(handle, iohandler, fd,
					 callback_arg, myerror);
			}
			return 0;
		}
	}
	UNLOCK(handle);
	return 0;
}

static void
idle_packet_callback(pubsub_handle_t *handle,
		     struct reply_packet *pktp, void *arg)
{
	if (pubsub_debug & PUBSUB_DEBUG_CALLBACK) {
		ps_info("Idle packet callback\n");
	}
	
	LOCK(handle);
	handle->idle_waiting = 0;
	free(pktp);
	UNLOCK(handle);
}

/*
 * Idle timer callback.
 */
static int
idle_timeout_callback(pubsub_handle_t *handle,
		      pubsub_timeout_t *timeout, void *data,
		      pubsub_error_t *myerror)
{
	if (pubsub_debug & PUBSUB_DEBUG_CALLBACK) {
		ps_info("Idle timeout callback\n");
	}

	/*
	 * No reply since last ping sent. Close the connection if open.
	 */
	LOCK(handle);
	if (handle->idle_waiting) {
		if (handle->sock >= 0) {
			pubsub_dropconnect(handle, myerror);
		}
	}
	UNLOCK(handle);

	/*
	 * Establish next timeout. 
	 */
	if (pubsub_add_timeout(handle, timeout, handle->idle_period * 1000,
			       idle_timeout_callback, NULL, myerror) < 0) {
		ps_error("reregister idle timeout failed\n");
		return -1;
	}
	LOCK(handle);
	if (!handle->idle_waiting) {
		/*
		 * Send out an async packet ping.
		 */
		packet_t	ping_packet, *pktp = &ping_packet;
		reply_packet_t	*reply_packet;

		if ((reply_packet = (reply_packet_t *)
		     calloc(1, sizeof(reply_packet_t))) == NULL) {
			UNLOCK(handle);
			myerror->msg   = "idle_timeout_cb: Out of memory";
			myerror->error = ENOMEM;
			return -1;
		}
		
		/* Init the packet here. */
		pktp->header.len   = htonl(sizeof(*pktp));
		pktp->header.type  = htons(PUBSUB_PKT_PING);
		pktp->header.flags = 0;
		pktp->header.seq   = htonl(sequence_number++);
		pktp->header.error = 0;
		pktp->header.vers  = htonl(PUBSUB_CUR_VERSION);

		reply_packet->callback = idle_packet_callback;

		/*
		 * Send and then wait for the reply packet in the dispatcher.
		 */
		if (pubsub_send(handle, 0, 1, (packet_t *) pktp,
				reply_packet, myerror) < 0) {
			free(reply_packet);
			UNLOCK(handle);
			return -1;
		}
		handle->idle_waiting = 1;
	}
	UNLOCK(handle);
	return 0;
}

/*
 * These two are internal functions for use by pubsubd.
 */
int
pubsub_daemon_dropconnect(pubsub_handle_t *handle)
{
	LOCK(handle);
	pubsub_dropconnect(handle, NULL);
	UNLOCK(handle);

	return 0;
}

int
pubsub_daemon_reconnect(pubsub_handle_t *handle, pubsub_error_t *myerror)
{
	while (handle->sock < 0) {
		LOCK(handle);
		pubsub_reconnect(handle, myerror);
		UNLOCK(handle);
		sleep(5);
	}
	return 0;
}

int
pubsub_set_sockbufsizes(int sendsockbufsize, int recvsockbufsize)
{
	if (sendsockbufsize) {
		pubsub_send_sockbufsize = sendsockbufsize;
	}
	if (recvsockbufsize) {
		pubsub_recv_sockbufsize = recvsockbufsize;
	}
	return 0;
}
