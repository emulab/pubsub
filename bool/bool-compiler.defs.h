/* bool-compiler.def.h */

#define INCLUDE_BOOL_H #include "bool.h"

#define SHORTCUT_OP_CREATE_NODE(COMMAND,SHORTCUT_COMMAND) \
  parseNodeT * shortcut = createParseNode($1, NULL); \
  if (parseResult.type != NO_ERROR) \
  { \
    YYERROR; \
  } \
  shortcut->instruction.op = SHORTCUT_COMMAND; \
  /* Jump past $3 and then past the COMMAND */ \
  shortcut->instruction.arg.jumpSize = $3->census + 1; \
  $$ = createParseNode(shortcut, $3); \
  if (parseResult.type != NO_ERROR) \
  { \
    YYERROR; \
  } \
  $$->instruction.op = COMMAND;

#define BINARY_OP_CREATE_NODE(COMMAND) \
  $$ = createParseNode($1, $3); \
  if (parseResult.type != NO_ERROR) \
  { \
    YYERROR; \
  } \
  $$->instruction.op = COMMAND;

#define UNARY_OP_CREATE_NODE(COMMAND) \
  $$ = createParseNode($2, NULL); \
  if (parseResult.type != NO_ERROR) \
  { \
    YYERROR; \
  } \
  $$->instruction.op = COMMAND;

