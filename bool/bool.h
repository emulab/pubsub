/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2016 University of Utah and the Flux Group.
 * All rights reserved.
 */

/* bool.h */

#ifndef _bool_h
#define _bool_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "pubsub.h"

typedef pubsub_type_t typeT;

typedef int int32T;
typedef long long int64T;
typedef double real64T;
typedef char * stringT;

typedef enum
{
  // Push commands.
  BEGIN_PUSH_RANGE,
  PUSH_VARIABLE_COMMAND,
  PUSH_INT32_LITERAL_COMMAND,
  PUSH_INT64_LITERAL_COMMAND,
  PUSH_REAL64_LITERAL_COMMAND,
  PUSH_STRING_LITERAL_COMMAND,
  END_PUSH_RANGE,

  // Binary commands
  BEGIN_BINARY_RANGE,
  OR_COMMAND,
  XOR_COMMAND,
  AND_COMMAND,
  BITWISE_LEFT_COMMAND,
  BITWISE_SIGNED_RIGHT_COMMAND,
  BITWISE_ZERO_RIGHT_COMMAND,
  END_BINARY_RANGE,

  // Numeric commands where numeric values are promoted as needed.
  BEGIN_PROMOTION_RANGE,
  MULTIPLY_COMMAND,
  DIVIDE_COMMAND,
  MODULUS_COMMAND,
  ADD_COMMAND,
  SUBTRACT_COMMAND,
  LESS_THAN_COMMAND,
  GREATER_THAN_COMMAND,
  LESS_THAN_EQUAL_COMMAND,
  GREATER_THAN_EQUAL_COMMAND,
  EQUALITY_COMMAND,
  BITWISE_AND_COMMAND,
  BITWISE_XOR_COMMAND,
  BITWISE_OR_COMMAND,
  END_PROMOTION_RANGE,

  // Unary commands
  BEGIN_UNARY_RANGE,
  NOT_COMMAND,
  BITWISE_NOT_COMMAND,
  NEGATE_COMMAND,
  END_UNARY_RANGE,

  // Shortcut commands
  BEGIN_SHORTCUT_RANGE,
  JUMP_IF_TRUE_COMMAND,
  JUMP_IF_FALSE_COMMAND,
  JUMP_IF_BOTTOM_COMMAND,
  END_SHORTCUT_RANGE
} commandT;

typedef struct
{
  commandT op;
  /* The type of arg is determined by op:
     PUSH_VARIABLE_COMMAND --> index into symbolTable
     PUSH_INT32_LITERAL_COMMAND --> int32
     PUSH_INT64_LITERAL_COMMAND --> int64
     PUSH_REAL64_LITERAL_COMMAND --> real64
     PUSH_STRING_LITERAL_COMMAND --> index into stringTable
     other --> ignored
   */
  union
  {
    int index;
    // Used in shortcut evaluation. Number of commands to jump.
    int jumpSize;
    int32T int32;
    int64T int64;
    real64T real64;
  } arg;
} instructionT;

typedef enum
{
  /* Follow the chart in Chapter 2 of the spec. */
  TERN_TRUE = 0,
  TERN_BOTTOM = 1,
  TERN_FALSE = 2,
} ternaryBoolT;

typedef struct
{
  typeT type;
  union
  {
    ternaryBoolT ternary;
    int32T int32;
    int64T int64;
    real64T real64;
    stringT string;
  } value;
} memoryT;

typedef struct
{
  /* The amount of stack space this program will require. */
  int maxStackSize;

  /* This structure contains dynamic memory. Allocate it with
     createProgram() or loadProgram() and de-allocate it with
     destroyProgram() */

  /* The string table is a character array consisting of all the
     strings and identifiers used in the instructionList separated by
     '\0's. */
  char * stringTable;
  int stringTableSize;

  /* The symbol table is an array of unique identifiers. Each
     entry stores an index into the string table where the label of
     the identifier is. This extra layer of indirection allows us to
     build a table of actual identifier values during actual
     evaluation */
  int * symbolTable;
  int symbolTableSize;

  instructionT * instructionList;
  int instructionListSize;
} programT;

/* Holds the 'variable layout' for a program. */
typedef struct
{
  int boundCount;
  programT * program;
  memoryT * memoryTable;
} bindingsT;

typedef enum
{
  NO_ERROR,
  SYNTAX_ERROR,
  OUT_OF_MEMORY_ERROR
} errorClassT;

typedef struct
{
  errorClassT type;
} errorT;

/*
 * This structure will hold all of the bindings associated with a
 * notification that we can pass off to bind all variables once per
 * subscription. We traverse the notification just once, since a typical
 * notification has about a dozen terms, but a typical subscription has
 * just a couple. Previously we traversed the note 100s of times doing
 * exactly the same thing, calling bindVariable() which searched for a
 * match in the variable names. Now we reverse the operation, going through
 * much smaller list of variables names each time, looking for a match in
 * notification bindings.
 */
struct note_binding {
  const char	*name;		/* From the notification */
  memoryT	binding;
};
typedef struct note_binding note_binding_t;

/* Our average is 15 ... */
#define MAX_NOTE_BINDINGS 128

struct note_bindings {
  int		  count;	/* Count of bound variables in the array */
  note_binding_t  bindings[MAX_NOTE_BINDINGS];
};
typedef struct note_bindings note_bindings_t;

/* Creates a program by compiling the null-terminated string pointed
   to by code. The compilation output is put in programOut
   destructively. It is your responsibility to call destroyProgram()
   on any program created by createProgram() or loadProgram() before
   calling one of these functions again. If there is an error,
   information about it is returned in errorT */
errorT createProgram(char const * code, programT * programOut);

/* Loads a previously saved program from bytes. See createProgram()
   for details. If there is an error, information about it is returned
   in errorT */
errorT loadProgram(char const * bytes, size_t size, programT * programOut);

/* De-allocates memory allocated by a previous call to createProgram()
   or loadProgram() */
void destroyProgram(programT * program);

/* Create the memory space for the variables referenced by a program. */
errorT createBindings(programT * program, bindingsT * bindingsOut);

/* De-allocates memory allocated by a previous call to createBindings() */
void destroyBindings(bindingsT * bindings);

/* Clear the variable values in bindings by setting them to TERN_BOTTOM. */
void clearBindings(bindingsT * bindings);

/* Bind a named value to a typed value. */
void bindVariable(bindingsT * bindings, const char * name, memoryT value);

void bindAllVariables(bindingsT * bindings, note_bindings_t * bound_variables);

/* Evaluate a program against the given bindings. */
ternaryBoolT evalBindings(bindingsT * bindings);

#endif
