%{
/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2011 University of Utah and the Flux Group.
 * All rights reserved.
 */

/* bool-compiler.y */

#include "bool-compiler.defs.h"

INCLUDE_BOOL_H

int yyerror(char *);
int yylex(void);
int yyparse(void);

typedef struct
{
  /* pos points to the middle of the string, so size does not include
     a trailing null character. */
  char const * pos;
  int size;
} tokenT;

typedef struct stringNodeStruct
{
  tokenT string;
  int stringIndex;
  struct stringNodeStruct * next;
} stringNodeT;

typedef struct symbolNodeStruct
{
  int stringIndex;
  tokenT token;
  struct symbolNodeStruct * next;
} symbolNodeT;

typedef struct parseNodeStruct
{
  /* When we spit out our compiled parse tree, we want to find out how
     many bytes it will require. We update this as the parse happens.
     census is the number of instructions in this sub-tree. */

  /* Note that later this can be used for shortcut evaluation on &&
     and || */
  int census;

  /* The maximum stack size consumed by this sub-tree. */
  int maxStackSize;

  /* Linked list for deletion after parsing */
  struct parseNodeStruct * next;
  /* Children, if this is an operation. */
  struct parseNodeStruct * left;
  struct parseNodeStruct * right;

  /* What to do. The instructions are ordered into a RPN stream after
     parsing. */
  instructionT instruction;
} parseNodeT;

/* Returns the newly created parseNodeT. The instruction field still
   needs to be initialized after this call. */
static parseNodeT * createParseNode(parseNodeT * left, parseNodeT * right);
/* Returns the index that the string will have during program execution. */
static int createStringNode(tokenT token);
/* Returns the index that will be used to access the symbol in the
   symbolTable during execution */
static int createSymbolNode(tokenT token);

/* Cleans up all nodes created during the parse and fills in the
   programOut structure with the data. */
static void destroyAllNodes(programT * programOut);

/* Helper function for destroyAllNodes. Visits and copies instructions
   in post-order */
static void postOrderParse(parseNodeT * root, instructionT * * pos);

static parseNodeT * parseHead;
static parseNodeT * parseTail;

static parseNodeT * parseRoot;

static stringNodeT * stringTableHead;
static stringNodeT * stringTableTail;
static int stringCharCount;

static symbolNodeT * symbolTableHead;
static symbolNodeT * symbolTableTail;
static int symbolCount;

static errorT parseResult;

static char const * lexBegin;
static int lexNext;

%}

%union
{
  tokenT terminal;
  parseNodeT * nonTerminal;
}

%start start

%token <terminal> IDENTIFIER
%token <terminal> STRING_LITERAL
%token <terminal> INT32_LITERAL
%token <terminal> INT64_LITERAL
%token <terminal> REAL64_LITERAL
%token <terminal> LEFT_PAREN
%token <terminal> RIGHT_PAREN

%token <terminal> NOT_OP BITWISE_NOT_OP

%token <terminal> MULTIPLY_OP DIVIDE_OP MODULUS_OP
%token <terminal> ADD_OP SUBTRACT_OP
%token <terminal> BITWISE_LEFT_OP BITWISE_SIGNED_RIGHT_OP
%token <terminal>   BITWISE_ZERO_RIGHT_OP

%token <terminal> BITWISE_AND_OP
%token <terminal> BITWISE_XOR_OP
%token <terminal> BITWISE_OR_OP

%token <terminal> LESS_THAN_OP GREATER_THAN_OP LESS_THAN_EQUAL_OP
%token <terminal>   GREATER_THAN_EQUAL_OP
%token <terminal> EQUALITY_OP NOT_EQUALITY_OP

%token <terminal> AND_OP
%token <terminal> XOR_OP
%token <terminal> OR_OP

%type <nonTerminal> start exp or_exp xor_exp and_exp comparison_exp
%type <nonTerminal> bitwise_or_exp bitwise_xor_exp bitwise_and_exp
%type <nonTerminal> bitwise_shift_exp add_exp multiply_exp unary_exp
%type <nonTerminal> primary_exp

%%

start
  : exp
    {
      $$ = $1;
      parseRoot = $$;
    }
;
exp
  : or_exp { $$ = $1; }
;

or_exp
  : xor_exp { $$ = $1; }
  | or_exp OR_OP xor_exp
    { SHORTCUT_OP_CREATE_NODE(OR_COMMAND,JUMP_IF_TRUE_COMMAND); }
;

xor_exp
  : and_exp { $$ = $1; }
  | xor_exp XOR_OP and_exp
    { SHORTCUT_OP_CREATE_NODE(XOR_COMMAND,JUMP_IF_BOTTOM_COMMAND); }

and_exp
  : comparison_exp { $$ = $1; }
  | and_exp AND_OP comparison_exp
    { SHORTCUT_OP_CREATE_NODE(AND_COMMAND,JUMP_IF_FALSE_COMMAND); }
 ;

comparison_exp
  : bitwise_or_exp { $$ = $1; }
  | comparison_exp LESS_THAN_OP bitwise_or_exp
    { BINARY_OP_CREATE_NODE(LESS_THAN_COMMAND); }
  | comparison_exp GREATER_THAN_OP bitwise_or_exp
    { BINARY_OP_CREATE_NODE(GREATER_THAN_COMMAND); }
  | comparison_exp LESS_THAN_EQUAL_OP bitwise_or_exp
    { BINARY_OP_CREATE_NODE(LESS_THAN_EQUAL_COMMAND); }
  | comparison_exp GREATER_THAN_EQUAL_OP bitwise_or_exp
    { BINARY_OP_CREATE_NODE(GREATER_THAN_EQUAL_COMMAND); }
  | comparison_exp EQUALITY_OP bitwise_or_exp
    { BINARY_OP_CREATE_NODE(EQUALITY_COMMAND); }
  | comparison_exp NOT_EQUALITY_OP bitwise_or_exp
    {
      parseNodeT * equality = createParseNode($1, $3);
      if (parseResult.type != NO_ERROR)
      {
        YYERROR;
      }
      equality->instruction.op = EQUALITY_COMMAND;
      $$ = createParseNode(equality, NULL);
      if (parseResult.type != NO_ERROR)
      {
        YYERROR;
      }
      $$->instruction.op = NOT_COMMAND;
    }
;

bitwise_or_exp
  : bitwise_xor_exp { $$ = $1; }
  | bitwise_or_exp BITWISE_OR_OP bitwise_xor_exp
    { BINARY_OP_CREATE_NODE(BITWISE_OR_COMMAND); }
;

bitwise_xor_exp
  : bitwise_and_exp { $$ = $1; }
  | bitwise_xor_exp BITWISE_XOR_OP bitwise_and_exp
    { BINARY_OP_CREATE_NODE(BITWISE_XOR_COMMAND); }
;

bitwise_and_exp
  : bitwise_shift_exp { $$ = $1; }
  | bitwise_and_exp BITWISE_AND_OP bitwise_shift_exp
    { BINARY_OP_CREATE_NODE(BITWISE_AND_COMMAND); }
;

bitwise_shift_exp
  : add_exp { $$ = $1; }
  | bitwise_shift_exp BITWISE_LEFT_OP add_exp
    { BINARY_OP_CREATE_NODE(BITWISE_LEFT_COMMAND); }
  | bitwise_shift_exp BITWISE_SIGNED_RIGHT_OP add_exp
    { BINARY_OP_CREATE_NODE(BITWISE_SIGNED_RIGHT_COMMAND); }
  | bitwise_shift_exp BITWISE_ZERO_RIGHT_OP add_exp
    { BINARY_OP_CREATE_NODE(BITWISE_ZERO_RIGHT_COMMAND); }
;

add_exp
  : multiply_exp { $$ = $1; }
  | add_exp ADD_OP multiply_exp
    { BINARY_OP_CREATE_NODE(ADD_COMMAND); }
  | add_exp SUBTRACT_OP multiply_exp
    { BINARY_OP_CREATE_NODE(SUBTRACT_COMMAND); }
;

multiply_exp
  : unary_exp { $$ = $1; }
  | multiply_exp MULTIPLY_OP unary_exp
    { BINARY_OP_CREATE_NODE(MULTIPLY_COMMAND); }
  | multiply_exp DIVIDE_OP unary_exp
    { BINARY_OP_CREATE_NODE(DIVIDE_COMMAND); }
  | multiply_exp MODULUS_OP unary_exp
    { BINARY_OP_CREATE_NODE(MODULUS_COMMAND); }
;

unary_exp
  : primary_exp { $$ = $1; }
  | NOT_OP unary_exp { UNARY_OP_CREATE_NODE(NOT_COMMAND); }
  | BITWISE_NOT_OP unary_exp { UNARY_OP_CREATE_NODE(BITWISE_NOT_COMMAND); }
  | SUBTRACT_OP unary_exp { UNARY_OP_CREATE_NODE(NEGATE_COMMAND); }
;

primary_exp
  : LEFT_PAREN exp RIGHT_PAREN
    {
      $$ = $2;
    }
  | IDENTIFIER
    {
      $$ = createParseNode(NULL, NULL);
      if (parseResult.type != NO_ERROR)
      {
        YYERROR;
      }
      $$->instruction.op = PUSH_VARIABLE_COMMAND;
      $$->instruction.arg.index = createSymbolNode($1);
      if (parseResult.type != NO_ERROR)
      {
        YYERROR;
      }
    }
  | STRING_LITERAL
    {
      $$ = createParseNode(NULL, NULL);
      if (parseResult.type != NO_ERROR)
      {
        YYERROR;
      }
      $$->instruction.op = PUSH_STRING_LITERAL_COMMAND;
      $$->instruction.arg.index = createStringNode($1);
      if (parseResult.type != NO_ERROR)
      {
        YYERROR;
      }
    }
  | INT32_LITERAL
    {
      $$ = createParseNode(NULL, NULL);
      if (parseResult.type != NO_ERROR)
      {
        YYERROR;
      }
      $$->instruction.op = PUSH_INT32_LITERAL_COMMAND;
      $$->instruction.arg.int32 = strtol($1.pos, NULL, 0);
    }
  | INT64_LITERAL
    {
      $$ = createParseNode(NULL, NULL);
      if (parseResult.type != NO_ERROR)
      {
        YYERROR;
      }
      $$->instruction.op = PUSH_INT64_LITERAL_COMMAND;
      $$->instruction.arg.int64 = strtoll($1.pos, NULL, 0);
    }
  | REAL64_LITERAL
    {
      $$ = createParseNode(NULL, NULL);
      if (parseResult.type != NO_ERROR)
      {
        YYERROR;
      }
      $$->instruction.op = PUSH_REAL64_LITERAL_COMMAND;
      $$->instruction.arg.real64 = atof($1.pos);
    }
;

%%

errorT createProgram(char const * code, programT * programOut)
{
  parseHead = NULL;
  parseTail = NULL;
  parseRoot = NULL;
  stringTableHead = NULL;
  stringTableTail = NULL;
  stringCharCount = 0;
  symbolTableHead = NULL;
  symbolTableTail = NULL;
  symbolCount = 0;
  parseResult.type = NO_ERROR;
  lexBegin = code;
  lexNext = 0;

  yyparse();
  destroyAllNodes(programOut);

  return parseResult;
}

void destroyProgram(programT * program)
{
  program->maxStackSize = 0;
  if (program->stringTable != NULL)
  {
    free(program->stringTable);
  }
  program->stringTable = NULL;
  program->stringTableSize = 0;

  if (program->symbolTable != NULL)
  {
    free(program->symbolTable);
  }
  program->symbolTable = NULL;
  program->symbolTableSize = 0;

  if (program->instructionList != NULL)
  {
    free(program->instructionList);
  }
  program->instructionList = NULL;
  program->instructionListSize = 0;
}

/* Returns the newly created parseNodeT. The instruction field still
   needs to be initialized after this call. */
static parseNodeT * createParseNode(parseNodeT * left, parseNodeT * right)
{
  int leftStack = 0;
  int rightStack = 0;
  parseNodeT * newNode = (parseNodeT *)malloc(sizeof(parseNodeT));
  if (newNode == NULL)
  {
    parseResult.type = OUT_OF_MEMORY_ERROR;
    return NULL;
  }
  newNode->census = 1;
  if (left != NULL)
  {
    newNode->census += left->census;
    leftStack = left->maxStackSize;
  }
  if (right != NULL)
  {
    newNode->census += right->census;
    rightStack = right->maxStackSize;
  }

  if (leftStack > rightStack + 1)
  {
    newNode->maxStackSize = leftStack;
  }
  else
  {
    newNode->maxStackSize = rightStack;
  }

  newNode->next = NULL;
  newNode->left = left;
  newNode->right = right;

  if (parseTail != NULL)
  {
    parseTail->next = newNode;
    parseTail = newNode;
  }
  else
  {
    parseHead = newNode;
    parseTail = newNode;
  }

  return newNode;
}

/* Returns the index that the string will have during program execution. */
static int createStringNode(tokenT token)
{
  stringNodeT * newNode = (stringNodeT *)malloc(sizeof(stringNodeT));
  if (newNode == NULL)
  {
    parseResult.type = OUT_OF_MEMORY_ERROR;
    return 0;
  }
  newNode->string = token;
  newNode->stringIndex = stringCharCount;
  newNode->next = NULL;

  if (stringTableTail != NULL)
  {
    stringTableTail->next = newNode;
    stringTableTail = newNode;
  }
  else
  {
    stringTableHead = newNode;
    stringTableTail = newNode;
  }

  /* Add the count plus one for a null terminator */
  stringCharCount += token.size + 1;
  return newNode->stringIndex;
}

/* Returns the index that will be used to access the symbol in the
   symbolTable during execution */
static int createSymbolNode(tokenT token)
{
  /* TODO: Replace this with a more efficient search */

  /* If the symbol already exists, return that index. Otherwise,
     generate a new one. */
  int index = 0;
  symbolNodeT * pos = symbolTableHead;
  while (pos != NULL)
  {
    if (token.size == pos->token.size &&
        strncmp(pos->token.pos, token.pos, token.size) == 0)
    {
      break;
    }
    ++index;
    pos = pos->next;
  }
  if (pos == NULL)
  {
    /* Search failed. Add a new node. */
    symbolNodeT * newNode = (symbolNodeT *)malloc(sizeof(symbolNodeT));
    if (newNode == NULL)
    {
      parseResult.type = OUT_OF_MEMORY_ERROR;
      return 0;
    }
    newNode->stringIndex = createStringNode(token);
    if (parseResult.type != NO_ERROR)
    {
      return 0;
    }
    newNode->token = token;
    newNode->next = NULL;

    if (symbolTableTail != NULL)
    {
      symbolTableTail->next = newNode;
      symbolTableTail = newNode;
    }
    else
    {
      symbolTableHead = newNode;
      symbolTableTail = newNode;
    }

    index = symbolCount;
    ++symbolCount;

  }
  /* Whether the search has succeeded or failed, index now points to
     the match or new node. */
  return index;
}

/* Cleans up all nodes created during the parse and fills in the
   programOut structure with the data. */
static void destroyAllNodes(programT * programOut)
{
  parseNodeT * parsePos = NULL;
  stringNodeT * stringPos = NULL;
  symbolNodeT * symbolPos = NULL;
  instructionT * instructionPos = NULL;
  int i = 0;
  if (parseResult.type == NO_ERROR)
  {
    programOut->maxStackSize = parseRoot->maxStackSize;

    /* Set up the string table */
    programOut->stringTable = (char *)malloc(stringCharCount);
    programOut->stringTableSize = stringCharCount;
    stringPos = stringTableHead;
    while (stringPos != NULL)
    {
      // TODO: Handle escape sequences.
      memcpy(programOut->stringTable + stringPos->stringIndex,
             stringPos->string.pos,
             stringPos->string.size);
      programOut->stringTable[stringPos->stringIndex
                              + stringPos->string.size] = '\0';
      stringPos = stringPos->next;
    }

    /* Set up the symbol table */
    programOut->symbolTable = (int *)malloc(symbolCount * sizeof(int));
    programOut->symbolTableSize = symbolCount;
    i = 0;
    symbolPos = symbolTableHead;
    while (symbolPos != NULL)
    {
      programOut->symbolTable[i] = symbolPos->stringIndex;
      ++i;
      symbolPos = symbolPos->next;
    }

    /* Set up the instruction list */
    programOut->instructionList = (instructionT *)malloc(parseRoot->census
                                                      * sizeof(instructionT));
    programOut->instructionListSize = parseRoot->census;
    /* We do a post-order traversal */
    i = 0;
    instructionPos = programOut->instructionList;
    postOrderParse(parseRoot, &instructionPos);
  }
  else
  {
    programOut->maxStackSize = 0;
    programOut->stringTable = NULL;
    programOut->stringTableSize = 0;
    programOut->symbolTable = NULL;
    programOut->symbolTableSize = 0;
    programOut->instructionList = NULL;
    programOut->instructionListSize = 0;
  }
  /* Free all memory used in compilation. */
  while (parseHead != NULL)
  {
    parsePos = parseHead->next;
    free(parseHead);
    parseHead = parsePos;
  }
  while (stringTableHead != NULL)
  {
    stringPos = stringTableHead->next;
    free(stringTableHead);
    stringTableHead = stringPos;
  }
  while (symbolTableHead != NULL)
  {
    symbolPos = symbolTableHead->next;
    free(symbolTableHead);
    symbolTableHead = symbolPos;
  }
}

static void postOrderParse(parseNodeT * root, instructionT * * pos)
{
  if (root != NULL)
  {
    postOrderParse(root->left, pos);
    postOrderParse(root->right, pos);

    **pos = root->instruction;
    ++(*pos);
  }
}

int yyerror(char * s)
{
  if (parseResult.type == NO_ERROR)
  {
    parseResult.type = SYNTAX_ERROR;
  }
  return 0;
}

int yylex()
{
  int type = 0;
  char const * futureBegin = lexBegin;
  while (isspace(lexBegin[lexNext]))
  {
    ++lexBegin;
  }
  if (isalpha(lexBegin[lexNext]) || lexBegin[lexNext] == '_')
  {
    type = IDENTIFIER;
    while (isalpha(lexBegin[lexNext]) || lexBegin[lexNext] == '_'
           || isdigit(lexBegin[lexNext]))
    {
      ++lexNext;
    }
    futureBegin = lexBegin + lexNext;
  }
  else if (isdigit(lexBegin[lexNext]) && lexBegin[lexNext+1] == 'x')
  {
    /* Hex number */
    lexNext += 2;
    while (isdigit(lexBegin[lexNext])
           || (toupper(lexBegin[lexNext]) >= 'A'
               && toupper(lexBegin[lexNext]) <= 'F'))
    {
      ++lexNext;
    }
    if (lexBegin[lexNext] == 'l' || lexBegin[lexNext] == 'L')
    {
      type = INT64_LITERAL;
      futureBegin = lexBegin + lexNext + 1;
    }
    else
    {
      type = INT32_LITERAL;
      futureBegin = lexBegin + lexNext;
    }
  }
  else if (isdigit(lexBegin[lexNext]))
  {
    /* Decimal number */
    while (isdigit(lexBegin[lexNext]))
    {
      ++lexNext;
    }
    if (lexBegin[lexNext] == '.' || lexBegin[lexNext] == 'e'
        || lexBegin[lexNext] == 'E')
    {
      if (lexBegin[lexNext] == '.')
      {
        ++lexNext;
        while (isdigit(lexBegin[lexNext]))
        {
          ++lexNext;
        }
      }
      if (lexBegin[lexNext] == 'e' || lexBegin[lexNext] == 'E')
      {
        ++lexNext;
        if (lexBegin[lexNext] == '+' || lexBegin[lexNext] == '-')
        {
          ++lexNext;
        }
        while (isdigit(lexBegin[lexNext]))
        {
          ++lexNext;
        }
      }
      type = REAL64_LITERAL;
      futureBegin = lexBegin + lexNext;
    }
    else if (lexBegin[lexNext] == 'l' || lexBegin[lexNext] == 'L')
    {
      type = INT64_LITERAL;
      futureBegin = lexBegin + lexNext + 1;
    }
    else
    {
      type = INT32_LITERAL;
      futureBegin = lexBegin + lexNext;
    }
  }
  else if (lexBegin[lexNext] == '"')
  {
    type = STRING_LITERAL;
    ++lexBegin;
    while (lexBegin[lexNext] != '"' && lexBegin[lexNext] != '\0')
    {
      if (lexBegin[lexNext] == '\\' && lexBegin[lexNext+1] != '\0')
      {
        ++lexNext;
      }
      ++lexNext;
    }
    futureBegin = lexBegin + lexNext + 1;
  }
  else
  {
    switch (lexBegin[lexNext])
    {
    case '|':
      ++lexNext;
      if (lexBegin[lexNext] == '|')
      {
        ++lexNext;
        type = OR_OP;
      }
      else
      {
        type = BITWISE_OR_OP;
      }
      break;
    case '^':
      ++lexNext;
      if (lexBegin[lexNext] == '^')
      {
        ++lexNext;
        type = XOR_OP;
      }
      else
      {
        type = BITWISE_XOR_OP;
      }
      break;
    case '&':
      ++lexNext;
      if (lexBegin[lexNext] == '&')
      {
        ++lexNext;
        type = AND_OP;
      }
      else
      {
        type = BITWISE_AND_OP;
      }
      break;
    case '<':
      ++lexNext;
      if (lexBegin[lexNext] == '=')
      {
        ++lexNext;
        type = LESS_THAN_EQUAL_OP;
      }
      else if (lexBegin[lexNext] == '<')
      {
        ++lexNext;
        type = BITWISE_LEFT_OP;
      }
      else
      {
        type = LESS_THAN_OP;
      }
      break;
    case '>':
      ++lexNext;
      if (lexBegin[lexNext] == '=')
      {
        ++lexNext;
        type = GREATER_THAN_EQUAL_OP;
      }
      else if (lexBegin[lexNext] == '>')
      {
        ++lexNext;
        if (lexBegin[lexNext] == '>')
        {
          ++lexNext;
          type = BITWISE_ZERO_RIGHT_OP;
        }
        else
        {
          type = BITWISE_SIGNED_RIGHT_OP;
        }
      }
      else
      {
        type = GREATER_THAN_OP;
      }
      break;
    case '*':
      ++lexNext;
      type = MULTIPLY_OP;
      break;
    case '/':
      ++lexNext;
      type = DIVIDE_OP;
      break;
    case '%':
      ++lexNext;
      type = MODULUS_OP;
      break;
    case '+':
      ++lexNext;
      type = ADD_OP;
      break;
    case '-':
      ++lexNext;
      type = SUBTRACT_OP;
      break;
    case '=':
      ++lexNext;
      if (lexBegin[lexNext] == '=')
      {
        ++lexNext;
        type = EQUALITY_OP;
      }
      break;
    case '!':
      ++lexNext;
      if (lexBegin[lexNext] == '=')
      {
        ++lexNext;
        type = NOT_EQUALITY_OP;
      }
      else
      {
        type = NOT_OP;
      }
      break;
    case '~':
      ++lexNext;
      type = BITWISE_NOT_OP;
      break;
    case '(':
      ++lexNext;
      type = LEFT_PAREN;
      break;
    case ')':
      ++lexNext;
      type = RIGHT_PAREN;
      break;
    case '\0':
      type = 0;
      break;
    }
    futureBegin = lexBegin + lexNext;
  }
  yylval.terminal.pos = lexBegin;
  yylval.terminal.size = lexNext;
  lexBegin = futureBegin;
  lexNext = 0;
  return type;
}
