/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2016 University of Utah and the Flux Group.
 * All rights reserved.
 */

/* bool-interpreter.c */

#include <assert.h>
#include <inttypes.h>
#include <stdarg.h>

#include "bool.h"

#define ternToIndex(a,b) (((a) << 2) | (b))

static ternaryBoolT ternaryNotTable[3] =
  { TERN_FALSE, TERN_BOTTOM, TERN_TRUE };

static ternaryBoolT ternaryAndTable[12] =
  { TERN_TRUE,   TERN_BOTTOM, TERN_FALSE, TERN_BOTTOM,
    TERN_BOTTOM, TERN_BOTTOM, TERN_FALSE, TERN_BOTTOM,
    TERN_FALSE,  TERN_FALSE,  TERN_FALSE, TERN_BOTTOM };

static ternaryBoolT ternaryOrTable[12] =
  { TERN_TRUE, TERN_TRUE,   TERN_TRUE,   TERN_BOTTOM,
    TERN_TRUE, TERN_BOTTOM, TERN_BOTTOM, TERN_BOTTOM,
    TERN_TRUE, TERN_BOTTOM, TERN_FALSE,  TERN_BOTTOM };

static ternaryBoolT ternaryXorTable[12] =
  { TERN_FALSE,  TERN_BOTTOM, TERN_TRUE,   TERN_BOTTOM,
    TERN_BOTTOM, TERN_BOTTOM, TERN_BOTTOM, TERN_BOTTOM,
    TERN_TRUE,   TERN_BOTTOM, TERN_FALSE,  TERN_BOTTOM };


#define TERNARY_NOT(a) (ternaryNotTable[a])
#define TERNARY_AND(a,b) (ternaryAndTable[ternToIndex(a,b)])
#define TERNARY_OR(a,b) (ternaryOrTable[ternToIndex(a,b)])
#define TERNARY_XOR(a,b) (ternaryXorTable[ternToIndex(a,b)])

enum { stackCapacity = 1000 };
static memoryT stack[stackCapacity];
/* stackIndex is one above the top valid value on the stack */
static int stackIndex = 0;

static ternaryBoolT eval(programT * program, memoryT * bindings);
static void runPushCommand(instructionT * instruction,
                           programT * program, memoryT * bindings);
static void runBinaryCommand(instructionT * instruction,
                             memoryT left, memoryT right);
static void runPromotionCommand(instructionT * instruction,
                                memoryT left, memoryT right);
static void runUnaryCommand(instructionT * instruction, memoryT arg);
/* Returns the number of extra instructions to jump ahead. 0 means
   that the next instruction will be executed. */
static int runShortcutCommand(instructionT * instruction, memoryT arg);

/* If left and right are numeric, return the value of left promoted to
   type max(left,right) where real64 > int64 > int32. Otherwise,
   return left. */
static memoryT promoteLeft(memoryT left, memoryT right);

/* Compare two strings, returning 0, less than 0, or greater than 0,
   if left is found to be less than, match, or be greater than
   right. Escape sequences are treated as synonymous with the
   characters they represent. Escape sequences are '\n' '\t' '\r' '\\'
   '\"' and '\''*/
static int strcmpesc(char const * left, char const * right);

errorT createBindings(programT * program, bindingsT * bindingsOut)
{
  errorT retval = { NO_ERROR };

  bindingsOut->boundCount = 0;
  bindingsOut->program = program;
  bindingsOut->memoryTable = (memoryT *)malloc(program->symbolTableSize
                                               * sizeof(memoryT));

  if (bindingsOut->memoryTable == NULL)
  {
    retval.type = OUT_OF_MEMORY_ERROR;
  }

  return retval;
}

void destroyBindings(bindingsT * bindings)
{
  bindings->program = NULL;
  if (bindings->memoryTable != NULL)
  {
    free(bindings->memoryTable);
  }
  bindings->memoryTable = NULL;
}

void clearBindings(bindingsT * bindings)
{
  int i;

  /* Reset counter to 0. */
  bindings->boundCount = 0;
  /* Initialize all bindings to BOTTOM. The spec says that if a
     binding is not found, then it results in BOTTOM. */
  for (i = 0; i < bindings->program->symbolTableSize; ++i)
  {
    bindings->memoryTable[i].type = TERNARY_TYPE;
    bindings->memoryTable[i].value.ternary = TERN_BOTTOM;
  }
}

void bindAllVariables(bindingsT * bindings, note_bindings_t * bound_variables)
{
  programT * program = bindings->program;
  int i, j;

  /* Reset counter to 0. */
  bindings->boundCount = 0;

  for (i = 0; i < program->symbolTableSize; i++)
  {
    char * potential = program->stringTable + program->symbolTable[i];

    /* Initialize all bindings to BOTTOM. The spec says that if a
       binding is not found, then it results in BOTTOM. */
    bindings->memoryTable[i].type = TERNARY_TYPE;
    bindings->memoryTable[i].value.ternary = TERN_BOTTOM;

    for (j = 0; j < bound_variables->count; j++)
    {
      if (strcmp(potential, bound_variables->bindings[j].name) == 0)
      {
        bindings->memoryTable[i] = bound_variables->bindings[j].binding;
        ++bindings->boundCount;
        break;
      } 
    }
  }
}

void bindVariable(bindingsT * bindings, const char * name, memoryT value)
{
  programT * program = bindings->program;
  int i;

  for (i = 0; i < program->symbolTableSize; i++)
  {
    char * potential = program->stringTable + program->symbolTable[i];

    if (strcmp(name, potential) == 0)
    {
      bindings->memoryTable[i] = value;
      ++bindings->boundCount;
      break;
    }
  }
}

ternaryBoolT evalBindings(bindingsT * bindings)
{
  programT * program = bindings->program;
  ternaryBoolT retval;

  if (bindings->boundCount == bindings->program->symbolTableSize)
  {
    retval = eval(program, bindings->memoryTable);
  }
  else
  {
    retval = TERN_BOTTOM;
  }

  return retval;
}

static ternaryBoolT eval(programT * program, memoryT * bindings)
{
  int programCounter = 0;
  memoryT result;

  stackIndex = 0; // XXX This seems to grow after every call, need to reset...

  if (program->maxStackSize > stackCapacity)
  {
    /* TODO: Make stack dynamic & re-entrant */
    return TERN_BOTTOM;
  }
  while (programCounter < program->instructionListSize)
  {
    instructionT * instruction = program->instructionList + programCounter;
    if (instruction->op > BEGIN_PUSH_RANGE
        && instruction->op < END_PUSH_RANGE)
    {
      runPushCommand(instruction, program, bindings);
    }
    else if (instruction->op > BEGIN_BINARY_RANGE
             && instruction->op < END_BINARY_RANGE)
    {
      memoryT left = stack[stackIndex - 2];
      memoryT right = stack[stackIndex - 1];
      stackIndex -= 2;
      runBinaryCommand(instruction, left, right);
    }
    else if (instruction->op > BEGIN_PROMOTION_RANGE
             && instruction->op < END_PROMOTION_RANGE)
    {
      memoryT left = promoteLeft(stack[stackIndex - 2],
                                 stack[stackIndex - 1]);
      memoryT right = promoteLeft(stack[stackIndex - 1],
                                  stack[stackIndex - 2]);
      stackIndex -= 2;
      runPromotionCommand(instruction, left, right);
    }
    else if (instruction->op > BEGIN_UNARY_RANGE
             && instruction->op < END_UNARY_RANGE)
    {
      memoryT arg = stack[stackIndex - 1];
      stackIndex -= 1;
      runUnaryCommand(instruction, arg);
    }
    else if (instruction->op > BEGIN_SHORTCUT_RANGE
             && instruction->op < END_SHORTCUT_RANGE)
    {
      memoryT arg = stack[stackIndex - 1];
      /* Shortcut commands peek at the top of the stack but do not
         consume the value */
      programCounter += runShortcutCommand(instruction, arg);
    }
    else
    {
      assert(0);
    }
    ++programCounter;
  }
  result = stack[stackIndex - 1];
  if (result.type == TERNARY_TYPE)
  {
    return result.value.ternary;
  }
  else
  {
    return TERN_BOTTOM;
  }
}

static void runPushCommand(instructionT * instruction,
                           programT * program, memoryT * bindings)
{
  memoryT pushee;
  switch (instruction->op)
  {
  case PUSH_VARIABLE_COMMAND:
    {
      int index = instruction->arg.index;
      pushee = bindings[index];
      break;
    }
  case PUSH_INT32_LITERAL_COMMAND:
    {
      int32T value = instruction->arg.int32;
      pushee.type = INT32_TYPE;
      pushee.value.int32 = value;
      break;
    }
  case PUSH_INT64_LITERAL_COMMAND:
    {
      int64T value = instruction->arg.int64;
      pushee.type = INT64_TYPE;
      pushee.value.int64 = value;
      break;
    }
  case PUSH_REAL64_LITERAL_COMMAND:
    {
      real64T value = instruction->arg.real64;
      pushee.type = REAL64_TYPE;
      pushee.value.real64 = value;
      break;
    }
  case PUSH_STRING_LITERAL_COMMAND:
    {
      int index = instruction->arg.index;
      pushee.type = STRING_TYPE;
      pushee.value.string = program->stringTable + index;
      break;
    }
  default:
    /* This shouldn't be possible. The function which dispatches to
       this one should make sure that no non-push commands are
       reached here. */
    pushee.type = TERNARY_TYPE;
    pushee.value.ternary = TERN_BOTTOM;
    break;
  }
  stack[stackIndex] = pushee;
  ++stackIndex;
}

static void runBinaryCommand(instructionT * instruction,
                             memoryT left, memoryT right)
{
  memoryT result;
  switch (instruction->op)
  {
  case OR_COMMAND:
  case XOR_COMMAND:
  case AND_COMMAND:
    {
      ternaryBoolT leftTern = TERN_BOTTOM;
      ternaryBoolT rightTern = TERN_BOTTOM;
      if (left.type == TERNARY_TYPE)
      {
        leftTern = left.value.ternary;
      }
      if (right.type == TERNARY_TYPE)
      {
        rightTern = right.value.ternary;
      }
      result.type = TERNARY_TYPE;
      if (instruction->op == OR_COMMAND)
      {
        result.value.ternary = TERNARY_OR(leftTern, rightTern);
      }
      else if (instruction->op == XOR_COMMAND)
      {
        result.value.ternary = TERNARY_XOR(leftTern, rightTern);
      }
      else /* instruction->op == AND_COMMAND) */
      {
        result.value.ternary = TERNARY_AND(leftTern, rightTern);
      }
      break;
    }
  case BITWISE_LEFT_COMMAND:
  case BITWISE_SIGNED_RIGHT_COMMAND:
  case BITWISE_ZERO_RIGHT_COMMAND:
    {
      if ((left.type == INT32_TYPE || left.type == INT64_TYPE)
          && (right.type == INT32_TYPE || right.type == INT64_TYPE))
      {
        int shiftAmount = 0;
        if (right.type == INT32_TYPE)
        {
          shiftAmount = right.value.int32;
        }
        else /* right.type == INT64_TYPE */
        {
          /* Assume no shifting of greater than 2 billion bits */
          shiftAmount = (int32T)right.value.int64;
        }
        if (shiftAmount < 0)
        {
          shiftAmount = 0;
        }
        if (left.type == INT32_TYPE)
        {
          uint32_t MSB = left.value.int32 & 0x80000000;
          result.type = INT32_TYPE;
          if (instruction->op == BITWISE_LEFT_COMMAND)
          {
            if (shiftAmount >= 32) 
            {
              result.value.int32 = 0;
            }
            else 
            {
              result.value.int32 = (left.value.int32 << shiftAmount);
            }
            break;
          }

          if ((instruction->op == BITWISE_SIGNED_RIGHT_COMMAND
                    && MSB == 0)
                   || instruction->op == BITWISE_ZERO_RIGHT_COMMAND)
          {
            if (shiftAmount >= 32)
            {
              result.value.int32 = 0;
            }
            else
            { 
              result.value.int32 = ((left.value.int32 >> shiftAmount)
                                      & (0xffffffffU >> shiftAmount));
            }
          }
          else /* instruction->op == BITWISE_SIGNED_RIGHT_COMMAND */
          {
            /* The most significant bit is on */
            if (shiftAmount >= 32)
            {
              result.value.int32 = MSB;
            }
            else 
            {
              result.value.int32 = ((left.value.int32 >> shiftAmount) | MSB);
            }
          }
        }
        else /* left.type == INT64_TYPE */
        {
          uint64_t MSB = left.value.int64 & 0x8000000000000000LL;
          result.type = INT64_TYPE;
          if (instruction->op == BITWISE_LEFT_COMMAND)
          {
            if (shiftAmount >= 64)
            {
              result.value.int64 = 0;
            }
            else 
            {
              result.value.int64 = (left.value.int64 << shiftAmount);
            }
            break;
          }

          if ((instruction->op == BITWISE_SIGNED_RIGHT_COMMAND
                    && MSB == 0)
                   || instruction->op == BITWISE_ZERO_RIGHT_COMMAND)
          {
            if (shiftAmount >= 64) 
            {
              result.value.int64 = 0;
            }
            else
            { 
              result.value.int64 = ((left.value.int64 >> shiftAmount)
                                    & (0xffffffffffffffffULL >> shiftAmount));
            }
          }
          else /* instruction->op == BITWISE_SIGNED_RIGHT_COMMAND */
          {
            /* The most significant bit is on */
            if (shiftAmount >= 64) 
            {
              result.value.int64 = MSB;
            }
            else
            {
              result.value.int64 = ((left.value.int64 >> shiftAmount) | MSB);
            }
          }
        }
      }
      else
      {
        result.type = TERNARY_TYPE;
        result.value.ternary = TERN_BOTTOM;
      }
      break;
    }
  default:
    /* This shouldn't be possible. The function which dispatches to
       this one should make sure that no non-binary commands are
       reached here. */
    result.type = TERNARY_TYPE;
    result.value.ternary = TERN_BOTTOM;
    break;
  }
  stack[stackIndex] = result;
  ++stackIndex;
}

#define PROMOTION_ARITHMETIC_COMMAND(OP) \
    if (left.type == INT32_TYPE && right.type == INT32_TYPE) \
    { \
      result.type = INT32_TYPE; \
      result.value.int32 = (left.value.int32 OP right.value.int32); \
    } \
    else if (left.type == INT64_TYPE && right.type == INT64_TYPE) \
    { \
      result.type = INT64_TYPE; \
      result.value.int64 = (left.value.int64 OP right.value.int64); \
    } \
    else if (left.type == REAL64_TYPE && right.type == REAL64_TYPE) \
    { \
      result.type = REAL64_TYPE; \
      result.value.real64 = (left.value.real64 OP right.value.real64); \
    } \
    else \
    { \
      result.type = TERNARY_TYPE; \
      result.value.ternary = TERN_BOTTOM; \
    }

#define PROMOTION_COMPARISON_COMMAND(OP) \
    result.type = TERNARY_TYPE; \
    if (left.type == INT32_TYPE && right.type == INT32_TYPE) \
    { \
      result.value.ternary \
        = (left.value.int32 OP right.value.int32)?TERN_TRUE:TERN_FALSE; \
    } \
    else if (left.type == INT64_TYPE && right.type == INT64_TYPE) \
    { \
      result.value.ternary \
        = (left.value.int64 OP right.value.int64)?TERN_TRUE:TERN_FALSE; \
    } \
    else if (left.type == REAL64_TYPE && right.type == REAL64_TYPE) \
    { \
      result.value.ternary \
        = (left.value.real64 < right.value.real64)?TERN_TRUE:TERN_FALSE; \
    } \
    else \
    { \
      result.value.ternary = TERN_BOTTOM; \
    }

#define PROMOTION_BITWISE_COMMAND(OP) \
    if (left.type == INT32_TYPE && right.type == INT32_TYPE) \
    { \
      result.type = INT32_TYPE; \
      result.value.int32 = (left.value.int32 OP right.value.int32); \
    } \
    else if (left.type == INT64_TYPE && right.type == INT64_TYPE) \
    { \
      result.type = INT64_TYPE; \
      result.value.int64 = (left.value.int64 OP right.value.int64); \
    } \
    else \
    { \
      result.type = TERNARY_TYPE; \
      result.value.ternary = TERN_BOTTOM; \
    }



static void runPromotionCommand(instructionT * instruction,
                                memoryT left, memoryT right)
{
  memoryT result;
  switch(instruction->op)
  {
  case MULTIPLY_COMMAND:
    PROMOTION_ARITHMETIC_COMMAND(*);
    break;
  case DIVIDE_COMMAND:
    if (left.type == INT32_TYPE && right.type == INT32_TYPE)
    {
      if (right.value.int32 == 0)
      {
        result.type = TERNARY_TYPE;
        result.value.ternary = TERN_BOTTOM;
      }
      else
      {
        result.type = INT32_TYPE;
        result.value.int32 = (left.value.int32 / right.value.int32);
      }
    }
    else if (left.type == INT64_TYPE && right.type == INT64_TYPE)
    {
      if (right.value.int64 == 0LL)
      {
        result.type = TERNARY_TYPE;
        result.value.ternary = TERN_BOTTOM;
      }
      else
      {
        result.type = INT64_TYPE;
        result.value.int64 = (left.value.int64 / right.value.int64);
      }
    }
    else if (left.type == REAL64_TYPE && right.type == REAL64_TYPE)
    {
      result.type = REAL64_TYPE;
      result.value.real64 = (left.value.real64 / right.value.real64);
    }
    else
    {
      result.type = TERNARY_TYPE;
      result.value.ternary = TERN_BOTTOM;
    }
    break;
  case MODULUS_COMMAND:
    if (left.type == INT32_TYPE && right.type == INT32_TYPE)
    {
      if (right.value.int32 == 0)
      {
        result.type = TERNARY_TYPE;
        result.value.ternary = TERN_BOTTOM;
      }
      else
      {
        result.type = INT32_TYPE;
        result.value.int32 = (left.value.int32 % right.value.int32);
      }
    }
    else if (left.type == INT64_TYPE && right.type == INT64_TYPE)
    {
      if (right.value.int64 == 0LL)
      {
        result.type = TERNARY_TYPE;
        result.value.ternary = TERN_BOTTOM;
      }
      else
      {
        result.type = INT64_TYPE;
        result.value.int64 = (left.value.int64 % right.value.int64);
      }
    }
    else
    {
      result.type = TERNARY_TYPE;
      result.value.ternary = TERN_BOTTOM;
    }
    break;
  case ADD_COMMAND:
    PROMOTION_ARITHMETIC_COMMAND(+);
    break;
  case SUBTRACT_COMMAND:
    PROMOTION_ARITHMETIC_COMMAND(-);
    break;
  case LESS_THAN_COMMAND:
    PROMOTION_COMPARISON_COMMAND(<);
    break;
  case GREATER_THAN_COMMAND:
    PROMOTION_COMPARISON_COMMAND(>);
    break;
  case LESS_THAN_EQUAL_COMMAND:
    PROMOTION_COMPARISON_COMMAND(<=);
    break;
  case GREATER_THAN_EQUAL_COMMAND:
    PROMOTION_COMPARISON_COMMAND(>=);
    break;
  case EQUALITY_COMMAND:
    result.type = TERNARY_TYPE;
    if (left.type == TERNARY_TYPE && right.type == TERNARY_TYPE)
    {
      if (left.value.ternary == TERN_BOTTOM
          || right.value.ternary == TERN_BOTTOM)
      {
        result.value.ternary = TERN_BOTTOM;
      }
      else if (left.value.ternary == right.value.ternary)
      {
        result.value.ternary = TERN_TRUE;
      }
      else
      {
        result.value.ternary = TERN_FALSE;
      }
    }
    else if (left.type == INT32_TYPE && right.type == INT32_TYPE)
    {
      if (left.value.int32 == right.value.int32)
      {
        result.value.ternary = TERN_TRUE;
      }
      else
      {
        result.value.ternary = TERN_FALSE;
      }
    }
    else if (left.type == INT64_TYPE && right.type == INT64_TYPE)
    {
      if (left.value.int64 == right.value.int64)
      {
        result.value.ternary = TERN_TRUE;
      }
      else
      {
        result.value.ternary = TERN_FALSE;
      }
    }
    else if (left.type == REAL64_TYPE && right.type == REAL64_TYPE)
    {
      if (left.value.real64 == right.value.real64)
      {
        result.value.ternary = TERN_TRUE;
      }
      else
      {
        result.value.ternary = TERN_FALSE;
      }
    }
    else if (left.type == STRING_TYPE && right.type == STRING_TYPE)
    {
      if (strcmpesc(left.value.string, right.value.string) == 0)
      {
        result.value.ternary = TERN_TRUE;
      }
      else
      {
        result.value.ternary = TERN_FALSE;
      }
    }
    else /* type mismatch */
    {
      result.value.ternary = TERN_BOTTOM;
    }
    break;
  case BITWISE_AND_COMMAND:
    PROMOTION_BITWISE_COMMAND(&);
    break;
  case BITWISE_XOR_COMMAND:
    PROMOTION_BITWISE_COMMAND(^);
    break;
  case BITWISE_OR_COMMAND:
    PROMOTION_BITWISE_COMMAND(|);
    break;
  default:
    /* This shouldn't be possible. The function which dispatches to
       this one should make sure that no non-promotion commands are
       reached here. */
    result.type = TERNARY_TYPE;
    result.value.ternary = TERN_BOTTOM;
    break;
  }
  stack[stackIndex] = result;
  ++stackIndex;
}

static void runUnaryCommand(instructionT * instruction, memoryT arg)
{
  memoryT result;
  switch (instruction->op)
  {
  case NOT_COMMAND:
    result.type = TERNARY_TYPE;
    if (arg.type == TERNARY_TYPE)
    {
      result.value.ternary = TERNARY_NOT(arg.value.ternary);
    }
    else
    {
      result.value.ternary = TERN_BOTTOM;
    }
    break;
  case BITWISE_NOT_COMMAND:
    if (arg.type == INT32_TYPE)
    {
      result.type = INT32_TYPE;
      result.value.int32 = ~(arg.value.int32);
    }
    else if (arg.type == INT64_TYPE)
    {
      result.type = INT64_TYPE;
      result.value.int64 = ~(arg.value.int64);
    }
    else
    {
      result.type = TERNARY_TYPE;
      result.value.ternary = TERN_BOTTOM;
    }
    break;
  case NEGATE_COMMAND:
    if (arg.type == INT32_TYPE)
    {
      result.type = INT32_TYPE;
      result.value.int32 = - arg.value.int32;
    }
    else if (arg.type == INT64_TYPE)
    {
      result.type = INT64_TYPE;
      result.value.int64 = - arg.value.int64;
    }
    else if (arg.type == REAL64_TYPE)
    {
      result.type = REAL64_TYPE;
      result.value.real64 = - arg.value.real64;
    }
    else
    {
      result.type = TERNARY_TYPE;
      result.value.ternary = TERN_BOTTOM;
    }
    break;
  default:
    /* This shouldn't be possible. The function which dispatches to
       this one should make sure that no non-unary commands are
       reached here. */
    result.type = TERNARY_TYPE;
    result.value.ternary = TERN_BOTTOM;
    break;
  }
  stack[stackIndex] = result;
  ++stackIndex;
}

static int runShortcutCommand(instructionT * instruction, memoryT arg)
{
  /* By default, jump no extra instructions */
  int jumpSize = 0;
  switch (instruction->op)
  {
  case JUMP_IF_TRUE_COMMAND:
    if (arg.type == TERNARY_TYPE && arg.value.ternary == TERN_TRUE)
    {
      jumpSize = instruction->arg.jumpSize;
    }
    break;
  case JUMP_IF_FALSE_COMMAND:
    if (arg.type == TERNARY_TYPE && arg.value.ternary == TERN_FALSE)
    {
      jumpSize = instruction->arg.jumpSize;
    }
    break;
  case JUMP_IF_BOTTOM_COMMAND:
    if (arg.type == TERNARY_TYPE && arg.value.ternary == TERN_BOTTOM)
    {
      jumpSize = instruction->arg.jumpSize;
    }
    break;
  default:
    /* This shouldn't happen. Do nothing since we don't have a result. */
    break;
  }
  return jumpSize;
}

static memoryT promoteLeft(memoryT left, memoryT right)
{
  memoryT result;
  if (left.type > BEGIN_NUMERIC_TYPE && left.type < END_NUMERIC_TYPE
      && right.type > BEGIN_NUMERIC_TYPE && right.type < END_NUMERIC_TYPE
      && right.type > left.type)
  {
    if (left.type == INT32_TYPE)
    {
      if (right.type == INT64_TYPE)
      {
        result.type = INT64_TYPE;
        result.value.int64 = (int64T)left.value.int32;
      }
      else /* right.type == REAL64_TYPE */
      {
        result.type = REAL64_TYPE;
        result.value.real64 = (real64T)left.value.int32;
      }
    }
    else /* left.type == INT64_TYPE */
    {
      /* The only type higher than INT64_TYPE is REAL64_TYPE. */
      result.type = REAL64_TYPE;
      result.value.real64 = (real64T)left.value.int64;
    }
  }
  else
  {
    result = left;
  }
  return result;
}

static char * initEscapeTable(void)
{
  static char escapeTable[256] = {0};

  escapeTable['n'] = '\n';
  escapeTable['t'] = '\t';
  escapeTable['r'] = '\r';
  escapeTable['\\'] = '\\';
  escapeTable['"'] = '"';
  escapeTable['\''] = '\'';
  return (char *)escapeTable;
}

static int strcmpesc(char const * left, char const * right)
{
  int result = 0;
  static char * escapeTable = NULL;
  char const * leftPos;
  char const * rightPos;

  if (escapeTable == NULL)
  {
    escapeTable = initEscapeTable();
  }
  leftPos = left;
  rightPos = right;
  while (result == 0 && *leftPos != '\0' && *rightPos != '\0')
  {
    if (*leftPos == '\\' && *(leftPos+1) != '\0')
    {
      ++leftPos;
/*      printf("left: \\%c\n", *leftPos); */
      result += escapeTable[(int)(*leftPos)];
    }
    else
    {
/*      printf("left: %c\n", *leftPos); */
      result += *leftPos;
    }
    if (*rightPos == '\\' && *(rightPos+1) != '\0')
    {
      ++rightPos;
/*      printf("right: \\%c\n", *rightPos); */
      result -= escapeTable[(int)(*rightPos)];
    }
    else
    {
/*      printf("right: %c\n", *rightPos); */
      result -= *rightPos;
    }
    ++leftPos;
    ++rightPos;
  }
  if (result == 0) {
    result += *leftPos;
    result -= *rightPos;
  }
  return result;
}
