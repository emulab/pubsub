/* test.c */

/* There are two alternate usages: */

/* USAGE: test foo=1 bar=2 'quux="string"' < program.b */
/* Runs program.b using the bindings given on the command line. */

/* USAGE: test */
/* Runs a standard set of regression tests. */

#include <assert.h>

#include "bool.h"

ternaryBoolT bindAndRun(programT * program, int argc, char * argv[]);
void runRegressionTests(void);
ternaryBoolT runTest(char * code, int bindingCount, ...);

int main(int argc, char * argv[])
{
  if (argc > 1)
  {
    int maxCodeSize = 100000;
    char code[maxCodeSize];
    ternaryBoolT result = TERN_BOTTOM;
    errorT error;
    programT program;
    int codeCount = fread(code, 1, maxCodeSize, stdin);
    if (codeCount < maxCodeSize)
    {
      code[codeCount] = '\0';
      error = createProgram(code, &program);
      switch (error.type)
      {
      case NO_ERROR:
        result = bindAndRun(&program, argc - 1, argv + 1);
        if (result == TERN_TRUE)
        {
          printf("result is TRUE\n");
        }
        else if (result == TERN_FALSE)
        {
          printf("result is FALSE\n");
        }
        else /* result == TERN_BOTTOM */
        {
          printf("result is BOTTOM\n");
        }
        break;
      case SYNTAX_ERROR:
        printf("Compile failed: Syntax error\n");
        break;
      case OUT_OF_MEMORY_ERROR:
        printf("Compile failed: Out of memory\n");
        break;
      }
      destroyProgram(&program);
    }
    else
    {
      printf("File too big. Ignored.\n");
    }
  }
  else
  {
    runRegressionTests();
  }
  return 0;
}

ternaryBoolT bindAndRun(programT * program, int argc, char ** argv)
{
  errorT error;
  bindingsT bindings;
  ternaryBoolT result = TERN_BOTTOM;
  error = createBindings(program, &bindings);
  clearBindings(&bindings);
  if (error.type == NO_ERROR)
  {
    int i = 0;
    for (i = 0; i < argc; ++i)
    {
      memoryT mem;
      char * name = NULL;
      char * value = NULL;
      name = argv[i];
      value = strchr(name, '=');
      if (value != NULL && value[1] != '"')
      {
        mem.type = INT32_TYPE;
        *value = '\0';
        ++value;
        mem.value.int32 = strtol(value, NULL, 0);
        bindVariable(&bindings, name, mem);
      }
      else
      {
        mem.type = STRING_TYPE;
        if (value == NULL)
        {
          // If there is no '=', then value is the empty string.
          value = name + strlen(name);
        }
        else
        {
          char * valueEnd = NULL;
          value[0] = '\0';
          value += 2; /* skip over initial '"' */
          valueEnd = strrchr(value, '"');
          if (valueEnd != NULL)
          {
            *valueEnd = '\0'; /* null out last '"' if it exists */
          }
        }
        mem.value.string = value;
        bindVariable(&bindings, name, mem);
      }
    }
    result = evalBindings(&bindings);

    destroyBindings(&bindings);
  }
  else
  {
    printf("Binding failed: Out of memory\n");
  }
  return result;
}

#define assertTrue(...) assert(runTest(__VA_ARGS__) == TERN_TRUE);
#define assertFalse(...) assert(runTest(__VA_ARGS__) == TERN_FALSE);
#define assertBottom(...) assert(runTest(__VA_ARGS__) == TERN_BOTTOM);

void runRegressionTests(void)
{
  assertBottom("foo == 1", 0);
  assertTrue("a == 1", 1, "a=1");
  assertFalse("a == 1", 1, "a=0");

  /* Ensure that the boolean operators work as expected */
  /* Each test ensures that a table entry is correct */
  assertFalse("!(a == 1)", 1, "a=1");
  assertBottom("!(a == 1)", 0);
  assertTrue("!(a == 1)", 1, "a=0");

  assertTrue("a == 1 && b == 1", 2, "a=1", "b=1");
  assertBottom("a == 1 && b == 1", 1, "a=1");
  assertFalse("a == 1 && b == 1", 2, "a=1", "b=0");
  assertBottom("a == 1 && b == 1", 1, "b=1");
  assertBottom("a == 1 && b == 1", 0);
//  assertFalse("a == 1 && b == 1", 1, "b=0");
  assertFalse("a == 1 && b == 1", 2, "a=0", "b=1");
//  assertFalse("a == 1 && b == 1", 1, "a=0");
  assertFalse("a == 1 && b == 1", 2, "a=0", "b=0");

  assertTrue("a == 1 || b == 1", 2, "a=1", "b=1");
//  assertTrue("a == 1 || b == 1", 1, "a=1");
  assertTrue("a == 1 || b == 1", 2, "a=1", "b=0");
//  assertTrue("a == 1 || b == 1", 1, "b=1");
  assertBottom("a == 1 || b == 1", 0);
  assertBottom("a == 1 || b == 1", 1, "b=0");
  assertTrue("a == 1 || b == 1", 2, "a=0", "b=1");
  assertBottom("a == 1 || b == 1", 1, "a=0");
  assertFalse("a == 1 || b == 1", 2, "a=0", "b=0");

  assertFalse("OBJTYPE == \"AB\"", 1, "OBJTYPE=\"A\"");
  assertFalse("OBJTYPE == \"AB\"", 1, "OBJTYPE=\"BA\"");

  assertFalse("a == 1 ^^ b == 1", 2, "a=1", "b=1");
  assertBottom("a == 1 ^^ b == 1", 1, "a=1");
  assertTrue("a == 1 ^^ b == 1", 2, "a=1", "b=0");
  assertBottom("a == 1 ^^ b == 1", 1, "b=1");
  assertBottom("a == 1 ^^ b == 1", 0);
  assertBottom("a == 1 ^^ b == 1", 1, "b=0");
  assertTrue("a == 1 ^^ b == 1", 2, "a=0", "b=1");
  assertBottom("a == 1 ^^ b == 1", 1, "a=0");
  assertFalse("a == 1 ^^ b == 1", 2, "a=0", "b=0");

  /* Test every operator on Strings */
  assertTrue("a == \"foo\"", 1, "a=\"foo\"");
  assertTrue("a == \"foo\n\"", 1, "a=\"foo\\n\"");
  assertTrue("a != \"foo\"", 1, "a=\"foo\\\"\"");

  /* Test every operator on int32's */
  assertTrue("a == 0x10", 1, "a=16");
  assertTrue("-a == 4", 1, "a=-4");
  assertTrue("a | b == c", 3, "a=0x1010", "b=0x1100", "c=0x1110");
  assertTrue("a | 0x1100 == 0x1110", 1, "a=0x1010");
  assertTrue("a ^ 0x1100 == 0x0110", 1, "a=0x1010");
  assertTrue("a & 0x1100 == 0x1000", 1, "a=0x1010");
  assertTrue("(~a & 0xf) == 7", 1, "a=8");
  assertTrue("~a == 0xfff0ff00", 1, "a=0xf00ff");
  assertTrue("a << 4 == 0x10", 1, "a=1");
  assertTrue("a >> 1 == -1", 1, "a=-2");
  assertTrue("a >>> 1 == a >> 1", 1, "a=10");
  assertTrue("a >>> 4 == 1", 1, "a=0x10");
  assertTrue("a*10 == 50", 1, "a=5");
  assertTrue("a/10 == 4", 1, "a=49");
  assertBottom("a/0 == 1", 1, "a=10");
  assertTrue("a%10 == 9", 1, "a=49");
  assertBottom("a%0 == 1", 1, "a=5");
  assertTrue("a+5 == 16", 1, "a=11");
  assertTrue("a-5 == 11", 1, "a=16");
  assertTrue("a < 10", 1, "a=9");
  assertTrue("a > 10", 1, "a=14234");
  assertTrue("a <= 10", 1, "a=9");
  assertTrue("a <= a", 1, "a=10");
  assertTrue("a >= 10", 1, "a=11");
  assertTrue("a >= a", 1, "a=10");
  assertTrue("a == a", 1, "a=11");
  assertTrue("a != 15", 1, "a=6");

  /* TODO: Test every operator on int64's */
  /* TODO: Test every operator on real64's */
  /* TODO: Test operators on invalid types */

  fprintf(stderr, "\n");
}

ternaryBoolT runTest(char * code, int bindingCount, ...)
{
  static int testCount = 0;
  char testStrings[50000];
  char * testPos = testStrings;
  va_list argList;
  ternaryBoolT result = TERN_BOTTOM;
  programT program;
  errorT compileResult = createProgram(code, &program);
  switch (compileResult.type)
  {
  case NO_ERROR:
    if (bindingCount > 0)
    {
      char ** args = (char **)malloc(sizeof(char **) * bindingCount);
      int i = 0;
      va_start(argList, bindingCount);
      for (i = 0; i < bindingCount; ++i)
      {
        strcpy(testPos, va_arg(argList, char *));
        args[i] = testPos;
        testPos += strlen(testPos) + 1;
      }
      result = bindAndRun(&program, bindingCount, args);
      va_end(argList);
      free(args);
    }
    else
    {
      result = bindAndRun(&program, bindingCount, NULL);
    }
    break;
  case SYNTAX_ERROR:
    fprintf(stderr, "Compile failed: Syntax error\n");
    break;
  case OUT_OF_MEMORY_ERROR:
    fprintf(stderr, "Compile failed: Out of memory\n");
    break;
  }
  destroyProgram(&program);
  fprintf(stderr, "[%d] ", testCount);
  ++testCount;
  return result;
}
