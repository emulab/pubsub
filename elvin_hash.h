#ifndef _elvin_hash_h
#define _elvin_hash_h

struct elvin_hashtable;

int
elvin_hashtable_buckethash(struct elvin_hashtable *table, char *key);

struct elvin_hashtable *
elvin_hashtable_alloc(int size, pubsub_error_t *myerror);

void
elvin_hashtable_free(struct elvin_hashtable *table);

void
elvin_hashtable_clear(struct elvin_hashtable *table);

int
elvin_hashtable_add(struct elvin_hashtable *table,
		    char *key, pubsub_value_t pv, pubsub_type_t type,
		    pubsub_error_t *myerror);

int
elvin_hashtable_remove(struct elvin_hashtable *table,
		       char *key, pubsub_error_t *myerror);

int
elvin_hashtable_traverse(struct elvin_hashtable *table,
			 pubsub_traverse_func_t func, void *arg,
			 pubsub_error_t *myerror);

#endif
