/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2012 University of Utah and the Flux Group.
 * All rights reserved.
 */

#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <assert.h>
#include <sys/types.h>
#include <setjmp.h>
#ifdef THREADED
#include <pthread.h>
#endif
#include "pubsub.h"
#include "log.h"

static int	debug = 0;
static char *	group = NULL;

/* Forward decls */
static void     notify_callback(pubsub_handle_t *handle,
				pubsub_subscription_t *subscription,
				pubsub_notification_t *notification,
				void *data);

static int	timeout_callback(pubsub_handle_t *handle,
				 pubsub_timeout_t *timeout,
				 void *data,
				 pubsub_error_t *error);

char *usagestr = 
 "usage: pinger [options] groupid\n"
 " -h              Display this message\n"
 " -d              Turn on debugging\n"
 " -s server       Specify a sync server to connect to\n"
 " -p portnum      Specify a port number to connect to\n"
 "\n";

void
usage()
{
	fprintf(stderr, "%s", usagestr);
	exit(64);
}

int
main(int argc, char **argv)
{
	int			ch;
	int			portnum = PUBSUB_SERVER_PORTNUM;
	char			*server = "localhost";
	char			expr[256];
	pubsub_handle_t		*handle = NULL;
	pubsub_error_t		pubsub_error;
	pubsub_subscription_t   *subscription;
	pubsub_timeout_t	*timeout;
#ifndef THREADED
	int			keep_looping = 1;
#endif
	
	while ((ch = getopt(argc, argv, "hVds:p:")) != -1) {
		switch(ch) {
		case 'd':
			debug++;
			break;
		case 'p':
			if (sscanf(optarg, "%d", &portnum) == 0) {
				fprintf(stderr,
					"Error: -p value is not a number: "
					"%s\n",
					optarg);
				usage();
			}
			else if ((portnum <= 0) || (portnum >= 65536)) {
				fprintf(stderr,
					"Error: -p value is not between "
					"0 and 65536: %d\n",
					portnum);
				usage();
			}
			break;
		case 's':
			if (strlen(optarg) == 0) {
				fprintf(stderr, "Error: -s value is empty\n");
				usage();
			}
			else {
				server = optarg;
			}
			break;
		case 'h':
			fprintf(stderr, "%s", usagestr);
			exit(0);
			break;
		default:
			usage();
			break;
		}
	}

	argv += optind;
	argc -= optind;

	if (argc != 1) {
		fprintf(stderr,
			"Error: Unrecognized command line arguments: %s ...\n",
			argv[0]);
		usage();
	}

	group = argv[0];

	if (!server) {
		fprintf(stderr,
			"Error: Could not deduce the name of the server!\n");
		usage();
	}

	if (pubsub_connect(server, portnum, &handle) < 0) {
		ps_error("Could not connect to pubsub server!\n");
		exit(-1);
	}

#ifdef __FreeBSD__
	srandomdev();
#else
	/* w00t! is this random or what? */
	srandom(getpid());
#endif
	
	snprintf(expr, sizeof(expr),
		 "type == \"reply\" && dst == \"%d\"",
		 getpid());
	if (! (subscription = 
	       pubsub_add_subscription(handle,
				       expr,
				       notify_callback, NULL,
				       &pubsub_error))) {
		fprintf(stderr, "add subscription failed\n");
		exit(-1);
	}

	/* Timeout test */
	if (! (timeout =
	       pubsub_add_timeout(handle, NULL, 0,
				  timeout_callback, NULL, &pubsub_error))) {
		fprintf(stderr, "add timeout failed\n");
		exit(-1);
	}

#ifdef  THREADED
	while (1) {
		sleep(10);
		printf("Main program loop\n");
	}
#else
	pubsub_mainloop(handle, &keep_looping, &pubsub_error);
#endif
	
	if (pubsub_rem_subscription(handle, subscription, &pubsub_error) < 0) {
		fprintf(stderr, "rem subscription failed\n");
		exit(-1);
	}
	pubsub_disconnect(handle);
	return 0;
}

static void
notify_callback(pubsub_handle_t *handle,
		pubsub_subscription_t *subscription,
		pubsub_notification_t *notification,
		void *data)
{
	char		*src;
	struct timeval	now, tv, diff;
	pubsub_error_t	error;
	int32_t sec, usec;

	if (pubsub_notification_get_int32(notification,
					  "sec", &sec, &error) == -1) {
		fprintf(stderr, "error: could not get 'sec' value");
		return;
	}
	if (pubsub_notification_get_int32(notification,
					  "usec", &usec, &error) == -1) {
		fprintf(stderr, "error: could not get 'usec' value");
		return;
	}
	tv.tv_sec = sec;
	tv.tv_usec = usec;
	if (pubsub_notification_get_string(notification,
					   "src", &src,
					   &error) == -1) {
		fprintf(stderr, "error: could not get 'src' value");
		return;
	}

	gettimeofday(&now, NULL);
	timersub(&now, &tv, &diff);
	if (debug) {
		printf("REPLY from %s: time=%d.%06ds\n",
		       src,
		       (int)diff.tv_sec,
		       (int)diff.tv_usec);
	}
}

static int
timeout_callback(pubsub_handle_t *handle,
		 pubsub_timeout_t *timeout, void *data,
		 pubsub_error_t *error)
{
	pubsub_notification_t	*pn;
	unsigned int		msdelay;
	struct timeval		now;

	msdelay = random() % (1 * 1000);

	if (! (pn = pubsub_notification_alloc(handle, error)))
		exit(-1);

	if (pubsub_notification_add_string(pn, "GROUP", group, error) == -1)
		exit(-1);

	gettimeofday(&now, NULL);

	if (pubsub_notification_add_string(pn, "type", "req", error) == -1)
		exit(-1);
	if (pubsub_notification_add_int32(pn, "src", getpid(), error) == -1)
		exit(-1);
	if (pubsub_notification_add_int32(pn, "sec", now.tv_sec, error) == -1)
		exit(-1);
	if (pubsub_notification_add_int32(pn, "usec",
					  now.tv_usec, error) == -1) {
		exit(-1);
	}

	if (pubsub_notify(handle, pn, error) == -1)
		exit(-1);

	pubsub_notification_free(handle, pn, error);

	if (debug) {
		printf("sent ping, sending next in %dms\n", msdelay);
	}
	
	if (! (timeout =
	       pubsub_add_timeout(handle, timeout, msdelay,
				  timeout_callback, NULL, error))) {
		exit(-1);
	}
	return 0;
}
