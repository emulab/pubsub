/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2010 University of Utah and the Flux Group.
 * All rights reserved.
 */

#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <sys/time.h>
#include <sys/param.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <assert.h>
#include <sys/types.h>
#include <setjmp.h>
#include <paths.h>
#include <elvin/elvin.h>
#include "pubsub.h"
#include "clientapi.h"
#include "log.h"

static int	debug   = 0;
static int	verbose = 1;

/* Forward decls */
static void     pubsub_notify_callback(pubsub_handle_t *handle,
				       pubsub_subscription_t *subscription,
				       pubsub_notification_t *notification,
				       void *data);

static void     elvin_notify_callback(elvin_handle_t handle,
				      elvin_subscription_t subscription,
				      elvin_notification_t notification,
				      int is_secure,
				      void *rock,
				      elvin_error_t error);

static int	iohandler_callback(elvin_io_handler_t handler,
				   int fd, void *arg,
				   elvin_error_t myerror);

/* A few things are easier if global */
static pubsub_handle_t		*pubsub_handle;
static elvin_handle_t		elvin_handle;
static elvin_error_t		elvin_error;

char *usagestr = 
 "usage: testclient [options]\n"
 " -h              Display this message\n"
 " -d              Turn on debugging\n"
 " -v              Turn on verbose logging\n"
 " -e handle       Specify elvin handle to connect to\n"
 " -s server       Specify a pubsub server to connect to\n"
 " -p portnum      Specify a pubsub port number to connect to\n"
 "\n";

void
usage()
{
	fprintf(stderr, "%s", usagestr);
	exit(1);
}

static void
setverbose(int sig)
{
	if (sig == SIGUSR1)
		verbose = 1;
	else
		verbose = 0;
}

int
main(int argc, char **argv)
{
	int			ch, keep_looping = 1;
	int			portnum = PUBSUB_SERVER_PORTNUM;
	char			*bossnode = "boss", bossipaddr[32];
	char			*server = "localhost";
	char			*elvin_server = "elvin://localhost";
	char			hostname[MAXHOSTNAMELEN], ipaddr[32];
	char			buf[BUFSIZ];
	struct hostent		*he;
	pubsub_error_t		pubsub_error;
	pubsub_subscription_t   *subscription;
	pubsub_notification_t   *pubsub_notification;
	elvin_subscription_t    elvin_subscription;
	elvin_io_handler_t      elvin_iohandler;
	elvin_notification_t    elvin_notification;
	
	while ((ch = getopt(argc, argv, "hdvs:p:")) != -1) {
		switch(ch) {
		case 'd':
			debug++;
			break;
		case 'v':
			verbose++;
			break;
		case 'p':
			if (sscanf(optarg, "%d", &portnum) == 0) {
				fprintf(stderr,
					"Error: -p value is not a number: "
					"%s\n",
					optarg);
				usage();
			}
			else if ((portnum <= 0) || (portnum >= 65536)) {
				fprintf(stderr,
					"Error: -p value is not between "
					"0 and 65536: %d\n",
					portnum);
				usage();
			}
			break;
		case 's':
			if (strlen(optarg) == 0) {
				fprintf(stderr, "Error: -s value is empty\n");
				usage();
			}
			else {
				server = optarg;
			}
			break;
		case 'h':
			fprintf(stderr, "%s", usagestr);
			exit(0);
			break;
		default:
			usage();
			break;
		}
	}

	argv += optind;
	argc -= optind;

	if (argc) {
		ps_error("Unrecognized command line arguments: %s ...\n",argv[0]);
		usage();
	}

	if (!server) {
		ps_error("Error: Could not deduce the name of the server!\n");
		usage();
	}

	if (!debug) {
		ps_loginit(1, "elvin_gateway");
		daemon(0, 0);
		pubsub_debug = (PUBSUB_DEBUG_CONNECT|
				PUBSUB_DEBUG_RECONNECT|
				PUBSUB_DEBUG_CALLBACK|
				PUBSUB_DEBUG_DISPATCHER); /* XXX for now */
	} else {
		/* XXX backward compat */
		pubsub_debug = (PUBSUB_DEBUG_CONNECT|
				PUBSUB_DEBUG_RECONNECT|
				PUBSUB_DEBUG_CALLBACK|
				PUBSUB_DEBUG_DISPATCHER);
		if (debug > 1)
			pubsub_debug |= PUBSUB_DEBUG_DETAIL;
	}

	ps_info("elvin<->pubsub gateway daemon starting\n");

	signal(SIGUSR1, setverbose);
	signal(SIGUSR2, setverbose);

	/*
	 * Stash the pid away.
	 */
	if (!geteuid()) {
		FILE	*fp;
		
		sprintf(buf, "%s/elvin_gateway.pid", _PATH_VARRUN);
		fp = fopen(buf, "w");
		if (fp != NULL) {
			fprintf(fp, "%d\n", getpid());
			(void) fclose(fp);
		}
	}

	/*
	 * Get hostname and IP for subscription.
	 */
	if (gethostname(hostname, MAXHOSTNAMELEN) < 0) {
		ps_error("could not get hostname: %s\n", strerror(errno));
		exit(-1);
	}
	if ((he = gethostbyname(hostname)) != NULL) {
		struct in_addr	myip;
		
		memcpy((char *)&myip, he->h_addr, he->h_length);
		strcpy(ipaddr, inet_ntoa(myip));
	}
	else {
		ps_error("could not get IP address from %s\n", hostname);
		exit(-1);
	}

	if ((he = gethostbyname(bossnode)) != NULL) {
		struct in_addr	myip;
		
		memcpy((char *)&myip, he->h_addr, he->h_length);
		strcpy(bossipaddr, inet_ntoa(myip));
	}
	else {
		ps_error("could not get IP address from %s\n", bossnode);
		exit(-1);
	}

	if (pubsub_connect(server, portnum, &pubsub_handle) < 0) {
		ps_error("Could not connect to pubsub server!\n");
		exit(-1);
	}
	ps_info("connected to pubsubd server\n");
	pubsub_notification = pubsub_notification_alloc(pubsub_handle,
							&pubsub_error);

	/* Initialize the Elvin synchronous interface */
	elvin_error        = elvin_sync_init_default();
	elvin_handle       = elvin_handle_alloc(elvin_error);
	elvin_notification = elvin_notification_alloc(elvin_error);
	
	/* Specify the server address  */
	if (! (elvin_handle_append_url(elvin_handle,
				       elvin_server, elvin_error))) {
		ps_error("Bad elvin URL: \"%s\"\n", elvin_server);
		exit(-1);
	}

	/* Connect to the server */
	if (! elvin_sync_connect(elvin_handle, elvin_error)) {
		ps_error("elvin_sync_connect(): failed\n");
		exit(-11);
	}
	ps_info("connected to elvind server\n");

	sprintf(buf, "___SENDER___ == \"%s\" || ___SENDER___ == \"%s\"",
		ipaddr, bossipaddr);
	if (! (subscription = 
	       pubsub_add_subscription(pubsub_handle, "",
				       pubsub_notify_callback,
				       elvin_notification,
				       &pubsub_error))) {
		ps_error("subscription to pubsub server failed\n");
		exit(-1);
	}
	ps_info("pubsub subscription added\n");

	sprintf(buf, "___SENDER___ != \"%s\" && ___SENDER___ != \"%s\"",
		ipaddr, bossipaddr);
	if (! (elvin_subscription =
	       elvin_sync_add_subscription(elvin_handle, buf,
					   NULL, 1,
					   elvin_notify_callback,
					   pubsub_notification,
					   elvin_error))) {
		ps_error("subscription to elvin server failed\n");
		exit(-1);
	}
	ps_info("elvin subscription added\n");

	if (! (elvin_iohandler = 
	       elvin_sync_add_io_handler(NULL,
					 pubsub_handle->sock,
					 ELVIN_READ_MASK,
					 iohandler_callback,
					 pubsub_handle, elvin_error))) {
		ps_error("subscription to pubsub server failed\n");
		exit(-1);
	}
	ps_info("elvin iohandler added\n");

	/* start mainloop */
	elvin_sync_default_mainloop(&keep_looping, elvin_error);

	pubsub_disconnect(pubsub_handle);
	return 0;
}

static char	notify_debug_string[2*BUFSIZ];

static int
pubsub_notify_traverse_debug(void *arg, char *name,
			     pubsub_type_t type, pubsub_value_t value,
			     pubsub_error_t *error)
{
	char		**bp = (char **) arg;
	char		buf[BUFSIZ], *tp = buf;
	unsigned char   *up;
	int		i, cc;

	switch (type) {
	case STRING_TYPE:
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=%s,", name, value.pv_string);
		break;
		
	case INT32_TYPE:
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=%d,", name, value.pv_int32);
		break;
		
	case OPAQUE_TYPE:
		up = (unsigned char *) value.pv_opaque.data;
		for (i = 0; i < value.pv_opaque.length; i++, up++) {
			cc = snprintf(tp,
				      &buf[sizeof(buf)-1]-tp, "%02hhx", *up);
			tp += cc;
			if (tp >= &buf[sizeof(buf)-1])
				break;
		}		
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=%s,", name, buf);
		break;
		
	default:
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=...,", name);
		break;
	}
	*bp = *bp + strlen(*bp);
	
	return 1;
}

static int
pubsub_notify_traverse(void *arg, char *name,
		       pubsub_type_t type, pubsub_value_t value,
		       pubsub_error_t *error)
{
	elvin_notification_t	elvin_notification = (elvin_notification_t)arg;

	switch (type) {
	case STRING_TYPE:
		elvin_notification_add_string(elvin_notification,
					      name, value.pv_string,
					      elvin_error);
		break;
	case INT32_TYPE:
		elvin_notification_add_int32(elvin_notification,
					     name, value.pv_int32,
					     elvin_error);
		break;
	case INT64_TYPE:
		elvin_notification_add_int64(elvin_notification,
					     name, value.pv_int64,
					     elvin_error);
		break;
	case REAL64_TYPE:
		elvin_notification_add_real64(elvin_notification,
					      name, value.pv_real64,
					      elvin_error);
		break;

	case OPAQUE_TYPE:
		elvin_notification_add_opaque(elvin_notification,
					      name,
					      (void *) value.pv_opaque.data,
					      value.pv_opaque.length,
					      elvin_error);
		break;
		       
	default:
		break;
	}
	
	return 1;
}

static int
elvin_notify_traverse_debug(void *arg, char *name, elvin_basetypes_t type,
			    elvin_value_t value, elvin_error_t error)

{
	char	**bp = (char **) arg;
	char    buf[BUFSIZ];
	int     cc, max;

	switch (type) {
	case ELVIN_INT32:
		snprintf(buf, sizeof(buf), "%s=%d,", name, value.i);
		break;
		
	case ELVIN_INT64:
		snprintf(buf, sizeof(buf), "%s=%lld,", name, value.h);
		break;
		
	case ELVIN_REAL64:
		snprintf(buf, sizeof(buf), "%s=%f,", name, value.d);
		break;
		
	case ELVIN_STRING:
		snprintf(buf, sizeof(buf), "%s=%s,", name, value.s);
		break;
		
	case ELVIN_OPAQUE:
		snprintf(buf, sizeof(buf), "%s='opaqueness',", name);
		break;

	default:
		return 1;
		break;
	}
	cc  = strlen(buf);
	max = &notify_debug_string[sizeof(notify_debug_string)-1] - *bp;

	if (cc >= max)
		cc = max;
	strncpy(*bp, buf, cc);
	*bp = *bp + cc;
	return 1;
}

static int
elvin_notify_traverse(void *arg, char *name, elvin_basetypes_t type,
			    elvin_value_t value, elvin_error_t elvin_error)

{
	pubsub_error_t		pubsub_error;
	pubsub_notification_t	*pubsub_notification =
		(pubsub_notification_t *) arg;

	switch (type) {
	case ELVIN_INT32:
		pubsub_notification_add_int32(pubsub_notification,
					      name, value.i, &pubsub_error);
		break;
		
	case ELVIN_INT64:
		pubsub_notification_add_int32(pubsub_notification,
					      name, value.h, &pubsub_error);
		break;
		
	case ELVIN_REAL64:
		pubsub_notification_add_real64(pubsub_notification,
					       name, value.d, &pubsub_error);
		break;
		
	case ELVIN_STRING:
		pubsub_notification_add_string(pubsub_notification,
					       name, value.s, &pubsub_error);
		break;
		
	case ELVIN_OPAQUE:
		pubsub_notification_add_opaque(pubsub_notification,
					       name,
					       (char *)(value.o.data),
					       value.o.length,
					       &pubsub_error);

		break;

	default:
		return 1;
		break;
	}
	return 1;
}

static void
pubsub_notify_callback(pubsub_handle_t *handle,
		       pubsub_subscription_t *subscription,
		       pubsub_notification_t *notification,
		       void *arg)
{
	pubsub_error_t		myerror;
	elvin_notification_t	elvin_notification;

	if (verbose) {
		char	*bp = notify_debug_string;
		
		*bp = '\0';
		pubsub_notification_traverse(notification,
					     pubsub_notify_traverse_debug,
					     &bp, &myerror);
					     
		ps_info("Pubsub Notification: '%s'\n", notify_debug_string);
	}
	elvin_notification = elvin_notification_alloc(elvin_error);

	pubsub_notification_traverse(notification,
				     pubsub_notify_traverse,
				     elvin_notification, &myerror);

	if (! elvin_sync_notify(elvin_handle,
				elvin_notification, 1, NULL, elvin_error)) {
		ps_error("Failed to send elvin notification!\n");
	}
	elvin_notification_free(elvin_notification, elvin_error);
}

static void
elvin_notify_callback(elvin_handle_t handle,
		      elvin_subscription_t subscription,
		      elvin_notification_t notification,
		      int is_secure, void *arg, elvin_error_t myerror)
{
	pubsub_error_t		pubsub_error;
	pubsub_notification_t	*pubsub_notification =
		(pubsub_notification_t *) arg;

	if (verbose) {
		char	*bp = notify_debug_string;
		
		*bp = '\0';
		elvin_notification_traverse(notification,
					    elvin_notify_traverse_debug,
					    &bp, myerror);
					     
		ps_info("Elvin Notification: '%s'\n", notify_debug_string);
	}
	if (elvin_notification_lookup(notification, "___PUBSUB___", myerror)) {
		ps_info("Elvin Notification: Dropping duplicate\n");
		return;
	}
#if 1
	pubsub_notification->used = 0;
#else
	/* Need to fix the library */
	pubsub_notification_clear(pubsub_notification, &pubsub_error);
#endif

	elvin_notification_traverse(notification,
				    elvin_notify_traverse,
				    pubsub_notification, myerror);

	/*
	 * Add a flag to indicate this notification is in "elvin order."
	 * See the hmac computation in testbed/event/lib/event.c to
	 * understand why this is necessary. Ick.
	 */
	pubsub_notification_add_int32(pubsub_notification,
				      "___elvin_ordered___", 1, &pubsub_error);
	
	if (pubsub_notify(pubsub_handle,
			  pubsub_notification, &pubsub_error) < 0) {
		ps_error("Failed to send pubsub notification!\n");
	}
}

static int
iohandler_callback(elvin_io_handler_t handler, int fd, void *arg,
		   elvin_error_t elvin_error)
{
	pubsub_error_t	myerror;
	pubsub_handle_t *handle = (pubsub_handle_t *) arg;
	int		oldsock = handle->sock;

	pubsub_dispatch(handle, 0, &myerror);
	
	/*
	 * Deal with a lost connection to the server.
	 */
	if (oldsock != handle->sock) {
		elvin_sync_remove_io_handler(handler, elvin_error);
		
		while (handle->sock < 0) {
			pubsub_dispatch(handle, 1, &myerror);
		}
		if (! (handler = 
		       elvin_sync_add_io_handler(NULL,
						 handle->sock,
						 ELVIN_READ_MASK,
						 iohandler_callback,
						 handle, elvin_error))) {
			ps_fatal("iohandle reinstall failed\n");
		}
	}
	return 0;
}
