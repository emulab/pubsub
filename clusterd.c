/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2010-2017 University of Utah and the Flux Group.
 * All rights reserved.
 */

#include <assert.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <paths.h>
#include "pubsub.h"
#include "clientapi.h" /* the opaque pubsub_notification_t is not enough;
			  we need details! */
#include "log.h"
#include "network.h"
#include "bool.h"
#ifdef WITHSSL
#include "ssl.h"
#endif

/*
 * The maximum number of clients is dictated by the FD_* macros
 * used in the network and client code for select() calls. See pubsubd.c
 * for details. Note that clusterd doesn't use select, but lets keep
 * the MAX_CLIENTS number consistent across the build.
 */
#ifdef FD_SETSIZE
#define MAX_CLIENTS FD_SETSIZE
#else
#define MAX_CLIENTS 1024
#endif

#define PROCESS_SUBSCRIPTIONS 1

struct subscription {
    int token, filter;
    programT expression;
    bindingsT bindings;
    struct subscription *next;
};

static struct client {
    int fd;
    struct pubsub_iohandler *iohandler;
    struct subscription *subscriptions;
} clients[ MAX_CLIENTS ];
static int num_clients;

static int client_map[ MAX_CLIENTS ]; /* map from file descriptor to
					 client index */
static int keep_looping;

static int print_traverse( void *arg, const char *name, pubsub_type_t type,
			    pubsub_value_t value, pubsub_error_t *error ) {

    printf( "  %s ", name );

    switch( type ) {
    case INT32_TYPE:
	printf( "(int32): %d\n", (int) value.pv_int32 );
	break;
	
    case INT64_TYPE:
	printf( "(int64): %lld\n", (long long) value.pv_int64 );
	break;
	
    case REAL64_TYPE:
	printf( "(real64): %f\n", value.pv_real64 );
	break;
	
    case STRING_TYPE:
	printf( "(string): %s\n", value.pv_string );
	break;

    case OPAQUE_TYPE:
	puts( "(opaque)" );
	break;
	
    default:
	puts( "(unknown)" );
    }

    return 1; /* must return non-zero to continue traversal */
}

static void kill_client( pubsub_handle_t *handle, int client ) {

    pubsub_error_t err;
#if PROCESS_SUBSCRIPTIONS
    struct subscription *p;
#endif

    if( pubsub_debug & PUBSUB_DEBUG_CLUSTERD )
	ps_info( "Closing client connection %d\n", client );
	
    close( clients[ client ].fd );
	
    pubsub_remove_iohandler( handle, clients[ client ].iohandler, &err );

#if PROCESS_SUBSCRIPTIONS
    p = clients[ client ].subscriptions;
    while( p ) {
	struct subscription *old;
	
	destroyBindings( &p->bindings );
	destroyProgram( &p->expression );
	
	old = p;
	p = p->next;
	free( old );
    }
#endif
	
    if( client != num_clients - 1 ) {
	/* Keep the client array compact. */
	memcpy( clients + client, clients + num_clients - 1,
		sizeof *clients );
	client_map[ clients[ client ].fd ] = client;
    }

    /* The client array is now contiguous again; the final entry is unused.
       We must be careful to avoid a dangling pointer! */
    clients[ num_clients - 1 ].subscriptions = NULL;
    
    num_clients--;
}

static int traverse_bindings( void *closure, const char *name,
			      pubsub_type_t type,
			      pubsub_value_t val, pubsub_error_t *error ) {
    
    struct subscription *p = closure;
    memoryT mem;

    switch( mem.type = type ) {
    case INT32_TYPE:
	mem.value.int32 = val.pv_int32;
	break;
	
    case INT64_TYPE:
	mem.value.int64 = val.pv_int64;
	break;
	
    case REAL64_TYPE:
	mem.value.real64 = val.pv_real64;
	break;
	
    case STRING_TYPE:
	mem.value.string = val.pv_string;
	break;
	
    default:
	return 0;
    }

    bindVariable( &p->bindings, name, mem );
    
    return 1;
}

static void subscription_callback( pubsub_handle_t *handle,
				   struct pubsub_subscription *subscription,
				   pubsub_notification_t *notification,
				   void *data ) {

    pubsub_error_t err;
    int i;
    notification_packet_t *packet;
    size_t full_size;

    if( pubsub_debug & PUBSUB_DEBUG_CLUSTERD ) {
	ps_info( "Subscription callback:\n" );
	pubsub_notification_traverse( notification, print_traverse, NULL,
				      &err );
    }
    
    /* notification->used is already in host byte order (clientapi.c
       does the conversion for us). */
    full_size = notification->used + sizeof *packet;
    
    if( !( packet = malloc( full_size ) ) )
	/* oh dear */
	return;

    /* This gets a bit messy.  Ideally, we would reuse the entire
       notification packet we got from our parent (including the
       header).  However, the clientapi library has already stripped
       off the header before we get it, so we attempt to recreate it,
       instead. */
    packet->header.len = htonl( full_size );
    packet->header.type = htons( PUBSUB_PKT_NOTIFICATION );
    packet->header.flags = 0;
    packet->header.seq = 0;
    packet->header.error = 0;
    packet->header.vers = htonl( PUBSUB_CUR_VERSION );
    packet->token = notification->token;
    packet->used = notification->used;
    memcpy( packet->data, notification->data, notification->used );
    packet->used = htonl( packet->used );

    i = 0;
client_gone:
    while( i < num_clients ) {
	/* we can't use a for loop here, because then the client_gone
	   handling would do the wrong thing */
#if PROCESS_SUBSCRIPTIONS
	struct subscription *p;

	for( p = clients[ i ].subscriptions; p; p = p->next ) {
	    if( p->filter ) {
		clearBindings( &p->bindings );
		pubsub_notification_traverse( notification, traverse_bindings,
					      p, &err );
		if( evalBindings( &p->bindings ) )
		    continue;
	    }

	    packet->token = htonl( p->token );
	    
	    if( write( clients[ i ].fd, packet, full_size ) < full_size ) {
		kill_client( handle, i );
		goto client_gone; /* retry new client at this index (if any) */
	    } else if( pubsub_debug & PUBSUB_DEBUG_CLUSTERD )
		ps_info( "  forwarded to client %d token %d\n", i, p->token );
	}
#else
	if( write( clients[ i ].fd, packet, full_size ) < full_size ) {
	    kill_client( handle, i );
	    goto client_gone; /* retry new client at this index (if any) */
	} else if( pubsub_debug & PUBSUB_DEBUG_CLUSTERD )
	    ps_info( "  forwarded to client %d\n", i );
#endif
	i++;
    }
    
    packet->header.type = htons( PUBSUB_PKT_NOTIFY );
}

static void add_subscription( int client, subscription_packet_t *packet ) {

    if( pubsub_debug & PUBSUB_DEBUG_CLUSTERD )
	ps_info( "add_subscription: client %d, expr '%s'\n", client,
	      packet->expression );

#if PROCESS_SUBSCRIPTIONS
    {
	struct subscription *p, *new;
	int token = ntohl( packet->token );
	
	for( p = clients[ client ].subscriptions; p; p = p->next )
	    if( p->token == token )
		/* duplicate; ignore */
		return;

	if( !( new = malloc( sizeof *new ) ) )
	    ps_pfatal( "malloc" );

	new->token = token;
	
	if( *packet->expression ) {
	    /* expression specified -- compile a filter */
	    new->filter = 1;

	    if( createProgram( packet->expression,
			       &new->expression ).type ||
		createBindings( &new->expression,
				&new->bindings ).type ) {
		ps_warning( "could not compile expression '%s', ignoring\n",
			    packet->expression );

		free( new );

		return;
	    }
	} else
	    /* empty expression -- match all */
	    new->filter = 0;
	
	new->next = clients[ client ].subscriptions;
	clients[ client ].subscriptions = new;
    }
#endif
}

static void rem_subscription( int client, subscription_packet_t *packet ) {

    if( pubsub_debug & PUBSUB_DEBUG_CLUSTERD )
	ps_info( "rem_subscription: client %d, expr '%s'\n", client,
	      packet->expression );

#if PROCESS_SUBSCRIPTIONS
    /* FIXME handle it */
#endif
}

static int client_callback( pubsub_handle_t *handle,
			    struct pubsub_iohandler *iohandler, int fd,
			    void *data, pubsub_error_t *error ) {

    maxpacket_t packet;
    int client = client_map[ fd ];

    assert( client < MAX_CLIENTS );

    if( pubsub_debug & PUBSUB_DEBUG_CLUSTERD )
	ps_info( "client_callback from client %d\n", client );

    if( ps_PacketRead( fd, (packet_t *) &packet, PUBSUB_MAX_PACKET ) < 0 ) {
	kill_client( handle, client );
	
	return 0;
    }

    if( pubsub_debug & PUBSUB_DEBUG_CLUSTERD )
	ps_info( "  read packet type %d\n", ntohs( packet.header.type ) );

    switch( ntohs( packet.header.type ) ) {
    case PUBSUB_PKT_NOTIFY:
	if (pubsub_notify_internal(handle, (packet_t *)&packet, error) < 0) {
	    ps_errorc( "pubsub_notify_internal" );
	    /* Our upstream state might be inconsistent.  Tell the main
	       loop to give up and restart. */
	    keep_looping = 0;
	}
	break;
	
    case PUBSUB_PKT_SUBSCRIBE:
	add_subscription( client, (subscription_packet_t *) &packet );
	
	break;
	
    case PUBSUB_PKT_UNSUBSCRIBE:	
	rem_subscription( client, (subscription_packet_t *) &packet );

	break;
	
    case PUBSUB_PKT_PING:
	/* no-op */
	break;
	
    default:
	/* no-op -- this is the same behaviour as pubsubd */
	break;
    }
    
    packet.header.len = htonl( sizeof packet.header );
    packet.header.error = 0;
    packet.header.flags = 0;

    if( ps_PacketSend( fd, (packet_t *) &packet, NULL ) < 0 )
	kill_client( handle, client );

    return 0;
}

static int listen_callback( pubsub_handle_t *handle,
			    struct pubsub_iohandler *iohandler, int fd,
			    void *data, pubsub_error_t *errorp ) {

    pubsub_error_t err;
    int client;

    if( pubsub_debug & PUBSUB_DEBUG_CLUSTERD )
	ps_info( "Listen callback\n" );

    if( ( client = accept( fd, NULL, NULL ) ) < 0 ) {
	ps_errorc( "accept" );
    
	return 0;
    }

    if( num_clients == MAX_CLIENTS || client >= MAX_CLIENTS ) {
	/* Can't cope with anybody else. */
	ps_error( "Too many connections; dropping.\n" );
	
	close( client );

	return 0;
    }
    
    client_map[ client ] = num_clients;
    
    clients[ num_clients ].fd = client;
    if( !( clients[ num_clients ].iohandler =
	   pubsub_add_iohandler( handle, NULL, client, 0, client_callback,
				 NULL, &err ) ) ) {
	ps_error( err.msg );
	close( client );
	
	return 0;
    }

    num_clients++;
    
    return 0;
}

static int failures; /* number of consecutive failures since last success */

static void fail( void ) {

    int i;
    
    failures++;

    /* Complain some but not all of the time. */
    for( i = 1; i; i <<= 1 )
	if( i == failures ) {
	    ps_warning( "parent connection failed %d time%s; still trying...\n",
			failures, failures > 1 ? "s" : "" );
	    break;
	}
    
    /* limited exponential backoff */
    sleep( failures < 7 ? 1 << failures : 1 << 7 );
}

static void succeed( void ) {

    if( failures ) {
	ps_warning( "parent connection restored after %d attempt%s.\n",
		    failures, failures > 1 ? "s" : "" );
	
	failures = 0;
    }
}

static void usage( void ) {

    fputs( "Usage: clusterd [-d] [-h] [-l port] [-p port] [-s server] [-v]\n"
	   "\n"
	   "    -d: stay in foreground\n"
	   "    -h: help\n"
	   "    -l port: listen on specified port\n"
	   "    -p port: connect to specified port on server\n"
	   "    -s server: hostname of parent server\n"
#ifdef WITHSSL
	   "    -S Connect using SSL to parent server\n"
	   "    -C Specify the certificate file for SSL\n"
	   "    -K Specify the key file for SSL\n"
#endif
	   "    -e pidfile: Specify pid file for writing process ID to\n"
	   "    -v: verbose\n", stderr );	   
}

extern int main( int argc, char *argv[] ) {

    char *parent_name = "ops.emulab.net";
    char *pidfile = (char *) NULL;
    int parent_port = PUBSUB_SERVER_PORTNUM;
    pubsub_handle_t *handle = NULL;
    pubsub_error_t err;
    int daemonize = 1, yes = 1;
    int incoming_port = PUBSUB_SERVER_PORTNUM;
    int incoming;
    struct sockaddr_in sin;
    int ch, rv, i;
    pubsub_iohandler_t *listen_iohandler;
    pubsub_subscription_t *subscription;
#ifdef  WITHSSL
    char	*certfile = NULL, *keyfile = NULL;
    int		dossl = 0;
#endif

    while( ( ch = getopt( argc, argv, "dhl:p:s:vSK:C:e:" ) ) > 0 )
	switch( ch ) {
	case 'd':
	    daemonize = 0;
	    break;
	    
	case 'e':
	    pidfile = optarg;
	    break;
	    
	case 'h':
	    usage();

	    return EXIT_SUCCESS;
	    
	case 'l':
	    if( ( incoming_port = atoi( optarg ) ) < 1 ) {
		usage();

		return EXIT_FAILURE;
	    }
	    break;	    

	case 'p':
	    if( ( parent_port = atoi( optarg ) ) < 1 ) {
		usage();

		return EXIT_FAILURE;
	    }
	    break;
	    
	case 's':
	    parent_name = optarg;
	    break;
	    
	case 'S':
#ifndef WITHSSL
	    fprintf(stderr, "Error: -S option, not built with SSL");
	    usage();
#else
	    dossl++;
#endif
	    break;
#ifdef	WITHSSL
	case 'C':
	    certfile = optarg;
	    break;
	case 'K':
	    keyfile = optarg;
	    break;
#endif
	case 'v':
	    pubsub_debug |= PUBSUB_DEBUG_CLUSTERD;
	    break;
	}

#ifdef	WITHSSL
    if (dossl) {
	 if (certfile == NULL || keyfile == NULL) {
	     ps_error("Must specify -K and -C options with -S!\n");
	     exit(-1);
	 }
	 if (ps_client_sslinit(certfile, keyfile)) {
	     exit(-1);
	 }
    }
#endif
    if (!daemonize) 
	    ps_loginit(0, NULL);
    else
	    ps_loginit( 1, "clusterd" );

retry:
    if( pubsub_alloc_handle( &handle ) < 0 )
	/* pubsub_connect already gave an error message */
	return EXIT_FAILURE;

#ifdef	WITHSSL
    if (dossl) {
	 rv = pubsub_sslconnect(parent_name, parent_port, &handle);
    }
    else
#endif
	rv = pubsub_connect(parent_name, parent_port, &handle);

    if (rv < 0) {
	fail();
	goto retry;
    }

    if( ( incoming = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0)
	ps_pfatal( "socket" );

    setsockopt( incoming, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes );

    i = 1;
    if (setsockopt(incoming, SOL_SOCKET, SO_KEEPALIVE, &i, sizeof(i)) < 0) {
	    ps_pwarning("setsockopt(SO_KEEPALIVE)");
    }
    i = 90;
    if (setsockopt(incoming, IPPROTO_TCP, TCP_KEEPIDLE, &i, sizeof(i)) < 0) {
	    ps_pwarning("setsockopt(TCP_KEEPIDLE)");
    }
    i = 180;
    if (setsockopt(incoming, IPPROTO_TCP, TCP_KEEPINTVL, &i, sizeof(i)) < 0) {
	    ps_pwarning("setsockopt(TCP_KEEPINTVL)");
    }
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons( incoming_port );
    if( bind( incoming, (struct sockaddr *) &sin, sizeof sin ) < 0 )
	ps_pfatal( "bind" );

    if( listen( incoming, SOMAXCONN ) < 0 )
	ps_pfatal( "listen" );

    if( !( listen_iohandler = pubsub_add_iohandler( handle, NULL, incoming, 0,
						    listen_callback, NULL,
						    &err ) ) ||
	!( subscription = pubsub_add_subscription( handle, "",
						   subscription_callback, NULL,
						   &err ) ) )
	ps_fatal( "%s\n", err.msg );

    if( daemonize ) {
	if (daemon(0, 0)) {
	    /* oh dear */
	    ps_pfatal( "fork" );
	}
	daemonize = 0; /* so we don't fork again if we retry */

	/*
	 * Stash the pid away.
	 */
	if (!geteuid()) {
	    char buf[BUFSIZ];
	    FILE *fp;
	
	    if (! pidfile) {
	        sprintf(buf, "%s/clusterd.pid", _PATH_VARRUN);
		pidfile = strdup(buf);
            }
	    fp = fopen(pidfile, "w");
	    if (fp != NULL) {
	        fprintf(fp, "%d\n", getpid());
		(void) fclose(fp);
            }
	}
    }

    succeed();
    keep_looping = 1;
    pubsub_mainloop( handle, &keep_looping, &err );
    ps_error("pubsub_mainloop returned\n");

    /* Something has gone very wrong (we've lost our state with our
       parent).  Clean everything up, and try again. */
    while( num_clients )
	kill_client( handle, num_clients - 1 );

    pubsub_remove_iohandler( handle, listen_iohandler, &err );
    pubsub_rem_subscription( handle, subscription, &err );
    close( incoming );

    pubsub_disconnect( handle );
    handle = NULL;

    fail();
    
    goto retry;
    
    return EXIT_SUCCESS;
}
