/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2018 University of Utah and the Flux Group.
 * All rights reserved.
 */

#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <sys/time.h>
#include <sys/param.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <assert.h>
#include <sys/types.h>
#include <setjmp.h>
#include <paths.h>
#include "pubsub.h"
#include "clientapi.h"
#include "log.h"
#ifdef WITHSSL
#include "ssl.h"
#endif

static int	verbose = 0;

/* Forward decls */
static void     local_notify_callback(pubsub_handle_t *handle,
				      pubsub_subscription_t *subscription,
				      pubsub_notification_t *notification,
				      void *data);

static pubsub_handle_t		*local_handle;
static pubsub_handle_t		*remote_handle;

char *usagestr = 
 "usage: testclient [options]\n"
 " -h              Display this message\n"
 " -d              Turn on debugging\n"
 " -f              Do not daemonize\n"
 " -v              Turn on verbose logging\n"
 " -s server       Specify a pubsub server to connect to\n"
 " -p portnum      Specify a pubsub port number to connect to\n"
 " -e pidfile      Specify pid file for writing process ID to\n"
#ifdef WITHSSL
 " -S              Connect using SSL to parent server\n"
 " -C              Specify the certificate file for SSL\n"
 " -K              Specify the key file for SSL\n"
#endif
 "\n";

void
usage()
{
	fprintf(stderr, "%s", usagestr);
	exit(1);
}

static void
setverbose(int sig)
{
	if (sig == SIGUSR1)
		verbose = 1;
	else
		verbose = 0;
}

int
main(int argc, char **argv)
{
	int			ch, rv;
	int			port = PUBSUB_SERVER_PORTNUM;
	int		        daemonize = 1;
	char			*server = "boss";
	char			buf[BUFSIZ];
	char			*pidfile = (char *) NULL;
	pubsub_error_t		pubsub_error;
	pubsub_subscription_t   *subscription;
	pubsub_notification_t   *notification;
#ifdef  WITHSSL
	char			*certfile = NULL, *keyfile = NULL;
	int			dossl = 0;
#endif
	pubsub_debug |= (PUBSUB_DEBUG_CONNECT|PUBSUB_DEBUG_RECONNECT);

	/*
	 * Override, we forward a lot of events, and the SSL server is
	 * getting and forwarding all the packets from all the clusters.
	 */
	pubsub_recv_sockbufsize = (1024 * 192);
	pubsub_send_sockbufsize = (1024 * 192);
	
	while ((ch = getopt(argc, argv, "hdvs:p:e:SK:C:f")) != -1) {
		switch(ch) {
		case 'd':
			pubsub_debug |= PUBSUB_DEBUG_CLUSTERD;
			break;
		case 'v':
			verbose++;
			break;
		case 'e':
			pidfile = optarg;
			break;
		case 'f':
			daemonize = 0;
			break;
		case 'p':
			if (sscanf(optarg, "%d", &port) == 0) {
				fprintf(stderr,
					"Error: -p value is not a number: "
					"%s\n",
					optarg);
				usage();
			}
			else if ((port <= 0) || (port >= 65536)) {
				fprintf(stderr,
					"Error: -p value is not between "
					"0 and 65536: %d\n",
					port);
				usage();
			}
			break;
		case 's':
			if (strlen(optarg) == 0) {
				fprintf(stderr, "Error: -s value is empty\n");
				usage();
			}
			else {
				server = optarg;
			}
			break;
		case 'S':
#ifndef WITHSSL
			fprintf(stderr, "Error: -S option, not built with SSL");
			usage();
#else
			dossl++;
#endif
			break;
#ifdef	WITHSSL
		case 'C':
			certfile = optarg;
			break;
		case 'K':
			keyfile = optarg;
			break;
#endif
		case 'h':
			fprintf(stderr, "%s", usagestr);
			exit(0);
			break;
		default:
			usage();
			break;
		}
	}

	argv += optind;
	argc -= optind;

	if (argc) {
		ps_error("Unrecognized command line arguments: %s\n", argv[0]);
		usage();
	}

	if (!server) {
		ps_error("Error: Could not deduce the name of the server!\n");
		usage();
	}
#ifdef	WITHSSL
	if (dossl) {
		if (certfile == NULL || keyfile == NULL) {
			ps_error("Must specify -K and -C options with -S!\n");
			exit(-1);
		}
		if (ps_client_sslinit(certfile, keyfile)) {
			exit(-1);
		}
	}
#endif
	if (pubsub_alloc_handle(&local_handle) < 0) {
		ps_fatal("Error: Could not allocate local handle\n");
	}
	if (pubsub_alloc_handle(&remote_handle) < 0) {
		ps_fatal("Error: Could not allocate remote handle\n");
	}
	notification = pubsub_notification_alloc(remote_handle, &pubsub_error);
	if (notification == NULL) {
		ps_fatal("Error: Could not allocate a notification!\n");
	}
	if (pubsub_notification_add_string(notification,
					   "__KEEPALIVE__", "YES!",
					   &pubsub_error) == -1) {
		ps_fatal("Error: %s\n", pubsub_error.msg);
	}
	printf("notification created\n");

	if (! (pubsub_debug & PUBSUB_DEBUG_CLUSTERD)) {
		ps_loginit(1, "pubsubd");
		if (daemonize) {
			if (daemon(0, 0) != 0)
				ps_fatal("Error: failed to daemonize\n");
		}
	}
	ps_info("igevent forwarded daemon starting\n");

	signal(SIGUSR1, setverbose);
	signal(SIGUSR2, setverbose);

	/*
	 * Stash the pid away.
	 */
	if (daemonize &&
	    ! (geteuid() || (pubsub_debug & PUBSUB_DEBUG_CLUSTERD))) {
		FILE *fp;
	
		if (! pidfile) {
			sprintf(buf, "%s/igevent_forward.pid", _PATH_VARRUN);
			pidfile = strdup(buf);
		}
		fp = fopen(pidfile, "w");
		if (fp != NULL) {
			fprintf(fp, "%d\n", getpid());
			(void) fclose(fp);
		}
	}

	/*
	 * We need to get the handles initially established. Once we have
	 * them, pubsub will handle reconnection after failure.
	 */
	while (1) {
		if (pubsub_connect("localhost", PUBSUB_SERVER_PORTNUM,
				   &local_handle) == 0) {
			ps_info("connected to local pubsubd server\n");
			break;
		}
		ps_error("Could not connect to local pubsub server!\n");
		sleep(10);
	}

	/*
	 * We want events with a urn in the SITE, these are the events
	 * we want to forward to the portal.
	 */
	if (!(subscription = pubsub_add_subscription(local_handle,
						     "SITE != \"*\"",
						     local_notify_callback,
						     NULL, &pubsub_error))) {
		ps_fatal("%s\n", pubsub_error.msg);
	}

	/*
	 * Connection to the upstream pubsubd.
	 */
	while (1) {
#ifdef	WITHSSL
		if (dossl) {
			rv = pubsub_sslconnect(server, port, &remote_handle);
		}
		else
#endif
			rv = pubsub_connect(server, port, &remote_handle);
		if (rv == 0) {
			ps_info("connected to remote pubsubd server\n");
			break;
		}

		ps_error("Could not connect to remote pubsub server!\n");
		sleep(10);
	}
	/* Ping the remote connection to keep it alive through firewalls */
	if (pubsub_set_idle_period(remote_handle, 60, &pubsub_error)) {
		ps_error("Could not start idle timer for remote handle!\n");
	}
	// Spin
	while (1) {
		sleep(5);
	}		
	return 0;
}

static void
local_notify_callback(pubsub_handle_t *handle,
		      pubsub_subscription_t *subscription,
		      pubsub_notification_t *notification,
		      void *arg)
{
	pubsub_error_t		myerror;
	char			*cp;

	if (pubsub_notification_get_string(notification,
					   "SITE", &cp, &myerror) != 0) {
		return;
	}
	if (strncmp(cp, "urn:", 4)) {
		//ps_info("Ignoring notification: %s\n", cp);
		return;
	}
	if (pubsub_debug & PUBSUB_DEBUG_CLUSTERD) {
		ps_info("Forwarding notification: %s\n", cp);
	}
	if (pubsub_notify(remote_handle, notification, &myerror)) {
		ps_error("Error forwarding notification: %d - %s\n",
			 myerror.error, myerror.msg);
	}
}
