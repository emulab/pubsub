/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2005-2018 University of Utah and the Flux Group.
 * All rights reserved.
 */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <assert.h>
#include "pubsub.h"
#include "queue.h"
#include "network.h"
#include "ssl.h"
#include "log.h"

/*
 * Allow clients to selectively override this.
 */
int		pubsub_send_sockbufsize = CLIENT_SEND_SOCKBUFSIZE;
int		pubsub_recv_sockbufsize = CLIENT_RECV_SOCKBUFSIZE;

static int readall(int, void *, size_t, int);
static int writeall(int, void *, size_t);
static int connect_with_timeout(int, struct sockaddr *, socklen_t,
				struct timeval *);

#ifdef WITHSSL
/*
 * This is quite silly, but do not want to redo everything.
 */
#ifdef FD_SETSIZE
#define MAX_CLIENTS	FD_SETSIZE
#else
#define MAX_CLIENTS	1024
#endif
static int isssl[MAX_CLIENTS];
#endif

/*
 * Client connect code. Just establishes network connection, nothing else.
 * Returns the socket. 
 */
int
ps_ClientConnect(char *server, int portnum, int usessl, int max_retries,
		 int *sockp, struct pubsub_error *pubsub_error)
{
	int			sock, cc, i;
	struct hostent		*he;
	struct in_addr		serverip;
	struct sockaddr_in	name;

#ifndef WITHSSL
	if (usessl) {
		pubsub_error_error(pubsub_error, EINVAL,
				   "SSL support is not compiled in");
		return -1;
	}
#endif
	/*
	 * Map server to IP.
	 */
	he = gethostbyname(server);
	if (he)
		memcpy((char *)&serverip, he->h_addr, he->h_length);
	else {
		pubsub_error_error(pubsub_error, EIO,
				   "gethostbyname(%s) failed: %s",
				   server, hstrerror(h_errno));
		return -1;
	}

	while (1) {
		struct timeval tv;
		int err;
		err = errno = 0;

		/* Create socket from which to read. */
		sock = socket(AF_INET, SOCK_STREAM, 0);
		if (sock < 0) {
			pubsub_error_error(pubsub_error, errno,
					   "creating stream socket: %s",
					   strerror(errno));
			return -1;
		}

		/* Create name. */
		name.sin_family = AF_INET;
		name.sin_addr   = serverip;
		name.sin_port   = htons(portnum);

		tv.tv_sec = 5;
		tv.tv_usec = 0;

		cc = connect_with_timeout(sock,
					  (struct sockaddr *) &name,
					  sizeof(name),
					  &tv);
		err = errno;

		/* Success */
		if (cc == 0)
			break;

		if (err != ECONNREFUSED && err != ETIMEDOUT) {
			pubsub_error_error(pubsub_error, err,
					   "connecting stream socket: %s",
					   strerror(err));
			close(sock);
			return -1;
		}

		close(sock);
		if (pubsub_debug & PUBSUB_DEBUG_CONNECT) 
			ps_error("Connection to pubsub server failed: %s\n",
			      strerror(err));
		if (--max_retries <= 0) {
			pubsub_error_error(pubsub_error, err,
					   "connecting stream socket: %s",
					   strerror(err));
			return -1;
		}
		sleep(5);
	}
	i = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, &i, sizeof(i)) < 0) {
		ps_errorc("setsockopt(SO_KEEPALIVE)");
		close(sock);
		return -1;
	}
	i = 90;
	if (setsockopt(sock, IPPROTO_TCP, TCP_KEEPIDLE, &i, sizeof(i)) < 0) {
		ps_errorc("setsockopt(TCP_KEEPIDLE)");
		close(sock);
		return -1;
	}
	i = 180;
	if (setsockopt(sock, IPPROTO_TCP, TCP_KEEPINTVL, &i, sizeof(i)) < 0) {
		ps_errorc("setsockopt(TCP_KEEPINTVL)");
		close(sock);
		return -1;
	}
	i = pubsub_send_sockbufsize;
	if (setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &i, sizeof(i)) < 0)
		ps_errorc("setsockopt(SO_SNDBUF)");
    
	i = pubsub_recv_sockbufsize;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &i, sizeof(i)) < 0)
		ps_errorc("setsockopt(SO_RCVBUF)");
#ifdef WITHSSL
	if (usessl) {
		int rval = ps_sslconnect(sock, NULL, 0);
		if (rval) {
			close(sock);
			return rval;
		}
		isssl[sock] = 1;
	}
#endif
	*sockp = sock;
	return 0;
}

/*
 * For the benefit of the server and SSL support.
 */
int
ps_ClientAccept(int sock, int dossl, struct sockaddr *addr, socklen_t *addrlen)
{
	if (dossl) {
#ifdef WITHSSL
		int newsock = ps_sslaccept(sock, addr, addrlen, 3000);
		if (newsock < 0) {
			return -1;
		}
		isssl[newsock] = 1;
		return newsock;
#else
		return -1;
#endif
	}
	
	return accept(sock, addr, addrlen);
}

/*
 * Client disconnect code. 
 */
int
ps_ClientDisconnect(int sock)
{
#ifdef WITHSSL
	if (isssl[sock]) {
		isssl[sock] = 0;
		return ps_sslclose(sock);
	}
#endif
	shutdown(sock, SHUT_RDWR);
	close(sock);
	return 0;
}

/*
 * Return the locally bound port for the socket.
 * Return 0 if none.
 */
int
ps_ClientPort(int sock)
{
	struct sockaddr_in n;
	socklen_t nl = sizeof(n);

	n.sin_port = 0;
	getsockname(sock, (struct sockaddr *)&n, &nl);

	return (int)ntohs(n.sin_port);
}

#ifdef BOGOCHECK
/* XXX debugging */
int bogosifypacket = 0;
#endif

/*
 * Read and write complete packets; packet length is stored in the first
 * word of course. We wait for a reply packet in this version. 
 */
int
ps_PacketSend(int socket, packet_t *packet, packet_t *reply)
{
	int hlen = ntohl(packet->header.len);
	int rv;
		
#ifdef BOGOCHECK
	/* don't send the whole packet to test timeout on the server */
	if (bogosifypacket && hlen > 1)
		hlen--;
#endif
	if ((rv = writeall(socket, packet, hlen)) <= 0) {
		if (rv == -2 && (pubsub_debug & PUBSUB_DEBUG_BADPACKETS))
			ps_error("ps_PacketSend: sent partial packet\n");
		return -1;
	}
	
	if (! reply)
		return 0;
#ifdef WITHSSL
	if (isssl[socket]) {
		return ps_sslread(socket, (void *)reply,
		                  (size_t)ntohl(reply->header.len));
	}
#endif	
	return ps_PacketRead(socket, reply, ntohl(reply->header.len));
}

int
ps_PacketRead(int socket, packet_t *packet, int maxlen)
{
	return ps_PacketReadTimo(socket, packet, maxlen, 0);
}

int
ps_PacketReadTimo(int socket, packet_t *packet, int maxlen, int timo)
{
	int hlen, dlen, vers, cc, ptype;

	if (0) {
		ps_info("ps_PacketReadTimo: %d %d %d", socket, maxlen, timo);
	}
	errno = 0;

	/*
	 * Read just the first part of the packet to get the total length.
	 */
	cc = readall(socket, &packet->header, sizeof(packet->header), timo);
	if (cc <= 0) {
		if (errno && (pubsub_debug & PUBSUB_DEBUG_BADPACKETS))
			ps_error("ps_PacketRead: error (%d) reading packet header\n",
			      errno);
		return -1;
	}

	ptype = ntohs(packet->header.type);
	hlen = ntohl(packet->header.len);
	/* Sanity check the header */
	if (hlen < sizeof(pheader_t)) {
		if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS) {
			ps_error("ps_PacketRead: packet too short (%u < %u)\n",
				 packet->header.len, sizeof(pheader_t));
		}
		errno = EINVAL;
		return -1;
	}
	if (hlen > sizeof(maxpacket_t)) {
		if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS) {
			ps_error("ps_PacketRead: packet too long (%u > %u)\n",
				 packet->header.len, sizeof(maxpacket_t));
		}
		errno = EINVAL;
		return -1;
	}
	dlen = hlen - sizeof(pheader_t);

	/* Make sure there is something besides the header */
	if (dlen == 0) {
		/*
		 * XXX ugh...the server replies for some packets have
		 * the same type, but just have no body.  So not every
		 * "null" packet is an error.
		 */
		switch (ptype) {
		case PUBSUB_PKT_SUBSCRIBE:
		case PUBSUB_PKT_UNSUBSCRIBE:
		case PUBSUB_PKT_NOTIFY:
		case PUBSUB_PKT_PING:
			break;
		default:
			if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
				ps_error("ps_PacketRead: no packet body\n");
			break;
		}
		return 0;
	}

	/* Perform some basic sanity checking on the header */
	switch (ptype) {
	case PUBSUB_PKT_CONNECT:
	case PUBSUB_PKT_DISCONNECT:
		/* Unused? */
		break;
	case PUBSUB_PKT_SUBSCRIBE:
		/*
		 * Subscription packets must contain at least a token
		 * and a null expression (zero-length string).
		 */
		if (hlen <= sizeof(subscription_packet_t)) {
			if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
				ps_error("ps_PacketRead: short subscription packet (%u <= %u)\n",
					 hlen, sizeof(subscription_packet_t));
			errno = EINVAL;
			return -1;
		}
		break;
	case PUBSUB_PKT_UNSUBSCRIBE:
		/*
		 * Unsubscription packets must contain a token.
		 */
		if (hlen < sizeof(subscription_packet_t)) {
			if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
				ps_error("ps_PacketRead: short unsubscription packet (%u < %u)\n",
					 hlen, sizeof(subscription_packet_t));
			errno = EINVAL;
			return -1;
		}
		break;
	case PUBSUB_PKT_NOTIFICATION:
		/*
		 * Notifications must have token/used.
		 */
		if (hlen < sizeof(notification_packet_t)) {
			if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
				ps_error("ps_PacketRead: short notification packet (%u < %u)\n",
					 hlen, sizeof(notification_packet_t));
			errno = EINVAL;
			return -1;
		}
		break;
	case PUBSUB_PKT_NOTIFY:
		/*
		 * Unfortunately, a NOTIFY packet may be a request (needs
		 * additional fields) or a reply (no additional fields)
		 * so we cannot further check here.
		 */
		break;
	default:
		if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
			ps_error("ps_PacketRead: bad packet type %d\n",
				 ptype);
		errno = EINVAL;
		return -1;
	}
	vers = ntohl(packet->header.vers);
	if (vers < PUBSUB_MIN_VERSION || vers > PUBSUB_MAX_VERSION) {
		if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
			ps_error("ps_PacketRead: invalid packet version %d\n",
				 vers);
		errno = EINVAL;
		return -1;
	}

	if (0) {
		ps_info("ps_PacketReadTimo: %d %d %d", socket, dlen, timo);
	}

	if (dlen > 0 && readall(socket, packet->data, dlen, timo) <= 0) {
		if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
			ps_error("ps_PacketRead: error reading packet body\n");
		return -1;
	}

	/* Perform basic sanity checking on the body */
	switch (ptype) {

	/* make sure subscription expression is null terminated */
	case PUBSUB_PKT_SUBSCRIBE:
		if (packet->data[dlen-1] != '\0') {
			if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
				ps_error("ps_PacketRead: non-terminated subscription expression\n");
#if 0
			/* fix or drop? */
			packet->data[dlen-1] = '\0';
#endif
		}
		break;

	/* the notification "used" field must be <= total packet size */
	/* XXX recall that NOTIFY reply packets may not have a body! */
	case PUBSUB_PKT_NOTIFY:
	case PUBSUB_PKT_NOTIFICATION:
	{
		notification_packet_t *npacket = 
			(notification_packet_t *) packet;
		if (hlen >= sizeof(notification_packet_t) &&
		    ntohl(npacket->used) > dlen) {
			if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
				ps_error("ps_PacketRead: truncated notification: (%d > %d)\n",
				      ntohl(npacket->used), dlen);
			/* Need to be defensive here; drop the packet. */
			return -1;
		}
		break;
	}		

	default:
		break;
	}

	return 0;
}

/*
 * Read all of the requested bytes from a socket.
 */
static int
readall(int socket, void *buffer, size_t len, int timo)
{
	int		offset = 0, error = 0, retval = 0;
	struct timeval	tv, now, then;
	fd_set		rfds;

	if (0) {
		ps_info("readall: %d %ld %d\n", socket, len, timo);
	}
	
	/* XXX this can go away if we switch to using poll() */
	if (socket >= FD_SETSIZE) {
		errno = EMFILE;
		return -1;
	}

	if (timo) {
		tv.tv_sec = timo;
		tv.tv_usec = 0;
		gettimeofday(&then, NULL);
		timeradd(&then, &tv, &then);
	}

	while((offset < len) && !error) {
		int	rc;

		if (timo) {
#ifdef WITHSSL
			/*
			 * SSL will buffer data inside the ssl object, so
			 * we cannot use select until we check to see if
			 * there is stuff to read.
			 */
			if (!isssl[socket] ||
			    ps_sslpending(socket) == 0)
#endif
			{
				gettimeofday(&now, NULL);
				if (timercmp(&now, &then, >=)) {
					rc = 0;
				} else {
					timersub(&then, &now, &tv);
					FD_ZERO(&rfds);
					FD_SET(socket, &rfds);
					rc = select(socket+1, &rfds,
						    NULL, NULL, &tv);
				}
				if (rc <= 0) {
					if (rc == 0)
						errno = ETIMEDOUT;
					return rc;
				}
			}
		}
#ifdef WITHSSL
		if (isssl[socket]) {
			rc = ps_sslread(socket,
					&((char *)buffer)[offset], len - offset);
		}
		else
#endif
			rc = read(socket,
				  &((char *)buffer)[offset], len - offset);
		
		if (rc < 0) {
			if (errno != EINTR) {
				error = 1;
				retval = -1;
			}
		}
		else if (rc == 0) {
			/* Connection was closed. */
			len = 0;
			retval = 0;
		}
		else {
			offset += rc;
			retval += rc;
		}
	}
	return retval;
}

/*
 * Write all of the bytes to a socket.
 */
static int
writeall(int socket, void *buffer, size_t len)
{
	int			retval = 0;
	fd_set			wfds;
	int			flags;
	struct timeval		tv;
#ifdef MSG_NOSIGNAL
	flags = MSG_NOSIGNAL;
#else
	flags = 0;
#endif
	if (0) {
		ps_info("writeall: %d %ld\n", socket, len);
	}

	/* XXX this can go away if we switch to using poll() */
	if (socket >= FD_SETSIZE) {
		errno = EMFILE;
		return -1;
	}

	while (len) {
		int rc;
		
		/* shouldn't need to zero everytime, but just to be safe */
		FD_ZERO(&wfds);
		FD_SET(socket, &wfds);
		tv.tv_sec  = 1;
		tv.tv_usec = 0;

		rc = select(socket + 1, NULL, &wfds, NULL, &tv);
		if (rc <= 0) {
			if (rc == 0)
				errno = ETIMEDOUT;
			return (retval ? -2 : -1);
		}
#ifdef WITHSSL
		if (isssl[socket]) {
			rc = ps_sslwrite(socket, (const void *)buffer, len);
		}
		else
#endif
			rc = send(socket, buffer, len, flags);
		if (rc < 0) {
			if (errno != EINTR)
				return (retval ? -2 : -1);
		}
		else if (rc == 0) {
			/* XXX should never happen, but don't get stuck */
			return (retval ? -2 : -1);
		}
		else {
			len    -= rc;
			buffer += rc;
			retval += rc;
		}
	}
	return retval;
}

/*
 * Export these for the benefit of clusterd and SSL.
 */
int
ps_Read(int socket, void *buffer, size_t len)
{
	return readall(socket, buffer, len, 0);
}

int
ps_Write(int socket, void *buffer, size_t len)
{
	return writeall(socket, buffer, len);
}

/*
 * Inspired by the Stevens' connect_nonb function
 * Return 0 if connection was successful, -1 and errno==ETIMEDOUT if the
 * connection timed-out, and -1 and errno if anything else went wrong.
 */
static int
connect_with_timeout(int s, struct sockaddr *name, socklen_t namelen,
		     struct timeval *tv)
{
	int oflags, rv, myerr = 0;
	fd_set rset, wset;
	socklen_t len;

	/* XXX this can go away if we switch to using poll() */
	if (s >= FD_SETSIZE) {
		errno = EMFILE;
		return -1;
	}

	/*
	 * Make the socket non-blocking if it isn't
	 */
	if ((oflags = fcntl(s, F_GETFL, 0)) == -1 ||
	    fcntl(s, F_SETFL, oflags|O_NONBLOCK) == -1)
		return -1;

	/*
	 * Attempt the connect, if it returns anything other than in-progress,
	 * return.  Zero indicates a successful connection.
	 */
	rv = connect(s, name, namelen);
	if (rv == 0 || errno != EINPROGRESS) {
		myerr = rv ? errno : 0;
		goto done;
	}

	/*
	 * Select on the socket, will return ready-to-write if connected.
	 */
	FD_ZERO(&rset);
	FD_SET(s, &rset);
	wset = rset;
	rv = select(s+1, &rset, &wset, 0, tv);
	if (rv <= 0) {
		myerr = rv ? errno : ETIMEDOUT;
		goto done;
	}

	/*
	 * Will also return ready if there was an error on the socket,
	 * so read the socket error to make sure.
	 */
	assert(FD_ISSET(s, &rset) || FD_ISSET(s, &wset));
	len = sizeof(myerr);
	if (getsockopt(s, SOL_SOCKET, SO_ERROR, &myerr, &len) < 0)
		myerr = errno;

 done:
	/*
	 * Restore the blocking state of the socket and return any error.
	 */
	fcntl(s, F_SETFL, oflags);
	if (myerr) {
		errno = myerr;
		return -1;
	}
	errno = 0;
	return 0;
}
