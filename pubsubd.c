/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2005-2022 University of Utah and the Flux Group.
 * All rights reserved.
 */

#include <inttypes.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <stdarg.h>
#include <assert.h>
#include <sys/fcntl.h>
#include <paths.h>
#include "pubsub.h"
#include "queue.h"
#include "network.h"
#include "clientapi.h"
#include "log.h"
#include "bool.h"
#ifdef WITHSSL
#include "ssl.h"
#endif

/*
 * The maximum number of clients is dictated by the FD_* macros
 * used in the network and client code for select() calls. The limit
 * there is FD_SETSIZE which, at least under FreeBSD, can be increased
 * by an application by redefining it before any use (see the select
 * man page). Not sure this is supported behavior under Linux, so
 * beware!
 */
#ifdef FD_SETSIZE
#define MAX_CLIENTS	FD_SETSIZE
#else
#define MAX_CLIENTS	1024
#endif

/* Too many consecutive failures, dump client */
#define MAX_CONSECUTIVE_CLIENTFAILS 3

int		verbose	       = 0;
int		dostats        = 0;
int		measure        = 0;
static int	portnum	       = PUBSUB_SERVER_PORTNUM;
static int	sslportnum     = PUBSUB_SERVER_SSLPORTNUM;
static int	dossl          = 0;
static int      testmode       = 0;
static int	pingreply      = 1; /* Turn this off for debugging idle */
static int	readtimo       = 0;
static int      sockbufsize    = SOCKBUFSIZE;

/* protos */
static int	makesocket(int portnum, int *tcpsockp);
static void     remove_tcpclient(int sock);
static int      add_tcpclient(int sock, struct sockaddr_in *client);
static int      handle_request(int sock, maxpacket_t *packet);
static void	Cleanup();
static int      ConnectToParent(char *, int, char *, pubsub_handle_t **);

/*
 * This describes a subscription in the server. Each can client can have
 * more then one subscription active, so each one is marked by a unique
 * token for that client. The client provides the token.
 */
struct subscr_ctrl {
	int			client_token;
	unsigned int		notes_emitted;	/* stats */
	unsigned int		notes_failed;	/* stats */
	int			match_all;      /* flag */
	char			*expression;    /* ie: "(a == 1 or b == 3)" */
	queue_chain_t		chain;
	programT		compiled_expression;
	bindingsT		variable_bindings;
};
typedef struct subscr_ctrl subscr_ctrl_t;

/*
 * This is the client structure, one per socket.
 */
struct client_ctrl {
	int			index;		/* For search */
	int			sock;
	int			error;
	int			isparent;	/* an upstream connection */
	pubsub_handle_t		*parent_handle; /* upstream handle */
	unsigned int		notes_recieved;	/* stats */
	unsigned int		notes_emitted;	/* stats */
	unsigned int		notes_failed;	/* stats */
	int                     consecutive_fails; /* Too many; dump client */
	struct in_addr		ipaddr;		/* Debugging */
	queue_head_t		subscriptions;	/* Linked list */
	queue_chain_t		chain;	
};
typedef struct client_ctrl client_ctrl_t;
static queue_head_t AllClients;
static int      notify_dispatch_forward(client_ctrl_t *, packet_t *);

/*
 * Map TCP sockets onto the clients. Maybe add UDP sockets later.
 */
struct {
	client_ctrl_t	*client;
} allsockets[MAX_CLIENTS];

/*
 * Stats.
 */
#define STATS_INTERVAL	900	/* 15 minutes */

struct pubsub_stats {
	int	        current_clients;
	unsigned int	total_clients;
	unsigned int	current_subscriptions;
	unsigned int	total_subscriptions;
	unsigned int	notifications_received;
	unsigned int	notifications_emitted;
	unsigned int	notifications_failed;
} pubsub_stats;
sigset_t blocksigmask;
#define BLOCKSIGNALS()	sigprocmask(SIG_BLOCK, &blocksigmask, NULL);
#define UNBLOCKSIGNALS()	sigprocmask(SIG_UNBLOCK, &blocksigmask, NULL);

static char *usagestr =
 "usage: emulab-pubsubd [-d] [-p #]\n"
 " -h              Display this message\n"
 " -V              Print version information and exit\n"
 " -d              Do not daemonize.\n"
 " -i              Turn off ping replies (for debugging)\n"
 " -v              Turn on verbose logging\n"
 " -s              Turn on statistics logging\n"
 " -B bufsize      Set the socket buffer size to bufsize K bytes.\n"
#ifdef WITHSSL
 " -S              Turn on SSL connection handling\n"
 " -C              Specify the certificate file for SSL\n"
 " -K              Specify the key file for SSL\n"
 " -A              Specify the trusted CAs file for SSL\n"
 " -P portnum      Listen for SSL enabled connections on this portnum\n"
#endif
 " -e pidfile      Specify pid file for writing process ID to\n"
 " -l logfile      Specify log file instead of syslog\n"
 " -p portnum      Specify a port number to listen on\n"
 " -T timeout      Max time in seconds to read a single message\n"
 "\n";

void
usage()
{
	fprintf(stderr, "%s", usagestr);
	exit(1);
}

static void
sigint(int sig)
{
	ps_info("Caught a signal: %d\n", sig);
	Cleanup();
	exit(0);
}

static void
dumpstats(int sig)
{
	BLOCKSIGNALS();
	ps_info("Totals: total:%u (%u) current:%u (%d) "
		"received:%u emitted:%u failed:%u\n",
		pubsub_stats.total_subscriptions,
		pubsub_stats.total_clients,
		pubsub_stats.current_subscriptions,
		pubsub_stats.current_clients,
		pubsub_stats.notifications_received,
		pubsub_stats.notifications_emitted,
		pubsub_stats.notifications_failed);
	
	if (verbose || pubsub_debug & PUBSUB_DEBUG_SUBSCRIBE) {
		client_ctrl_t		*client;
		subscr_ctrl_t		*subscription;
		
		queue_iterate(&AllClients, client, client_ctrl_t *, chain) {
			ps_info(" Client:%s sock:%d received:%u "
			     "emitted:%u failed:%u (%d)\n",
			     inet_ntoa(client->ipaddr), client->sock,
			     client->notes_recieved,
			     client->notes_emitted, client->notes_failed,
			     client->consecutive_fails);

			queue_iterate(&client->subscriptions,
				      subscription, subscr_ctrl_t *, chain) {
				ps_info("   Sub: token:%d emitted:%u failed:%u\n",
				     subscription->client_token,
				     subscription->notes_emitted,
				     subscription->notes_failed);
			}
		}
	}
	UNBLOCKSIGNALS();
	alarm(STATS_INTERVAL);
}

static void
dump_subscriptions(int sig)
{
	client_ctrl_t		*client;
	subscr_ctrl_t		*subscription;
		
	BLOCKSIGNALS();
	ps_info("Totals: total:%u (%u) current:%u (%d) received:%u "
		"emitted:%u failed:%u\n",
		pubsub_stats.total_subscriptions,
		pubsub_stats.total_clients,
		pubsub_stats.current_subscriptions,
		pubsub_stats.current_clients,
		pubsub_stats.notifications_received,
		pubsub_stats.notifications_emitted,
		pubsub_stats.notifications_failed);
	
	queue_iterate(&AllClients, client, client_ctrl_t *, chain) {
		ps_info(" Client:%s sock:%d received:%u emitted:%u "
		     "failed:%u (%d)\n",
		     inet_ntoa(client->ipaddr), client->sock,
		     client->notes_recieved,
		     client->notes_emitted, client->notes_failed,
		     client->consecutive_fails);

		queue_iterate(&client->subscriptions,
			      subscription, subscr_ctrl_t *, chain) {
			ps_info("   Sub: token:%d emitted:%u failed:%u expression:'%s'\n",
			     subscription->client_token,
			     subscription->notes_emitted,
			     subscription->notes_failed,
			     subscription->expression);
		}
	}
	UNBLOCKSIGNALS();
}

static void
setverbose(int sig)
{
	if (sig == SIGUSR1)
		verbose = 1;
	else
		verbose = 0;

	dump_subscriptions(sig);
}

int
main(int argc, char **argv)
{
	int			tcpsock, ssltcpsock = -1, ch, retries;
	unsigned int		numfds;
	FILE			*fp;
	char			buf[BUFSIZ], *cp;
	extern char		build_info[];
	struct pollfd		fds[MAX_CLIENTS];
	char			*logfile = (char *) NULL;
	char			*pidfile = (char *) NULL;
	char			*parent  = (char *) NULL;
	int			parent_port = PUBSUB_SERVER_PORTNUM;
	int			parent_socket = 0;
	pubsub_handle_t		*parent_handle = (pubsub_handle_t *) NULL;
	char			*parent_expression = "";
	pubsub_error_t		pubsub_error;
	int			daemonize = 1;
#ifdef  WITHSSL
	char		        *certfile = NULL, *keyfile = NULL;
	char			*trustedfile = NULL;
#endif

	pubsub_debug = PUBSUB_DEBUG_READTIMO|PUBSUB_DEBUG_BADPACKETS|
		PUBSUB_DEBUG_SRVCONNECT|PUBSUB_DEBUG_RECONNECT|
		PUBSUB_DEBUG_SUBSCRIBE;
	while ((ch = getopt(argc, argv,
			    "hVdp:vil:tsmb:T:e:SP:A:C:K:c:E:F:B:")) != -1)
		switch(ch) {
		case 'B':
			if (sscanf(optarg, "%d", &sockbufsize) == 0) {
				fprintf(stderr,
					"Error: -B value is not a number: "
					"%s\n",
					optarg);
				usage();
			}
			else if ((sockbufsize < 32) || (sockbufsize > 256)) {
				fprintf(stderr,
					"Error: -B value is not between "
					"32 and 256: %d\n",
					sockbufsize);
				usage();
			}
			sockbufsize = sockbufsize * 1024;
			break;
		case 'c':
			parent = strdup(optarg);
			if ((cp = strchr(parent, ':')) != NULL) {
				*cp = '\0';
				parent_port = atoi(++cp);
			}
			break;
		case 'E':
			parent_expression = optarg;
			break;
		case 'F':
			fp = fopen(optarg, "r");
			if (fp == NULL) {
				ps_fatal("File does not exist: %s\n", optarg);
			}
			if (fgets(buf, sizeof(buf), fp) == NULL) {
				ps_fatal("Cannot read subscription expression\n");
			}
			(void) fclose(fp);
			if ((cp = strchr(buf, '\n')) != NULL) {
				*cp = '\0';
			}
			parent_expression = strdup(buf);
			break;
		case 'l':
			if (strlen(optarg) > 0) {
				logfile = optarg;
			}
			break;
		case 'p':
			if (sscanf(optarg, "%d", &portnum) == 0) {
				fprintf(stderr,
					"Error: -p value is not a number: "
					"%s\n",
					optarg);
				usage();
			}
			else if ((portnum <= 0) || (portnum >= 65536)) {
				fprintf(stderr,
					"Error: -p value is not between "
					"0 and 65536: %d\n",
					portnum);
				usage();
			}
			break;
		case 'P':
			if (sscanf(optarg, "%d", &sslportnum) == 0) {
				fprintf(stderr,
					"Error: -P value is not a number: "
					"%s\n",
					optarg);
				usage();
			}
			else if ((sslportnum <= 0) || (sslportnum >= 65536)) {
				fprintf(stderr,
					"Error: -P value is not between "
					"0 and 65536: %d\n",
					sslportnum);
				usage();
			}
			break;
		case 'd':
			/* XXX hack to approximate old behavior */
			if (daemonize) {
				pubsub_debug |= (PUBSUB_DEBUG_CONNECT|
						 PUBSUB_DEBUG_RECONNECT|
						 PUBSUB_DEBUG_CALLBACK|
						 PUBSUB_DEBUG_DISPATCHER|
						 PUBSUB_DEBUG_SRVCONNECT);
			} else {
				pubsub_debug |= PUBSUB_DEBUG_DETAIL;
			}
			daemonize = 0;
			break;
		case 't':
			testmode++;
			break;
		case 'i':
			pingreply = 0;
			break;
		case 'e':
			pidfile = optarg;
			break;
		case 'v':
			verbose++;
			break;
		case 's':
			dostats++;
			break;
		case 'S':
#ifndef WITHSSL
			fprintf(stderr, "Error: -S option, not built with SSL");
			usage();
#endif
			dossl++;
			break;
#ifdef	WITHSSL
		case 'A':
			trustedfile = optarg;
			break;
		case 'C':
			certfile = optarg;
			break;
		case 'K':
			keyfile = optarg;
			break;
#endif
		case 'm':
			measure++;
			break;
		case 'T':
			readtimo = atoi(optarg);
			break;
		case 'V':
			fprintf(stderr, "%s\n", build_info);
			exit(0);
			break;
		case 'h':
		case '?':
		default:
			usage();
			break;
		}
	argc -= optind;
	argv += optind;

	if (argc) {
		fprintf(stderr,
			"Error: Unrecognized command line arguments: %s ...\n",
			argv[0]);
		usage();
	}
#ifdef	WITHSSL
	if (dossl && !(certfile && keyfile && trustedfile)) {
		ps_error("Must supply -A,-C,-K options with -S!\n");
		exit(1);
	}
#endif
	if (!daemonize) 
		ps_loginit(0, logfile);
	else {
		if (logfile)
			ps_loginit(0, logfile);
		else
			ps_loginit(1, "pubsubd");
	}
	ps_info("daemon starting (version %d, debug=0x%x, "
		"maxclients=%d, sockbufsize=%dKB)\n",
		PUBSUB_VERSION, pubsub_debug, MAX_CLIENTS, sockbufsize/1024);
	ps_info("%s\n", build_info);

	queue_init(&AllClients);

	/*
	 * Create TCP server.
	 */
	if (makesocket(portnum, &tcpsock) < 0) {
		ps_error("Could not make primary tcp socket!");
		exit(1);
	}
#ifdef	WITHSSL
	if (dossl && makesocket(sslportnum, &ssltcpsock) < 0) {
		ps_error("Could not make SSL tcp socket!");
		exit(1);
	}
#endif
	/*
	 * If we are a clusterd, connect to upstream. This will try
	 * forever, which might present a problem.
	 */
	if (parent &&
	    ConnectToParent(parent,
			    parent_port, parent_expression, &parent_handle)) {
		exit(34);
	}

	/* Now become a daemon */
	if (daemonize && daemon(0, 0)) {
		ps_errorc("Could not daemonize");
		exit(1);
	}

	signal(SIGUSR1, setverbose);
	signal(SIGUSR2, setverbose);

	signal(SIGINT,  sigint);
	signal(SIGTERM, sigint);

	sigemptyset(&blocksigmask);
	sigaddset(&blocksigmask, SIGUSR1);
	sigaddset(&blocksigmask, SIGUSR2);
	if (dostats) {
		sigaddset(&blocksigmask, SIGALRM);
		signal(SIGALRM, dumpstats);
		alarm(STATS_INTERVAL);
	}
	
	/*
	 * Stash the pid away.
	 */
	if (!geteuid()) {
		if (! pidfile) {
			sprintf(buf, "%s/pubsubd.pid", _PATH_VARRUN);
			pidfile = buf;
		}
		fp = fopen(pidfile, "w");
		if (fp != NULL) {
			fprintf(fp, "%d\n", getpid());
			(void) fclose(fp);
		}
	}
	/*
	 * Now sit and listen for connections.
	 */
	memset(fds, 0, sizeof(fds));
	fds[0].fd = tcpsock;
	fds[0].events = POLLIN;
	allsockets[tcpsock].client = (client_ctrl_t *)-1; /* sentinel */
	numfds = 1;
#ifdef	WITHSSL
	if (dossl) {
		if (ps_server_sslinit(certfile, keyfile, trustedfile)) {
			ps_error("Could not initialize SSL server!");
			exit(1);
		}
		fds[1].fd = ssltcpsock;
		fds[1].events = POLLIN;
		allsockets[ssltcpsock].client = (client_ctrl_t *)-1;
		numfds++;
	}
#endif
	retries = 0;
	
	while (1) {
		struct sockaddr_in	client;
		int			cc, newsock, i, j;
		int			current_size;
		socklen_t		length = sizeof(client);
		int			compact_fds = 0;

		/*
		 * If we lose our connection to the parent, we are going to
		 * stop processing and wait till we reestablish. This is going
		 * to stop event processing, but that is probably okay for now
		 * since we use clustering for sending events in the downward
		 * direction, but not upwards.
		 */
		if (parent && parent_socket != parent_handle->sock) {
			if (parent_handle->sock < 0) {
				ps_error("Lost our connection to our parent\n");
				pubsub_daemon_reconnect(parent_handle,
							&pubsub_error);
				ps_error("Connection to our parent restored\n");
			}

			/*
			 * Yes, we are reaching into the handle to get the
			 * socket.  Since we can lose the connection to the
			 * parent, we have to watch for the socket changing
			 * in our loop.
			 */
			parent_socket = parent_handle->sock;

			/*
			 * We want the socket to look like a normal client.
			 */
			if (getpeername(parent_socket,
					(struct sockaddr *)&client, &length)) {
				ps_errorc("getpeername()");
				exit(1);
			}
			if (add_tcpclient(parent_socket, &client) != 0) {
				ps_error("Could not add upstream tcp client");
				exit(1);
			}
			fds[numfds].fd = parent_socket;
			fds[numfds].events = POLLIN;
			numfds++;
			/* Mark as parent */
			allsockets[parent_socket].client->isparent = 1;
			allsockets[parent_socket].client->parent_handle = parent_handle;
		}
		errno = 0;
		i = poll(fds, numfds, -1);
		if (i < 0) {
			if (errno == EINTR) {
				retries = 0;
				continue;
			}
			/* XXX have seen this once, I think it is transient */
			if (errno == ENOBUFS && retries++ < 3) {
				ps_errorc("poll");
				sleep(1);
				continue;
			}
			ps_fatal("poll: %s", strerror(errno));
		} else
			retries = 0;

		if (i == 0)
			continue;

		current_size = numfds;
		for (i = 0; i < current_size; i++) {
			if (fds[i].revents == 0)
				continue;

			if (fds[i].fd == tcpsock ||
			    fds[i].fd == ssltcpsock) {
				length  = sizeof(client);
#ifdef WITHSSL
				if (fds[i].fd == ssltcpsock) {
					newsock = ps_ClientAccept(ssltcpsock, 1,
						 (struct sockaddr *)&client,
						  &length);
				}
				else
#endif
					newsock = ps_ClientAccept(tcpsock, 0,
						 (struct sockaddr *)&client,
						 &length);
				if (newsock < 0) {
					ps_errorc("accepting TCP connection");
					continue;
				}

				if (add_tcpclient(newsock, &client) != 0) {
					ps_ClientDisconnect(newsock);
					continue;
				}
				fds[numfds].fd = newsock;
				fds[numfds].events = POLLIN;
				numfds++;
				continue;
			}
			else {
				maxpacket_t	packet;
				if (fds[i].revents & POLLHUP) {
					cc = -1;
					if (pubsub_debug & PUBSUB_DEBUG_READTIMO)
						ps_info("%s: detected client disconnect\n",
							inet_ntoa((allsockets[fds[i].fd].client)->ipaddr));
				} else {
					cc = ps_PacketReadTimo(fds[i].fd,
							       (packet_t *) &packet,
							       sizeof(packet), readtimo);
					if ((pubsub_debug & PUBSUB_DEBUG_READTIMO) &&
					    cc < 0 && errno == ETIMEDOUT)
						ps_info("%s: timeout reading packet\n",
							inet_ntoa((allsockets[fds[i].fd].client)->ipaddr));
				}

				/* Drop the connection; it can reconnect */
				if (cc < 0 ||
				    handle_request(fds[i].fd, &packet) < 0) {
					client_ctrl_t	*controlp =
						allsockets[fds[i].fd].client;
					
					if (controlp->isparent) {
						pubsub_daemon_dropconnect(parent_handle);
					}
					else {
						ps_ClientDisconnect(fds[i].fd);
					}
					remove_tcpclient(fds[i].fd);
					fds[i].fd = -1;
					compact_fds = 1;
					continue;
				}
			}

		}

		/*
		 * Look for troublesome clients and cut their heads off.
		 * Skip the sentinal in the first positions.
		 */
		for (j = 2; j < numfds; j++) {
			int             s = fds[j].fd;
			client_ctrl_t	*controlp;
				
			if (s == -1)
				continue;

			controlp = allsockets[s].client;
			if (controlp->consecutive_fails >
			    MAX_CONSECUTIVE_CLIENTFAILS) {
				ps_info("%s: dropping after too many failures\n",
				     inet_ntoa(controlp->ipaddr));
				if (controlp->isparent) {
					pubsub_daemon_dropconnect(parent_handle);
				}
				else {
					ps_ClientDisconnect(s);
				}
				remove_tcpclient(s);
				fds[j].fd = -1;
				compact_fds = 1;
			}
		}

		if (compact_fds) {
			for (i = 0; i < numfds; i++) {

				if (fds[i].fd != -1)
					continue;

				memmove(&fds[i], &fds[i+1],
					(numfds - i - 1) * sizeof(fds[0]));
				i--;
				numfds--;
			}
			compact_fds = 0;
		}
	}
	close(tcpsock);
	ps_info("daemon terminating\n");
	exit(0);
}

/*
 * Create sockets on specified port.
 */
static int
makesocket(int portnum, int *tcpsockp)
{
	struct sockaddr_in	name;
	int			i, tcpsock;
	socklen_t		length;

	/*
	 * Setup TCP socket for incoming connections.
	 */

	/* Create socket from which to read. */
	tcpsock = socket(AF_INET, SOCK_STREAM, 0);
	if (tcpsock < 0) {
		ps_pfatal("opening stream socket");
	}

	i = 1;
	if (setsockopt(tcpsock, SOL_SOCKET, SO_REUSEADDR,
		       (char *)&i, sizeof(i)) < 0)
		ps_pwarning("setsockopt(SO_REUSEADDR)");;
	i = 1;
	if (setsockopt(tcpsock, SOL_SOCKET, SO_KEEPALIVE, &i, sizeof(i)) < 0) {
		ps_pwarning("setsockopt(SO_KEEPALIVE)");
	}
	i = 90;
	if (setsockopt(tcpsock, IPPROTO_TCP, TCP_KEEPIDLE, &i, sizeof(i)) < 0) {
		ps_pwarning("setsockopt(TCP_KEEPIDLE)");
	}
	i = 180;
	if (setsockopt(tcpsock, IPPROTO_TCP, TCP_KEEPINTVL, &i, sizeof(i)) < 0) {
		ps_pwarning("setsockopt(TCP_KEEPINTVL)");
	}
	
	/* Create name. */
	name.sin_family = AF_INET;
	name.sin_addr.s_addr = INADDR_ANY;
	name.sin_port = htons((u_short) portnum);
	if (bind(tcpsock, (struct sockaddr *) &name, sizeof(name))) {
		ps_pfatal("binding stream socket");
	}
	/* Find assigned port value and print it out. */
	length = sizeof(name);
	if (getsockname(tcpsock, (struct sockaddr *) &name, &length)) {
		ps_pfatal("getsockname");
	}
	if (listen(tcpsock, 128) < 0) {
		ps_pfatal("listen");
	}
	*tcpsockp = tcpsock;
	ps_info("listening on TCP port %d\n", ntohs(name.sin_port));
	
	return 0;
}

/*
 * Prevent SIGPIPE when writing to a disconnected socket,
 * we will check the error return instead.  There are three
 * different ways to do this:
 *
 * 1. Per-write with the MSG_NOSIGNAL flag.  This is the preferred
 *    way and is supported by all Linuxes and newer (6+) BSDs.
 *
 * 2. Per-socket with the SO_NOSIGPIPE socket option.  This is a
 *    newer BSD (5+) option and is used only if MSG_NOSIGNAL is
 *    not available.
 *
 * 3. Per-process by ignoring SIGPIPE.  This is the last resort
 *    for older BSDs which don't support either of the former.
 */
static int
stopsigpipe(int sock)
{
	static int beenhere = 0;

#ifdef MSG_NOSIGNAL
	if (!beenhere) {
		ps_info("Using MSG_NOSIGNAL to block SIGPIPE\n");
		beenhere = 1;
	}
#else
#ifdef SO_NOSIGPIPE
	int on = 1;

	if (!beenhere) {
		ps_info("Using SO_NOSIGPIPE to block SIGPIPE\n");
		beenhere = 1;
	}

	if (setsockopt(sock, SOL_SOCKET, SO_NOSIGPIPE, &on, sizeof(on)) < 0) {
		ps_pwarning("Could not set SO_NOSIGPIPE on socket");
		return -1;
	}
#else
	if (!beenhere) {
		ps_info("Using SIG_IGN to block SIGPIPE\n");
		beenhere = 1;

		signal(SIGPIPE, SIG_IGN);
	}
#endif
#endif
	return 0;
}

/*
 * Add a (TCP) client
 */
static int
add_tcpclient(int sock, struct sockaddr_in *client)
{
	client_ctrl_t	*controlp;
	int		i;

	controlp = calloc(1, sizeof(client_ctrl_t));
	if (!controlp) {
		ps_error("add_tcpclient: Out of memory!\n");
		return -1;
	}

	if (sock >= sizeof(allsockets) / sizeof(allsockets[0])) {
		ps_warning("%s: Connection rejected, socket fd too large (%d)",
			   inet_ntoa(client->sin_addr), sock);
		return -1;
	}

	if (stopsigpipe(sock))
		return -1;

	if (pubsub_debug & PUBSUB_DEBUG_SRVCONNECT) {
		ps_info("%s: Client Connected: cport:%u sock:%d\n",
			inet_ntoa(client->sin_addr),
			(unsigned)ntohs(client->sin_port), sock);
	}

	i = sockbufsize;
	if (setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &i, sizeof(i)) < 0)
		ps_warning("Could not increase send socket buffer size to %d",
			   SOCKBUFSIZE);
    
	i = sockbufsize;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &i, sizeof(i)) < 0)
		ps_warning("Could not increase recv socket buffer size to %d",
			   SOCKBUFSIZE);
    
	controlp->ipaddr.s_addr = client->sin_addr.s_addr;
	controlp->index = sock;
	controlp->sock  = sock;
	queue_init(&controlp->subscriptions);
	allsockets[sock].client = controlp;

	/* Hook into list of clients */
	BLOCKSIGNALS();
	queue_enter(&AllClients, controlp, client_ctrl_t *, chain);
	UNBLOCKSIGNALS();
	pubsub_stats.total_clients++;
	pubsub_stats.current_clients++;
	return 0;
}

/*
 * Remove a (TCP) client.
 */
static void
remove_tcpclient(int sock)
{
	client_ctrl_t	*controlp = allsockets[sock].client;
	
	if (! controlp)
		return;
	
	if (pubsub_debug & PUBSUB_DEBUG_SRVCONNECT) {
		ps_info("%s: Client Disconnected: sock:%d\n",
		     inet_ntoa(controlp->ipaddr), sock);
	}

	/*
	 * Remove any subscriptions for this client
	 */
	BLOCKSIGNALS();
	while (! queue_empty(&controlp->subscriptions)) {
		subscr_ctrl_t	*subscription;

		queue_remove_first(&controlp->subscriptions,
				   subscription, subscr_ctrl_t *, chain);

		if (pubsub_debug &
		    (PUBSUB_DEBUG_SRVCONNECT|PUBSUB_DEBUG_SUBSCRIBE)) {
			ps_info("  Rem Subscription token:%d "
				"emitted:%u failed:%u\n",
				subscription->client_token,
				subscription->notes_emitted,
				subscription->notes_failed);
		}
		
		/* Fix this */
		if (! subscription->match_all) {
			destroyBindings(&subscription->variable_bindings);
			destroyProgram(&subscription->compiled_expression);
		}
		free(subscription->expression);
		free(subscription);
		pubsub_stats.current_subscriptions--;
	}
	queue_remove(&AllClients, controlp, client_ctrl_t *, chain);
	allsockets[sock].client	= (client_ctrl_t *) NULL;
	free(controlp);
	UNBLOCKSIGNALS();
	pubsub_stats.current_clients--;
}

/*
 * Add a subscription for a client.
 * Returns zero except on "non recoverable error" in which case no reply
 * will be sent to the client and the connection will be dropped.
 */
static int
add_subscription(client_ctrl_t *controlp, subscription_packet_t *packet)
{
	char		*expression = packet->expression;
	unsigned int	token = ntohl(packet->token);
	subscr_ctrl_t	*subscription;
	errorT		err;

	if (verbose || pubsub_debug & PUBSUB_DEBUG_SUBSCRIBE) {
		ps_info("%s: Add Subscription:%d token:%d '%s'\n",
		     inet_ntoa(controlp->ipaddr), controlp->sock,
		     token, expression);
	}

	/*
	 * Look for a duplicate Subscription and drop. Only return error
	 * if the expression is different.
	 */
	queue_iterate(&controlp->subscriptions,
		      subscription, subscr_ctrl_t *, chain) {

		if (subscription->client_token == token) {
			if (strcmp(expression, subscription->expression)) {
				ps_error("%s: Duplicate subscription token:% "
				      "expression mismatch: '%s'\n",
				      inet_ntoa(controlp->ipaddr), token,
				      subscription->expression);
				controlp->error = EINVAL;
			}
			else {
				ps_error("%s: Duplicate subscription token:%d\n",
				      inet_ntoa(controlp->ipaddr), token);
			}
			return 0;
		}
	}

	subscription = calloc(1, sizeof(subscr_ctrl_t));
	if (!subscription) {
		ps_error("add_subscription: Out of memory!");
		return -1;
	}

	subscription->client_token = token;

	subscription->expression = strdup(expression);
	if (!subscription->expression) {
		ps_error("add_subscription: Out of memory!");
		free(subscription);
		return -1;
	}

	/*
	 * Look for special match everything subscription
	 */
	if (strcmp(expression, "") == 0) {
		subscription->match_all = 1;
		goto done;
	}
	subscription->match_all = 0;

	err = createProgram(subscription->expression,
			    &subscription->compiled_expression);
	switch (err.type) {
	case NO_ERROR:
		break;
	case SYNTAX_ERROR:
		ps_error("Could not compile subscription: Syntax error\n");
		controlp->error = EINVAL;
		goto bad;
	case OUT_OF_MEMORY_ERROR:
		ps_error("Could not compile subscription: Out of memory\n");
		controlp->error = ENOMEM;
		goto bad;
	}
	err = createBindings(&subscription->compiled_expression,
			     &subscription->variable_bindings);
	if (err.type == OUT_OF_MEMORY_ERROR) {
		destroyProgram(&subscription->compiled_expression);
		ps_error("Could not allocate bindings: Out of memory\n");
		controlp->error = ENOMEM;
		goto bad;
	}

 done:
	BLOCKSIGNALS();
	queue_enter(&controlp->subscriptions,
		    subscription, subscr_ctrl_t *, chain);
	UNBLOCKSIGNALS();
	pubsub_stats.total_subscriptions++;
	pubsub_stats.current_subscriptions++;
	return 0;

 bad:
	free(subscription->expression);
	free(subscription);
	return 0;
}

/*
 * Remove a subscription for a client.
 */
static int
rem_subscription(client_ctrl_t *controlp, subscription_packet_t *packet)
{
	unsigned int	token = ntohl(packet->token);
	subscr_ctrl_t	*subscription;

	if (verbose || pubsub_debug & PUBSUB_DEBUG_SUBSCRIBE) {
		ps_info("%s: Rem Subscription:%d token:%d "
			"received:%u emitted:%u failed:%u (%d)\n",
			inet_ntoa(controlp->ipaddr), controlp->sock, token,
			controlp->notes_recieved,
			controlp->notes_emitted, controlp->notes_failed,
			controlp->consecutive_fails);
	}

	/*
	 * Need to find this subscription in the list.
	 */
	queue_iterate(&controlp->subscriptions,
		      subscription, subscr_ctrl_t *, chain) {

		if (subscription->client_token == token) {
			if (! subscription->match_all)
			    destroyProgram(&subscription->compiled_expression);
			BLOCKSIGNALS();
			if (! subscription->match_all)
			    destroyBindings(&subscription->variable_bindings);
			queue_remove(&controlp->subscriptions,
				     subscription, subscr_ctrl_t *, chain);
			UNBLOCKSIGNALS();
			pubsub_stats.current_subscriptions--;
			free(subscription->expression);
			free(subscription);
			return 0;
		}
	}
	/*
	 * Not really an error yet.
	 */
	ps_info("%s: rem_subscription:%d token:%d, No match!\n",
	     inet_ntoa(controlp->ipaddr), controlp->sock, token);
	return 0;
}

/* XXX PUBSUB_ERROR_MSGSIZE is somewhat of an arbitrary value */
static char	notify_debug_string[PUBSUB_ERROR_MSGSIZE];

static int
notify_dispatch_traverse_debug(void *arg, const char *name,
			       pubsub_type_t type, pubsub_value_t value,
			 pubsub_error_t *error)
{
	char		**bp = (char **) arg;
	char		buf[BUFSIZ], *tp = buf;
	unsigned char   *up;
	int		i, cc;

	switch (type) {
	case STRING_TYPE:
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=%s,", name, value.pv_string);
		break;
	case INT32_TYPE:
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=%"PRId32",", name, value.pv_int32);
		break;

	case INT64_TYPE:
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=%"PRId64",", name, value.pv_int64);
		break;
	case REAL64_TYPE:
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=%f,", name, value.pv_real64);
		break;

	case OPAQUE_TYPE:
		up = (unsigned char *) value.pv_opaque.data;
		for (i = 0; i < value.pv_opaque.length; i++, up++) {
			cc = snprintf(tp,
				      &buf[sizeof(buf)-1]-tp, "%02hhx", *up);
			tp += cc;
			if (tp >= &buf[sizeof(buf)-1])
				break;
		}		
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=%s,", name, buf);
		break;
		
	default:
		snprintf(*bp,
			 &notify_debug_string[sizeof(notify_debug_string)-1]-
			 *bp,
			 "%s=...,", name);
		break;
	}
	*bp = *bp + strlen(*bp);
	
	return 1;
}

static int
notify_dispatch_traverse(void *arg, const char *name,
			 pubsub_type_t type, pubsub_value_t value,
			 pubsub_error_t *error)
{
	note_bindings_t *bindings = (note_bindings_t *)arg;
	note_binding_t	*binding  = &(bindings->bindings[bindings->count++]);

	if (bindings->count >= MAX_NOTE_BINDINGS) {
		ps_error("Too many terms in notification; skipping\n");
		return 0;
	}
	
	binding->name = name;
	binding->binding.type = type;
	
	switch (type) {
	case INT32_TYPE:
		binding->binding.value.int32 = value.pv_int32;
		break;
	case INT64_TYPE:
		binding->binding.value.int64 = value.pv_int64;
		break;
	case REAL64_TYPE:
		binding->binding.value.real64 = value.pv_real64;
		break;
	case STRING_TYPE:
		binding->binding.value.string = value.pv_string;
		break;
	default:
		return 1;
	}
	return 1;
}

/*
 * Dispatch a notification to its subscribers.
 *
 * TODO: We should return status to the caller immediately and schedule
 * the notifications for main loop. But for now do not worry.
 */
static int
notify_dispatch(client_ctrl_t *controlp, notification_packet_t *packet)
{
	subscr_ctrl_t	      *subscription;
	client_ctrl_t         *client;
	pubsub_error_t	      error;
	struct timeval	      before, after;
	int		      failed = 0, emitted = 0;
	pubsub_notification_t *notification = (pubsub_notification_t *)
		(((char *) packet) + sizeof(pheader_t));
	int		      save_used = packet->used;
	note_bindings_t	      allbindings;

	pubsub_stats.notifications_received++;
	controlp->notes_recieved++;

	// Incoming is a generic packet, so this is not set. Need to set
	// it before we can traverse it.
	notification->used = ntohl(save_used);

	if (verbose) {
		char	*bp = notify_debug_string;
		
		pubsub_notification_traverse(notification,
					     notify_dispatch_traverse_debug,
					     &bp, &error);
					     
		ps_info("%s: Notification:%d '%s'\n", inet_ntoa(controlp->ipaddr),
		     controlp->sock, notify_debug_string);
	}
	if (measure)
		gettimeofday(&before, NULL);

	allbindings.count = 0;
	if (! pubsub_notification_traverse(notification,
					   notify_dispatch_traverse,
					   &allbindings, &error)) {
		ps_error("%s: Notification:%d Traversal failure\n",
			 inet_ntoa(controlp->ipaddr), controlp->sock);
		return 0;
	}

	/* Change to a notification packet */
	packet->header.type = htons(PUBSUB_PKT_NOTIFICATION);

	/*
         * Need to think about caching and duplicate subscriptions.
	 */
	queue_iterate(&AllClients, client, client_ctrl_t *, chain) {
		/*
		 * Why were we sending notifications back to the sender?
		 *
		 * Oooh, I know! I know! Because the Emulab event scheduler
		 * both sends and receives the SEQUENCE START notification
		 * and won't work without it! Strange but true...
		 */
		if (0 && client == controlp) {
			continue;
		}
		/*
		 * If this is our parent, there are no subscriptions, we just
		 * forward everything.
		 */
		if (client->isparent) {
			/*
			 * Don't loopback notification we got from the parent.
			 * The need for this special case is because of getting
			 * rid of the client == controlp check above!
			 */
			if (client == controlp)
				continue;

			if (verbose && !measure) {
				ps_info("%s: Notifying parent:%d\n",
					inet_ntoa(client->ipaddr), client->sock);
			}
			packet->used = save_used;
			if (notify_dispatch_forward(client,
						    (packet_t *) packet) < 0) {
				failed++;
				controlp->error = errno;
				/*
				 * Drop the parent connection, we will notice
				 * it in main loop above.
				 */
				pubsub_daemon_dropconnect(client->parent_handle);
			}
			else {
				emitted++;
			}
			continue;
		}
		queue_iterate(&client->subscriptions,
			      subscription, subscr_ctrl_t *, chain) {

			/*
			 * Skip all the goo if expression was ""
			 */
			if (subscription->match_all) {
				/*
				 * Do not send (loop) back to the sender. I
				 * added this to make the elvin gateway
				 * easier (okay, possible) to implement.
				 */
				if (client == controlp)
					continue;
				
				packet->used = save_used;
				goto matchall;
			}

			/* Bind all variables */
			bindAllVariables(&subscription->variable_bindings,
					 &allbindings);
			packet->used = save_used;
			if (evalBindings(&subscription->variable_bindings) ==
			    TERN_TRUE) {
			matchall:
				if (verbose && !measure) {
					ps_info("%s: "
					     "Notifying client:%d token:%d len:%d used:%d\n",
					     inet_ntoa(client->ipaddr),
					     client->sock,
						subscription->client_token,
						ntohl(packet->header.len),
						ntohl(packet->used));
				}
				packet->token =
					htonl(subscription->client_token);

				if (notify_dispatch_forward(client,
							    (packet_t *)packet)
				    < 0) {
					failed++;
					subscription->notes_failed++;
					controlp->error = errno;
				}
				else {
					emitted++;
					subscription->notes_emitted++;
				}
			}
		}
	}
	if (measure) {
		struct timeval diff;
		
		gettimeofday(&after, NULL);
		timersub(&after, &before, &diff);

		ps_info(" Processed after %d.%06d seconds, "
		     "used %u, emitted %d, failed %d\n",
		     diff.tv_sec, diff.tv_usec,
		     ntohl(save_used), emitted, failed);
	}
	/* Change back to a notify packet */
	packet->header.type = htons(PUBSUB_PKT_NOTIFY);
	
	return 0;
}

/*
 * Helper function for above.
 */
static int
notify_dispatch_forward(client_ctrl_t *client, packet_t *packet)
{
	/*
	 * Clients expect a PUBSUB_PKT_NOTIFICATION,
	 * but a pubsubd wants PUBSUB_PKT_NOTIFY
	 */
	if (!client->isparent) {
		packet->header.type = htons(PUBSUB_PKT_NOTIFICATION);
	}
	else {
		packet->header.type = htons(PUBSUB_PKT_NOTIFY);
	}
	if (ps_PacketSend(client->sock, (packet_t *)packet, NULL) < 0) {
		pubsub_stats.notifications_failed++;
		client->notes_failed++;
		client->consecutive_fails++;
		return -1;
	}
	pubsub_stats.notifications_emitted++;
	client->notes_emitted++;
	client->consecutive_fails = 0;
	return 0;
}

/*
 * Handle a request from a client. A non-zero return value is fatal to the
 * client, but that should not happen unless its a very unusual case. Instead
 * most errors are propogated back to the client in the reply packet so that
 * it can do something useful.
 */
static int
handle_request(int sock, maxpacket_t *packet)
{
	client_ctrl_t	*controlp = allsockets[sock].client;
	int		rval = 0;
	int		vers;

	if (! controlp)
		return 0;

	/* Clear the error indicator since callee sets it on error */
	controlp->error = 0;
	
	vers = ntohl(packet->header.vers);
	if (verbose) {
		ps_info("%s: Client Request:%d len:%d seq:%d type:%d vers:%d\n",
		     inet_ntoa(controlp->ipaddr), sock,
		     ntohl(packet->header.len),
		     ntohl(packet->header.seq),
		     ntohs(packet->header.type), vers);
	}

	/*
	 * The packet reader only makes context independent sanity checks.
	 * We make request-only sanity checks here, filtering out:
	 *   - certain packet types,
	 *   - reply packets (packets with no payloads)
	 * Note we make no reply for these and force disconnect of the client.
	 *
	 * One exception is that we will see NOTIFY replies from our parent
	 * when we forward a request. We quietly drop those without a
	 * disconnect.
	 */
	switch (ntohs(packet->header.type)) {
	case PUBSUB_PKT_NOTIFY:
		if (ntohl(packet->header.len) == sizeof(pheader_t)) {
			if (controlp->isparent) {
				ps_info("%s: Parent Reply:%d seq:%d type:%d ignored\n",
					inet_ntoa(controlp->ipaddr), sock,
					ntohl(packet->header.seq),
					ntohs(packet->header.type));
				return 0;
			}
			if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
				ps_error("%s: got request packet (type:%d) with no body\n",
					 inet_ntoa(controlp->ipaddr),
					 ntohs(packet->header.type));
			return -1;
		}
		break;
	case PUBSUB_PKT_SUBSCRIBE:
	case PUBSUB_PKT_UNSUBSCRIBE:
	case PUBSUB_PKT_NOTIFICATION:
		if (ntohl(packet->header.len) == sizeof(pheader_t)) {
			if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
				ps_error("%s: got request packet (type:%d) with no body\n",
					 inet_ntoa(controlp->ipaddr),
					 ntohs(packet->header.type));
			return -1;
		}
		break;

	case PUBSUB_PKT_CONNECT:
	case PUBSUB_PKT_DISCONNECT:
		/*
		 * XXX Are they part of the protocol we just don't implement,
		 * or is just sending a reply all that is required?
		 */
		break;

	case PUBSUB_PKT_PING:
		break;

	default:
		if (pubsub_debug & PUBSUB_DEBUG_BADPACKETS)
			ps_error("%s: invalid request type %d\n",
				 inet_ntoa(controlp->ipaddr),
				 ntohs(packet->header.type));
		return -1;
	}

	/*
	 * See if we drop the packet. 
	 */
	if (testmode) {
		if (random() % 4 == 0) {
			return 0;
		}
	}

	/* XXX here we may someday need to do packet munging based on vers */

	switch (ntohs(packet->header.type)) {
	case PUBSUB_PKT_SUBSCRIBE:
		rval = add_subscription(controlp,
					(subscription_packet_t *) packet);
		break;
	case PUBSUB_PKT_UNSUBSCRIBE:
		rval = rem_subscription(controlp,
					(subscription_packet_t *) packet);
		break;
	case PUBSUB_PKT_NOTIFICATION:
	case PUBSUB_PKT_NOTIFY:
		rval = notify_dispatch(controlp,
				       (notification_packet_t *) packet);
		break;
	case PUBSUB_PKT_PING:
	        if (!pingreply)
		    return 0;
		rval = 0;
		break;
	}
	/*
	 * If this is an upstream notification from a parent pubsubd, we do
	 * not send a reply since the parent does not want/expect one. Only
	 * real clients get replies.
	 */
	if (controlp->isparent)
		return rval;

	/*
	 * See if we drop the reply or delay the reply.
	 */
	if (testmode) {
		if (random() % 4 == 0) {
			return 0;
		}
		if (random() % 4 == 0) {
			sleep(random() % 10);
		}
	}

	/* XXX here we may someday need to do reply munging based on vers */

	/*
	 * All request packets get a reply, unless there is an unrecoverable
	 * error in the callee.
	 */
	if (! rval) {
		packet->header.len   = htonl(sizeof(packet->header));
		packet->header.error = htonl(controlp->error);
		packet->header.flags = 0;
		packet->header.vers  = htonl(vers);

		if (verbose) {
			ps_info("%s: Client Reply:%d len:%d seq:%d type:%d\n",
			     inet_ntoa(controlp->ipaddr), sock,
			     ntohl(packet->header.len),
			     ntohl(packet->header.seq),
			     ntohs(packet->header.type));
		}
		if (ps_PacketSend(controlp->sock, (packet_t *)packet, NULL) != 0) {
			ps_error("%s: Client Reply Failed "
			      "sock:%d len:%d seq:%d type:%d\n", 
			      inet_ntoa(controlp->ipaddr), sock,
			      ntohl(packet->header.len),
			      ntohl(packet->header.seq),
			      ntohs(packet->header.type));
			rval = -1;
		}
	}
	return rval;
}

/*
 * Connect to a parent pubsubd (as for clustering). 
 */
static int
ConnectToParent(char *server, int server_port,
		char *expression, pubsub_handle_t **handle)
{
	pubsub_subscription_t   *subscription;
	pubsub_error_t		pubsub_error;
	pubsub_handle_t		*newhandle = NULL;

	ps_info("Connecting to parent: %s:%d\n", server, server_port);

	if (pubsub_connect(server, server_port, &newhandle) < 0) {
		ps_error("Could not connect to pubsub parent!\n");
		return -1;
	}
	/*
	 * No callback function needed, it will never be used.
	 */
	ps_info("Adding parent subscription: %s\n", expression);
	if (!(subscription =
	      pubsub_add_subscription(newhandle, expression, NULL, NULL,
				      &pubsub_error))) {
		ps_error("Could not subscribe to pubsub parent!\n");
		return -1;
	}
	*handle = newhandle;
	return 0;
}

static void
Cleanup(void)
{
	client_ctrl_t         *client;

	queue_iterate(&AllClients, client, client_ctrl_t *, chain) {
		shutdown(client->sock, SHUT_RDWR);
		close(client->sock);
	}
	queue_init(&AllClients);
}
