#
# This version cannot be distribued to commercial sites.
#
%define debug_package %{nil}
%define elvinstr      -elvincompat
%define elvinconfig   --enable-elvin_compat
%define _prefix       /usr/local


Summary:	Publish Subscribe system for Emulab. With Elvin Compat
Name:		pubsub%{elvinstr}
Version:	0.98
Release:	1
License:	LGPL
Group:		Applications/System
Source:		%{name}-%{version}.tar.gz

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
URL:		http://www.emulab.net
BuildRequires:  libtool
BuildRequires:	make
BuildRequires:  byacc
Requires:	glibc
Provides:	pubsub
Prefix:		/usr

%description
Publish Subscribe system written for and used by the Emulab control plane.

%package shadow
Summary: For minimal installation on a Protogeni cooked mode node.
Group: Applications/System

%description shadow
Publish Subscribe system written for and used by the Emulab control plane.

%prep
%setup -n pubsub-%{version}

%build
%configure %{elvinconfig}
make client

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
make DESTDIR=%{buildroot} install-client

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_libexecdir}/*
%{_libdir}/*
%{_includedir}/pubsub/*
/etc/init.d/*
/etc/rc*.d/*
%if "%{_prefix}" == "/usr/local"
/etc/ld.so.conf.d/*
%endif

%files shadow
%defattr(-,root,root)
%{_libexecdir}/*
%{_libdir}/*.a
%{_includedir}/pubsub/*

%post
echo -n "Running ldconfig... "
/sbin/ldconfig %{_libdir}
echo "done"

