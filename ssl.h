/*
 * Copyright (c) 2000-2016 University of Utah and the Flux Group.
 * 
 * {{{EMULAB-LICENSE
 * 
 * This file is part of the Emulab network testbed software.
 * 
 * This file is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this file.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * }}}
 */
#include <sys/socket.h>
#include <openssl/x509.h>

/*
 * SSL prototypes and definitions.
 */

int		ps_server_sslinit(const char *certfile, const char *keyfile,
				  const char *trustedfile);
int		ps_client_sslinit(const char *certfile, const char *keyfile);
int		ps_sslaccept(int sock, struct sockaddr *, socklen_t *, int);
int		ps_sslconnect(int sock, const struct sockaddr *, socklen_t);
int		ps_sslwrite(int sock, const void *buf, size_t nbytes);
int		ps_sslread(int sock, void *buf, size_t nbytes);
int		ps_sslpending(int sock);
int		ps_sslclose(int sock);
int		ps_sslverify_client(int sock);
