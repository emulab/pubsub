/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2005-2018 University of Utah and the Flux Group.
 * All rights reserved.
 */
#ifndef	_PUBSUB_NETWORK_H_
#define	_PUBSUB_NETWORK_H_

#include <inttypes.h>
#include "queue.h"

/*
 * The structure of a request (response) packet. The len is field used to
 * calculate how much more data to read to get the entire packet. 
 */
typedef struct {
	uint32_t	len;		/* length of entire packet */
	uint16_t	type;		/* See below */
	uint16_t	flags;		/* See below */
	uint32_t	seq;	        /* unique sequence number */
	uint32_t	error;		/* Error code (0 == no error) */
	uint32_t	vers;		/* protocol version */
} __attribute__((__packed__)) pheader_t;

/* The complete packet */
typedef struct packet {
	pheader_t	header;
	char		data[0];
} __attribute__((__packed__)) packet_t;

/* must be at least PUBSUB_MAX_NOTIFICATION + 28 */
#define PUBSUB_MAX_PACKET		4096	/* XXX arbitrary */

/* A maximum size packet. */
typedef struct {
	pheader_t	header;
	int8_t		data[PUBSUB_MAX_PACKET-sizeof(pheader_t)];
} __attribute__((__packed__)) maxpacket_t;

/*
 * A callback for when a reply packet is received. Optional, used for
 * the few async calls we need.
 */
struct reply_packet;
typedef void (*packet_reply_callback_t)(pubsub_handle_t *handle,
					struct reply_packet *reply,
					void *arg);

/* A reply packet, which is never transmitted, but locally queued. */
typedef struct reply_packet {
	pheader_t		header;
	int			sequence;
	struct timeval		timesent;
	int			aborted;
	int			reply_received;
	packet_reply_callback_t callback;
	void			*callback_arg;
	packet_t		*pktp;
	queue_chain_t		chain;
} reply_packet_t;

/* A queued packet, which is never transmitted, but locally queued. */
typedef struct queued_packet {
	queue_chain_t	chain;
	maxpacket_t     packet;
} queued_packet_t;

/* Protocol Versions */
#define PUBSUB_WIRE_V1			1
#define PUBSUB_MIN_VERSION		PUBSUB_WIRE_V1
#define PUBSUB_CUR_VERSION		PUBSUB_WIRE_V1
#define PUBSUB_MAX_VERSION		PUBSUB_WIRE_V1

/* Packet Type */
#define PUBSUB_PKT_CONNECT		1
#define PUBSUB_PKT_DISCONNECT		2
#define PUBSUB_PKT_SUBSCRIBE		3
#define PUBSUB_PKT_UNSUBSCRIBE		4
#define PUBSUB_PKT_NOTIFY		5
#define PUBSUB_PKT_NOTIFICATION		6
#define PUBSUB_PKT_PING			7

/* Subpacket definitions */
typedef struct {
	pheader_t	header;
	uint32_t	token;		/* Unique token to identify it */
	char		expression[0];  /* subscription string. */
} __attribute__((__packed__)) subscription_packet_t;

/*
 * XXX This needs to match the version in clientapi.h ...
 */
typedef struct {
	pheader_t	header;
	uint32_t	token;		/* Token to match subscription */
	uint32_t	used;
	char		data[0];	/* key=value pairs */
} __attribute__((__packed__)) notification_packet_t;

/* Forward decls */
struct pubsub_error;

/* protos */
int     ps_ClientConnect(char *server, int portnum, int usessl, int retry,
			 int *sockp, struct pubsub_error *error);
int     ps_ClientAccept(int sock, int isssl, struct sockaddr *addr,
			socklen_t *addrlen);
int     ps_ClientDisconnect(int sock);
int	ps_ClientPort(int sock);
int	ps_PacketSend(int socket, packet_t *packet, packet_t *reply);
int	ps_PacketRead(int socket, packet_t *packet, int maxlen);
int	ps_PacketReadTimo(int socket, packet_t *packet, int maxlen, int timo);
int     ps_Read(int socket, void *buffer, size_t len);
int     ps_Write(int socket, void *buffer, size_t len);

#define SOCKBUFSIZE			(1024 * 64)
#define CLIENT_SEND_SOCKBUFSIZE		(1024 * 64)
#define CLIENT_RECV_SOCKBUFSIZE		(1024 * 64)
#endif
