/*
 * Copyright (c) 2000-2016 University of Utah and the Flux Group.
 * 
 * {{{EMULAB-LICENSE
 * 
 * This file is part of the Emulab network testbed software.
 * 
 * This file is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this file.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * }}}
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <openssl/engine.h>
#include <openssl/rsa.h>
#include <openssl/ssl.h>
#include <openssl/sha.h>
#include <openssl/err.h>
#include <openssl/x509v3.h>
#include <openssl/bio.h>
#include "ssl.h"
#include "log.h"

#if defined(linux) || defined(__CYGWIN__)
#define EAUTH	EPERM
#endif

/* Keep all this stuff private. */
static SSL_CTX		*ctx;
static void		sslerror(const char *fmt, ...);
static void		sslprint(const char *fmt, ...);

/* See comment in pubsubd.c */
#ifdef FD_SETSIZE
#define MAX_CLIENTS	FD_SETSIZE
#else
#define MAX_CLIENTS	1024
#endif
/*
 * Map TCP sockets onto the SSL objects.
 */
static struct {
	int		sock;
	SSL		*ssl;
} sslsockets[MAX_CLIENTS];

/*
 * Init our SSL context, as for pubsubd and clusterd.
 */
int
ps_server_sslinit(const char *certfile, const char *keyfile,
		  const char *trustedfile)
{
	SSL_library_init();
	SSL_load_error_strings();
	
	if (! (ctx = SSL_CTX_new(SSLv23_method()))) {
		sslerror("SSL_CTX_new");
		return 1;
	}

	/*
	 * Load our server key and certificate and then check it.
	 */
	if (! SSL_CTX_use_certificate_file(ctx, certfile, SSL_FILETYPE_PEM)) {
		sslerror("SSL_CTX_use_certificate_file");
		return 1;
	}
	if (! SSL_CTX_use_PrivateKey_file(ctx, keyfile, SSL_FILETYPE_PEM)) {
		sslerror("SSL_CTX_use_PrivateKey_file");
		return 1;
	}
	if (SSL_CTX_check_private_key(ctx) != 1) {
		sslerror("SSL_CTX_check_private_key");
		return 1;
	}

	/*
	 * Load the trusted CAs so that we can request client authentication.
	 * The CA list is sent to the client.
	 */
	if (! SSL_CTX_load_verify_locations(ctx, trustedfile, NULL)) {
		sslerror("SSL_CTX_load_verify_locations");
		return 1;
	}
	SSL_CTX_set_client_CA_list(ctx, SSL_load_client_CA_file(trustedfile));

	/*
	 * Make it so the client must provide authentication.
	 */
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER |
			   SSL_VERIFY_FAIL_IF_NO_PEER_CERT, 0);

	/*
	 * No session caching! Useless and eats up memory.
	 */
	SSL_CTX_set_session_cache_mode(ctx, SSL_SESS_CACHE_OFF);

	return 0;
}

int
ps_client_sslinit(const char *certfile, const char *keyfile)
{
	SSL_library_init();
	SSL_load_error_strings();
	
	if (! (ctx = SSL_CTX_new(SSLv23_method()))) {
		sslerror("SSL_CTX_new");
		return 1;
	}

	/*
	 * Load our certificate and key
	 */
	if (! SSL_CTX_use_certificate_file(ctx, certfile, SSL_FILETYPE_PEM)) {
		sslerror("SSL_CTX_use_certificate_file");
		return 1;
	}
	if (! SSL_CTX_use_PrivateKey_file(ctx, keyfile, SSL_FILETYPE_PEM)) {
		sslerror("SSL_CTX_use_PrivateKey_file");
		return 1;
	}
	
	if (SSL_CTX_check_private_key(ctx) != 1) {
		sslerror("SSL_CTX_check_private_key");
		return 1;
	}

	/*
	 * No session caching! Useless and eats up memory.
	 */
	SSL_CTX_set_session_cache_mode(ctx, SSL_SESS_CACHE_OFF);

	return 0;
}

/*
 * Handshake a new server connection from a client.
 */
int
ps_sslaccept(int sock, struct sockaddr *addr, socklen_t *addrlen, int ms)
{
	int		newsock, err, i, valid = 0, ext_count;
	SSL		*ssl;
	X509		*peer = NULL;

	if (! (ssl = SSL_new(ctx))) {
		ps_error("sslaccept: Could not create new SSL object\n");
		errno = EIO;
		return -1;
	}
	if ((newsock = accept(sock, addr, addrlen)) < 0) {
		err = errno;
		SSL_free(ssl);
		ERR_clear_error();
		close(sock);
		errno = err;
		return -1;
	}
	if (0) {
		ps_info("ps_sslaccept: %d\n", newsock);
	}

	/*
	 * Set timeout values to keep us from hanging due to a
	 * malfunctioning or malicious client.
	 */
	if (ms > 0) {
		struct timeval tv;

		tv.tv_sec = ms / 1000;
		tv.tv_usec = (ms % 1000) * 1000;
		if (setsockopt(newsock, SOL_SOCKET, SO_RCVTIMEO,
			       &tv, sizeof(tv)) < 0) {
			ps_error("setting SO_RCVTIMEO");
		}
	}
	sslsockets[newsock].sock = newsock;
	sslsockets[newsock].ssl  = ssl;

#ifdef SO_NOSIGPIPE
	/*
	 * No SIGPIPE on writes to closed socket, EPIPE instead.
	 */
	i = 1;
	if (setsockopt(newsock, SOL_SOCKET, SO_NOSIGPIPE, &i, sizeof(i)) < 0) {
		ps_warning("setting SO_NOSIGPIPE");
	}
#endif
	
	if (! SSL_set_fd(ssl, newsock)) {
		sslerror("SSL_set_fd");
		err = EIO;
		goto badauth;
	}
	if (SSL_accept(ssl) <= 0) {
		sslerror("SSL_accept");
		err = EAUTH;
		goto badauth;
	}
	/*
	 * Do the verification dance.
	 */
	if (SSL_get_verify_result(ssl) != X509_V_OK) {
		sslerror("Certificate did not verify!\n");
		err = EAUTH;
		goto badauth;
	}
	if (! (peer = SSL_get_peer_certificate(ssl))) {
		sslprint("No certificate presented!\n");
		err = EAUTH;
		goto badauth;
	}
	/*
	 * We are going to check the extensions for a proper root URN
	 */
	ext_count = X509_get_ext_count(peer);

	if (ext_count <= 0) {
		sslprint("No extensions in peer certificate");
		err = EAUTH;
		goto badauth;
	}
	for (i = 0; i < ext_count; i++) {
		ASN1_OBJECT	*obj;
		X509_EXTENSION	*ext;
		char		buf[BUFSIZ];

		bzero(buf, sizeof(buf));

		ext = X509_get_ext(peer, i);
		if (!ext) {
			sslprint("No extension value for extension %d", i);
			err = EAUTH;
			goto badauth;
		}
		obj = X509_EXTENSION_get_object(ext);
		if (!obj) {
			sslprint("No extension object for extension %d", i);
			err = EAUTH;
			goto badauth;
		}
		OBJ_obj2txt(buf, sizeof(buf), obj, 0);
		if (strcmp("X509v3 Subject Alternative Name", buf)) {
			BIO *bio = BIO_new(BIO_s_mem());
			X509V3_EXT_print(bio, ext, 0, 0);

			BUF_MEM *bptr;
			BIO_get_mem_ptr(bio, &bptr);
			BIO_set_close(bio, BIO_NOCLOSE);
			bptr->data[bptr->length] = (char) 0;

			if (strstr(bptr->data, "+authority+root")) {
				valid = 1;
				ps_info("Peer: %s\n", bptr->data);
				BIO_free_all(bio);
				break;
			}
			BIO_free_all(bio);
		}
	}
	if (!valid) {
		sslprint("Peer certificate does not verify");
		err = EAUTH;
		goto badauth;
	}
	X509_free(peer);
	return newsock;
 badauth:
	ps_error("sslaccept: error %d speaking to %s\n", err,
		 inet_ntoa(((struct sockaddr_in *)addr)->sin_addr));
	ps_sslclose(newsock);
	if (peer)
		X509_free(peer);	
	errno = err;
	return -1;
}

/*
 * Handshake a new client connection. 
 */
int
ps_sslconnect(int sock, const struct sockaddr *name, socklen_t namelen)
{
	SSL	*ssl;
#ifdef SO_NOSIGPIPE
	int     i;
#endif

	if (namelen && connect(sock, name, namelen) < 0) {
		return -1;
	}

#ifdef SO_NOSIGPIPE
	/*
	 * No SIGPIPE on writes to closed socket, EPIPE instead.
	 */
	i = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_NOSIGPIPE, &i, sizeof(i)) < 0) {
		ps_warning("setting SO_NOSIGPIPE");
	}
#endif
	
	if (! (ssl = SSL_new(ctx))) {
		sslerror("SSL_new");
		errno = EIO;
		return -1;
	}
	if (! SSL_set_fd(ssl, sock)) {
		sslerror("SSL_set_fd");
		errno = EIO;
		return -1;
	}
	if (SSL_connect(ssl) <= 0) {
		sslerror("SSL_connect");
		goto badauth;
	}
	if (0) {
		ps_info("ps_sslconnect: %d\n", sock);
	}
	sslsockets[sock].sock = sock;
	sslsockets[sock].ssl  = ssl;
	return 0;
 badauth:
	errno = EAUTH;
	return -1;
}

/*
 * Verify the certificate of the client.
 */
int
ps_sslverify_client(int sock)
{
	if (SSL_get_verify_result(sslsockets[sock].ssl) != X509_V_OK) {
		ps_error("sslverify: Certificate did not verify!\n");
		return -1;
	}
	/*
	 * TODO!
	 *
	 * We need to check the peer to make sure it is the boss node,
	 * not a user or some other certificate signed by the root cert.
	 */
	return 0;
}

/*
 * Write stuff out. According to docs, the write call will not
 * return until the entire amount is written. Not that it matters; the
 * caller is going to check and rewrite if short.
 */
int
ps_sslwrite(int sock, const void *buf, size_t nbytes)
{
	int	cc;

	if (0) {
		ps_info("ps_sslwrite: %d %ld\n", sock, nbytes);
	}
	errno = 0;
	cc = SSL_write(sslsockets[sock].ssl, buf, nbytes);

	if (cc <= 0) {
		if (cc < 0) {
			sslerror("SSL_write");
		}
		return cc;
	}
	return cc;
}

/*
 * Read stuff in.
 */
int
ps_sslread(int sock, void *buf, size_t nbytes)
{
	int	cc = 0;

	if (0) {
		ps_info("ps_sslread: %d %ld\n", sock, nbytes);
	}
	errno = 0;
	cc = SSL_read(sslsockets[sock].ssl, buf, nbytes);
	
	if (cc <= 0) {
		if (cc < 0) {
			sslerror("SSL_read");
		}
		return cc;
	}
	return cc;
}

/*
 * Have stuff to read? Caller will select if not.
 */
int
ps_sslpending(int sock)
{
	return SSL_pending(sslsockets[sock].ssl);
}

/*
 * Terminate the SSL part of a connection. Also close the sock.
 */
int
ps_sslclose(int sock)
{
	int	error;
	SSL	*ssl = sslsockets[sock].ssl;

	if (! (error = SSL_shutdown(ssl))) {
		shutdown(sock, SHUT_WR);
		error = SSL_shutdown(ssl);
	}
	if (error < 0) {
		sslerror("SSL_shutdown");
	}
	SSL_free(ssl);
	ERR_clear_error();
	close(sock);
	sslsockets[sock].ssl  = NULL;
	sslsockets[sock].sock = 0;
	return 0;
}

/*
 * Log an SSL error
 */
static void
sslerror(const char *fmt, ...)
{
	char	buf1[BUFSIZ], buf2[BUFSIZ];
	va_list args;

	va_start(args, fmt);
	vsnprintf(buf1, sizeof(buf1), fmt, args);
	va_end(args);

	ERR_error_string_n(ERR_get_error(), buf2, sizeof(buf2));

	ps_error("SSL Error: %s: %s\n", buf1, buf2);
}

/*
 * General error from inside this file.
 */
static void
sslprint(const char *fmt, ...)
{
	char	buf[BUFSIZ];
	va_list args;

	va_start(args, fmt);
	vsnprintf(buf, sizeof(buf), fmt, args);
	va_end(args);

	ps_error("%s\n", buf);
}
