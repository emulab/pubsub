/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2010, 2018 University of Utah and the Flux Group.
 * All rights reserved.
 */
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <assert.h>
#include <sys/types.h>
#include <setjmp.h>
#include "pubsub.h"
#include "log.h"

static int	debug = 0;
static int	bogus = 0;

char *usagestr = 
 "usage: testserver [options] [key1 value1 key2 value2 ...]\n"
 " -h              Display this message\n"
 " -d              Turn on debugging\n"
 " -s server       Specify a sync server to connect to\n"
 " -p portnum      Specify a port number to connect to\n"
 " -B		   Send a bogus packet to test server timeout\n"
 "\n";

void
usage()
{
	fprintf(stderr, "%s", usagestr);
	exit(64);
}

#ifdef BOGOCHECK
int
mytimeout(pubsub_handle_t *h, struct pubsub_timeout *to, void *d,
	  pubsub_error_t *e)
{
	printf("bogopacket timeout!\n");
	exit(0);
}
#endif

int
main(int argc, char **argv)
{
	int			ch;
	int			portnum = PUBSUB_SERVER_PORTNUM;
	char			*server = "localhost";
	pubsub_handle_t		*handle = NULL;
	pubsub_error_t		pubsub_error;
	pubsub_notification_t   *notification;
	
	while ((ch = getopt(argc, argv, "hVds:p:B")) != -1) {
		switch(ch) {
		case 'd':
			debug++;
			break;
		case 'p':
			if (sscanf(optarg, "%d", &portnum) == 0) {
				fprintf(stderr,
					"Error: -p value is not a number: "
					"%s\n",
					optarg);
				usage();
			}
			else if ((portnum <= 0) || (portnum >= 65536)) {
				fprintf(stderr,
					"Error: -p value is not between "
					"0 and 65536: %d\n",
					portnum);
				usage();
			}
			break;
		case 's':
			if (strlen(optarg) == 0) {
				fprintf(stderr, "Error: -s value is empty\n");
				usage();
			}
			else {
				server = optarg;
			}
			break;
		case 'h':
			fprintf(stderr, "%s", usagestr);
			exit(0);
			break;
		case 'B':
#ifdef BOGOCHECK
			bogus = 1;
			break;
#else
			fprintf(stderr,
				"Packet bogosification not supported\n");
			exit(1);
#endif
		default:
			usage();
			break;
		}
	}

	argv += optind;
	argc -= optind;

#if 0
	if (argc) {
		fprintf(stderr,
			"Error: Unrecognized command line arguments: %s ...\n",
			argv[0]);
		usage();
	}
#endif

	if (!server) {
		fprintf(stderr,
			"Error: Could not deduce the name of the server!\n");
		usage();
	}

	if (pubsub_connect(server, portnum, &handle) < 0) {
		ps_error("Could not connect to pubsub server!\n");
		exit(-1);
	}
	printf("Connected to server\n");
	sleep(5);

	notification = pubsub_notification_alloc(handle, &pubsub_error);
	if (notification == NULL) {
		ps_error("Could not allocate a notification!\n");
		exit(-1);
	}

	while (argc >= 2) {
		printf("adding %s=%s\n", argv[0], argv[1]);
		
		if (pubsub_notification_add_string(notification,
						   argv[0], argv[1],
						   &pubsub_error) == -1) {
			fprintf(stderr, "Error: %s\n", pubsub_error.msg);
			exit(-1);
		}
		
		argc -= 2;
		argv += 2;
	}

#ifdef BOGOCHECK
	if (bogus) {
		pubsub_error_t myerror;
		extern int bogosifypacket;
		extern int pubsub_debug;

		pubsub_notification_add_string(notification,
					       "BOGUS", "Yes indeed",
					       &myerror);
		pubsub_add_timeout(handle, NULL, 60*1000,
				   mytimeout, NULL, &myerror);
		printf("Sending bogus notification, will timeout in 60 sec\n");
		bogosifypacket = 1;
		pubsub_debug = 0xFF;
	}
#endif
	if (pubsub_notify(handle, notification, &pubsub_error) == -1) {
		fprintf(stderr, "Error: %s\n", pubsub_error.msg);
		exit(-1);
	}
	
	printf("Notification sent\n");
	sleep(1);
	pubsub_disconnect(handle);
	return 0;
}

