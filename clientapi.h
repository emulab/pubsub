/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2022 University of Utah and the Flux Group.
 * All rights reserved.
 */

#include <sys/time.h>
#include "queue.h"
#include "network.h"

/*
 * The client holds this handle to its library state.
 */
struct pubsub_handle {
	int		sock;
	int		isssl;          /* An SSL connection */
	char		*server;	/* For reconnect after failure */
	int		portnum;	/* For reconnect after failure*/
	int		max_retries;    /* Number of times to attempt */
	int		retry_count;    /* Number of times attempted so far */
	fd_set		sfds;		/* select FDs */
	int		maxfds;		/* also for select */
	pubsub_status_callback_t status_callback;
	void		*status_callback_arg;
	pubsub_status_t	this_status;
	pubsub_status_t	last_status;
	pubsub_status_t	next_status;
	int		idle_period;
	int		idle_waiting;
	int		reconnecting;
	int		failover;
	int		send_count;
	int		sub_count;	/* Number of subscriptions */
	pubsub_error_t  error;
	queue_head_t	reply_packets;	/* Linked list */
	queue_head_t	subscriptions;	/* Linked list */
	queue_head_t	iohandlers;	/* Linked list */
	queue_head_t	timeouts; /* One timeout callback only */
	struct thread_private *priv;	/* multithreaded version only */
};


/*
 * A notification object.
 *
 * XXX This needs to match the packet version in network.h ...
 */
struct pubsub_notification {
	unsigned int	token;		/* Match against subscription token */
	unsigned int	used;
	char		data[PUBSUB_MAX_NOTIFICATION];
					/* XXX temporary, waiting on Tim */
};

/*
 * A subscription is described by this structure in the client so that
 * it can issue an unsubscribe later.
 */
struct pubsub_subscription {
	unsigned int	token;		/* Uniquely identifies to the server */
	char		*expression;    /* For reconnect */
	void		*notify_arg;    /* Argument to notify callback */
	void		*subscribe_arg;
	int		busy;

	void (*notify_callback)(pubsub_handle_t *handle,
				struct pubsub_subscription *subscription,
				pubsub_notification_t *notification,
				void *data);
	void (*subscribe_callback)(pubsub_handle_t *handle, int result,
				struct pubsub_subscription *subscription,
				void *data, pubsub_error_t *error);

	reply_packet_t		reply_packet;
	queue_chain_t		chain;	
};

/*
 * A timeout to call a callback at a time in the future.
 */
struct pubsub_timeout {
	struct timeval		 timeout;	   /* useconds in the future */
	void			 *callback_arg;    /* Argument to callback */
	pubsub_timeout_callback_t callback;
	queue_chain_t		  chain;
};

/*
 * An I/O handler.
 */
struct pubsub_iohandler {
	int			    fd;
	int			    select_mask;     /* Read/Write/Error */
	void			   *callback_arg;    /* Argument to callback */
	pubsub_iohandler_callback_t callback;
	queue_chain_t		    chain;	     /* Linked list */
};

/* Global that can be selectively overridden */
extern int pubsub_send_sockbufsize;
extern int pubsub_recv_sockbufsize;

int
pubsub_notify_internal(pubsub_handle_t *handle, packet_t *pktp,
		       pubsub_error_t *myerror);
int
pubsub_daemon_dropconnect(pubsub_handle_t *handle);
int
pubsub_daemon_reconnect(pubsub_handle_t *handle, pubsub_error_t *myerror);
