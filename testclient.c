/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2017 University of Utah and the Flux Group.
 * All rights reserved.
 */

#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <unistd.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <assert.h>
#include <sys/types.h>
#include <setjmp.h>
#ifdef THREADED
#include <pthread.h>
#endif
#include "pubsub.h"
#include "log.h"
#ifdef WITHSSL
#include "ssl.h"
#endif

static int	debug = 0;

/* Forward decls */
static void     notify_callback(pubsub_handle_t *handle,
				pubsub_subscription_t *subscription,
				pubsub_notification_t *notification,
				void *data);

static int	timeout_callback(pubsub_handle_t *handle,
				 pubsub_timeout_t *timeout,
				 void *data,
				 pubsub_error_t *error);

static int	iohandler_callback(pubsub_handle_t *handle,
				   struct pubsub_iohandler *iohandler,
				   int fd, void *data, pubsub_error_t *error);

static void	status_callback(pubsub_handle_t *handle,
				pubsub_status_t status,
				void *data, pubsub_error_t *error);

static void	subscribe_callback(pubsub_handle_t *handle, int result, 
				   struct pubsub_subscription *subscription,
				   void *data, pubsub_error_t *error);

static void	unsubscribe_callback(pubsub_handle_t *handle, int result, 
				     struct pubsub_subscription *subscription,
				     void *data, pubsub_error_t *error);

pubsub_subscription_t   *async_subscription;
pubsub_handle_t		*handle = NULL;
pubsub_error_t		pubsub_error;
pubsub_subscription_t   *subscription;
pubsub_notification_t   *notification;
pubsub_timeout_t	*timeout;
pubsub_iohandler_t	*iohandler;
int callbacks;

char *usagestr = 
 "usage: testclient [options] -- [key val] ...\n"
 " -h              Display this message\n"
 " -d              Turn on debugging\n"
 " -s server       Specify a sync server to connect to\n"
 " -p portnum      Specify a port number to connect to\n"
#ifdef WITHSSL
 " -S              Make an SSL connection\n"
 " -C              Specify the certificate file for SSL\n"
 " -K              Specify the key file for SSL\n"
#endif
 "\n";

void
usage()
{
	fprintf(stderr, "%s", usagestr);
	exit(64);
}

int
main(int argc, char **argv)
{
	int			ch, rv, dossl = 0;
	int			portnum = PUBSUB_SERVER_PORTNUM;
	char			*server = "localhost";
#ifdef  WITHSSL
	char		        *certfile = NULL, *keyfile = NULL;
#endif
#ifndef THREADED
	int			keep_looping = 1;
#endif
	pubsub_status_t		status;
	
	while ((ch = getopt(argc, argv, "hVds:p:SK:C:")) != -1) {
		switch(ch) {
		case 'd':
			debug++;
			break;
		case 'p':
			if (sscanf(optarg, "%d", &portnum) == 0) {
				fprintf(stderr,
					"Error: -p value is not a number: "
					"%s\n",
					optarg);
				usage();
			}
			else if ((portnum <= 0) || (portnum >= 65536)) {
				fprintf(stderr,
					"Error: -p value is not between "
					"0 and 65536: %d\n",
					portnum);
				usage();
			}
			break;
		case 's':
			if (strlen(optarg) == 0) {
				fprintf(stderr, "Error: -s value is empty\n");
				usage();
			}
			else {
				server = optarg;
			}
			break;
		case 'S':
#ifndef WITHSSL
			fprintf(stderr, "Error: -S option, not built with SSL");
			usage();
#endif
			dossl++;
			break;
#ifdef	WITHSSL
		case 'C':
			certfile = optarg;
			break;
		case 'K':
			keyfile = optarg;
			break;
#endif
		case 'h':
			fprintf(stderr, "%s", usagestr);
			exit(0);
			break;
		default:
			usage();
			break;
		}
	}

	argv += optind;
	argc -= optind;
#ifdef __FreeBSD__
	srandomdev();
#else
	/* w00t! is this random or what? */
	srandom(getpid());
#endif

	if (!server) {
		fprintf(stderr,
			"Error: Could not deduce the name of the server!\n");
		usage();
	}

	if (pubsub_alloc_handle(&handle) != 0) {
		ps_error("Could not allocate pubsub handle!\n");
		exit(-1);
	}

	/* check initial status */
	status = pubsub_get_status(handle);
	if (status != PUBSUB_STATUS_CONNECTION_NOSTATUS) {
		ps_error("Invalid handle status %d post-alloc!\n", status);
	}
#ifdef	WITHSSL
	if (dossl) {
		if (certfile == NULL || keyfile == NULL) {
			ps_error("Must specify -K and -C options with -S!\n");
			exit(-1);
		}
		if (ps_client_sslinit(certfile, keyfile)) {
			exit(-1);
		}
		rv = pubsub_sslconnect(server, portnum, &handle);
	}
	else
#endif
		rv = pubsub_connect(server, portnum, &handle);
	if (rv) {
		ps_error("Could not connect to pubsub server!\n");
		exit(-1);
	}

	/* status check */
	status = pubsub_get_status(handle);
	if (status != PUBSUB_STATUS_CONNECTION_FOUND) {
		ps_error("Invalid handle status %d post-connect!\n", status);
	}

	/* Subscription test */
	if (! (subscription = 
	       pubsub_add_subscription(handle,
				       "foo == \"69\" && bar == \"96\"",
				       notify_callback, NULL,
				       &pubsub_error))) {
		fprintf(stderr, "add subscription failed\n");
		exit(-1);
	}
	printf("subscription added\n");

	notification = pubsub_notification_alloc(handle, &pubsub_error);
	if (notification == NULL) {
		fprintf(stderr, "Could not allocate a notification!\n");
		exit(-1);
	}
	while (argc >= 2) {
		printf("adding %s=%s to notification\n", argv[0], argv[1]);
		
		if (pubsub_notification_add_string(notification,
						   argv[0], argv[1],
						   &pubsub_error) == -1) {
			fprintf(stderr, "Error: %s\n", pubsub_error.msg);
			exit(-1);
		}
		argc -= 2;
		argv += 2;
	}
	printf("notification created\n");

	/* Timeout test */
	if (! (timeout =
	       pubsub_add_timeout(handle, NULL, 5000,
				  timeout_callback, NULL, &pubsub_error))) {
		fprintf(stderr, "add timeout failed\n");
		exit(-1);
	}
	printf("timeout added\n");
	
	/* I/O handler test */
	if (! (iohandler =
	       pubsub_add_iohandler(handle, NULL, fileno(stdin), 0,
				    iohandler_callback, NULL,
				    &pubsub_error))) {
		fprintf(stderr, "add iohandler failed\n");
		exit(-1);
	}
	printf("iohandler added\n");

	pubsub_set_status_callback(handle,
				   status_callback, NULL, &pubsub_error);
	printf("status handler added\n");

/*	pubsub_set_idle_period(handle, 5, &pubsub_error); */

	/* async Subscription test */
	if (pubsub_add_subscription_async(handle,
					  "fee == \"100\"",
					  notify_callback, NULL,
					  subscribe_callback, NULL,
					  &pubsub_error) < 0) {
		fprintf(stderr, "add async subscription failed\n");
		exit(-1);
	}
	printf("async subscription added\n");

#ifdef  THREADED
	while (1) {
		sleep(10);
		printf("Main program loop\n");
	}
#else
	pubsub_mainloop(handle, &keep_looping, &pubsub_error);
#endif
	
	if (pubsub_rem_subscription(handle, subscription, &pubsub_error) < 0) {
		fprintf(stderr, "rem subscription failed\n");
		exit(-1);
	}
	printf("subscription deleted\n");
	sleep(1);
	pubsub_disconnect(handle);
	return 0;
}

static int
print_notification(void *arg, const char *key,
		   pubsub_type_t type, pubsub_value_t value,
		   pubsub_error_t *error)
{
	switch (type) {
	case STRING_TYPE:
		printf("%s=%s\n", key, value.pv_string);
		break;
	case INT32_TYPE:
		printf("%s=%d\n", key, value.pv_int32);
		break;
	default:
		printf("%s=...\n", key);
		break;
	}
	
	return 1;
}

static void
notify_callback(pubsub_handle_t *handle,
		pubsub_subscription_t *subscription,
		pubsub_notification_t *notification,
		void *data)
{
	pubsub_error_t pe;
	
	pubsub_notification_traverse(notification, print_notification, NULL,
				     &pe);
	if (callbacks++ > 100) {
		printf("stopping after 100 callbacks\n");
		exit(0);
	}
}

static int
timeout_callback(pubsub_handle_t *handle,
		 pubsub_timeout_t *timeout, void *data,
		 pubsub_error_t *error)
{
	int	msdelay = random() % (5 * 1000);

	printf("Timeout callback: next in %d\n", msdelay);

	if (async_subscription) {
		if (pubsub_rem_subscription_async(handle,
						  async_subscription,
						  unsubscribe_callback, NULL,
						  error) < 0) {
			fprintf(stderr, "rem async subscription failed\n");
			pubsub_error_fprintf(stderr, error);
			exit(-1);
		}
		printf("async subscription deleted\n");
		async_subscription = NULL;
	}
	if (notification) {
		if (pubsub_notify(handle, notification, error) == -1) {
			fprintf(stderr, "failed to send notification\n");
			pubsub_error_fprintf(stderr, error);
		}
		else
			printf("notification sent\n");
	}
	if (! (timeout =
	       pubsub_add_timeout(handle, timeout, msdelay,
				  timeout_callback, NULL, error))) {
		fprintf(stderr, "reregister timeout failed\n");
		pubsub_error_fprintf(stderr, error);
		exit(-1);
	}
	if (callbacks++ > 100) {
		printf("stopping after 100 callbacks\n");
		exit(0);
	}
	return 0;
}

static int
iohandler_callback(pubsub_handle_t *handle,
		   struct pubsub_iohandler *iohandler,
		   int fd, void *data, pubsub_error_t *error)
{
	char	buf[BUFSIZ];
	
	printf("I/O handler callback\n");
	if (fgets(buf, sizeof(buf), stdin) == NULL)
		;
	printf("%s", buf);
	if (callbacks++ > 100) {
		printf("stopping after 100 callbacks\n");
		exit(0);
	}
	return 0;
}

static void
status_callback(pubsub_handle_t *handle, pubsub_status_t status,
		void *data, pubsub_error_t *error)
{
	printf("Status handler callback: %d\n", status);
	if (callbacks++ > 100) {
		printf("stopping after 100 callbacks\n");
		exit(0);
	}
}

static void
subscribe_callback(pubsub_handle_t *handle, int result,
		   struct pubsub_subscription *subscription,
		   void *data, pubsub_error_t *error)
{
	printf("subscription handler callback: %d\n", result);
	async_subscription = subscription;
	if (callbacks++ > 100) {
		printf("stopping after 100 callbacks\n");
		exit(0);
	}
}

static void
unsubscribe_callback(pubsub_handle_t *handle, int result,
		     struct pubsub_subscription *subscription,
		     void *data, pubsub_error_t *error)
{
	printf("delete subscription handler callback: %d\n", result);
	if (callbacks++ > 100) {
		printf("stopping after 100 callbacks\n");
		exit(0);
	}
}



