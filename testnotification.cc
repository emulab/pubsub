/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2007-2016 University of Utah and the Flux Group.
 * All rights reserved.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <assert.h>

extern "C" {
#include "pubsub.h"
#include "network.h"
#include "clientapi.h"
}

#include <map>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

struct traverse_arg {
	int retval;
	map<string, string> names;
};

int my_traverse(void *arg, const char *name,
		pubsub_type_t type, pubsub_value_t value,
		pubsub_error_t *error)
{
	struct traverse_arg *ta = (struct traverse_arg *)arg;
	
	assert(arg != NULL);

	if (ta->retval) {
		char buffer[1024];
		
		switch (type) {
		case INT32_TYPE:
			snprintf(buffer, sizeof(buffer),
				 "%"PRId32,
				 value.pv_int32);
			ta->names[string(name)] = string(buffer);
			break;
		case INT64_TYPE:
			snprintf(buffer, sizeof(buffer),
				 "%"PRId64,
				 value.pv_int64);
			ta->names[string(name)] = string(buffer);
			break;
		case REAL64_TYPE:
			snprintf(buffer, sizeof(buffer),
				 "%1.2f",
				 value.pv_real64);
			ta->names[string(name)] = string(buffer);
			break;
		case STRING_TYPE:
			ta->names[string(name)] = string(value.pv_string);
			break;
		default:
			break;
		}
	}

	return ta->retval;
}

static struct {
	const char *name;
	const char *value;
} test_strings[] = {
	{ "writer", "Liz Lemon" },
	{ "boss", "Jack Donaghey" },
	{ "saying", "Jack Welch, greatest leader since the pharaohs" },
	
	{ NULL, NULL }
};

int main(int argc, char *argv[])
{
	int prelen, lpc, retval = EXIT_SUCCESS;
	pubsub_notification_t *pn, *pn2;
	pubsub_error_t pe;
	unsigned int len;
	char *sval;
	
	pn = pubsub_notification_alloc(NULL, &pe);
	assert(pn != NULL);

	/* Try to remove from an empty notification. */
	assert(pubsub_notification_remove(pn, "msg", &pe) == -1);

	/* Add a string and then */
	assert(pubsub_notification_add_string(pn, "msg", "Hello, World!",
					      &pe) == 0);
	assert(pn->used > 0); // Make sure there is something there.

	/* ... remove it. */
	assert(pubsub_notification_remove(pn, "msg", &pe) == 0);
	assert(pubsub_notification_remove(pn, "msg", &pe) == -1);

	assert(pn->used == 0); // Make sure it's empty.

	/*
	 * Try removing the first string inserted and make sure the other one
	 * is still there.
	 */
	assert(pubsub_notification_add_string(pn, "msg", "Hello, World!",
					      &pe) == 0);
	prelen = pn->used;
	assert(pubsub_notification_add_string(pn, "msg2", "Goodbye, World!",
					      &pe) == 0);
	len = pn->used - prelen;
	
	assert(pubsub_notification_remove(pn, "msg", &pe) == 0);
	assert(pn->used == len);
	
	assert(pubsub_notification_get_string(pn, "msg2", &sval, &pe) == 0);
	assert(strcmp(sval, "Goodbye, World!") == 0);

	assert(pubsub_notification_get_string(pn, "msg", &sval, &pe) == -1);

	/* Add some more strings and then */
	for (lpc = 0; test_strings[lpc].name; lpc++) {
		assert(pubsub_notification_add_string(pn,
						      test_strings[lpc].name,
						      test_strings[lpc].value,
						      &pe) == 0);
	}

	/* ... remove them from the 'middle' of the note. */
	for (lpc = 0; test_strings[lpc].name; lpc++) {
		assert(pubsub_notification_get_string(pn,
						      test_strings[lpc].name,
						      &sval, &pe) == 0);
		
		assert(pubsub_notification_remove(pn, test_strings[lpc].name,
						  &pe) == 0);
	}

	/* Traverse the notification. */
	{
		struct traverse_arg ta;

		ta.retval = 1;
		assert(pubsub_notification_traverse(pn, my_traverse, &ta,
						    &pe) == 1);
		assert(ta.names.size() == 1);
		assert(ta.names["msg2"] == "Goodbye, World!");

		/* Try returning early. */
		ta.retval = 0;
		ta.names.clear();
		assert(pubsub_notification_traverse(pn, my_traverse, &ta,
						    &pe) == 0);
		assert(ta.names.size() == 0);
	}
	
	assert(pubsub_notification_add_int32(pn, "i", 1234, &pe) == 0);
	{
		int32_t i;
		
		assert(pubsub_notification_get_int32(pn, "i", &i, &pe) == 0);
		assert(i == 1234);
	}
	
	{
		struct traverse_arg ta;

		ta.retval = 1;
		assert(pubsub_notification_traverse(pn, my_traverse, &ta,
						    &pe) == 1);
		assert(ta.names.size() == 2);
		assert(ta.names["msg2"] == "Goodbye, World!");
		assert(ta.names["i"] == "1234");
	}

	assert((pn2 = pubsub_notification_clone(NULL, pn, &pe)) != NULL);

	assert(pubsub_notification_remove(pn, "msg2", &pe) == 0);
	
	assert(pubsub_notification_get_string(pn2, "msg2", &sval, &pe) == 0);
	assert(strcmp(sval, "Goodbye, World!") == 0);

	assert(pubsub_notification_add_opaque(pn, "bigmsg", "Hi",
					      1024 * 1024, &pe) == -1);
	
	assert(pubsub_notification_add_int64(pn, "j", 1234LL, &pe) == 0);
	{
		int64_t j;
		
		assert(pubsub_notification_get_int64(pn, "j", &j, &pe) == 0);
		assert(j == 1234);
	}
	
	assert(pubsub_notification_add_real64(pn, "pi", 3.14, &pe) == 0);
	{
		double pi;
		
		assert(pubsub_notification_get_real64(pn, "pi",
						      &pi, &pe) == 0);
		assert(pi == 3.14);
	}
	
	
	{
		struct traverse_arg ta;

		ta.retval = 1;
		assert(pubsub_notification_traverse(pn, my_traverse, &ta,
						    &pe) == 1);
		assert(ta.names["j"] == "1234");
		assert(ta.names["pi"] == "3.14");
	}
	
	return retval;
}
