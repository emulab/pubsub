/*
 * EMULAB-COPYRIGHT
 * Copyright (c) 2005-2018 University of Utah and the Flux Group.
 * All rights reserved.
 */

#ifndef _pubsub_h
#define _pubsub_h

#include <sys/types.h>
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Info */
#define PUBSUB_VERSION		 2
#define PUBSUB_SERVER_PORTNUM	 16505
#define PUBSUB_SERVER_SSLPORTNUM PUBSUB_SERVER_PORTNUM + 1
#define PUBSUB_ERROR_MSGSIZE	 1024

/* Maximum notification size */
/* If you increase this, you might also need to change PUBSUB_MAX_PACKET! */
#define PUBSUB_MAX_NOTIFICATION	2048	/* XXX hack */

/* This is the external API, for clients. */

/* Forward decls */
struct pubsub_subscription;
struct pubsub_timeout;
struct pubsub_iohandler;
struct thread_private;
struct packet;
struct pubsub_handle;
struct pubsub_notification;
struct pubsub_timeout;
struct pubsub_iohandler;

typedef enum
{
	/*
	 * This order is important. It is used for type promotion of numeric
	 * types.
	 */
	TERNARY_TYPE = 0,
	BEGIN_NUMERIC_TYPE = 1,
	INT32_TYPE = 2,
	INT64_TYPE = 3,
	REAL64_TYPE = 4,
	END_NUMERIC_TYPE = 5,
	STRING_TYPE = 6,
	OPAQUE_TYPE = 7,
} pubsub_type_t;

typedef union pubsub_value {
	int32_t pv_int32;
	int64_t pv_int64;
	double pv_real64;
	struct {
		char *data;
		int length;
	} pv_opaque;
	char *pv_string;
} pubsub_value_t;

/*
 * An error may hold some extra info at some point.
 */
typedef struct pubsub_error {
	int		error;
	char		*msg;
	char		buf[PUBSUB_ERROR_MSGSIZE]; /* Storage for a string */
} pubsub_error_t;

/*
 * These are defined internally; client programs hold opaque pointers.
 */
typedef struct pubsub_handle		pubsub_handle_t;
typedef struct pubsub_notification	pubsub_notification_t;
typedef struct pubsub_subscription	pubsub_subscription_t;
typedef struct pubsub_timeout		pubsub_timeout_t;
typedef struct pubsub_iohandler		pubsub_iohandler_t;

/*
 * A notification callback.
 */
typedef void (*pubsub_notify_callback_t)(pubsub_handle_t *handle,
				 struct pubsub_subscription *subscription,
				 pubsub_notification_t *notification,
				 void *data);

/*
 * Subscription callback, for async subscribe.
 */
typedef void (*pubsub_subscribe_callback_t)(pubsub_handle_t *handle,
				int result,	    
				struct pubsub_subscription *subscription,
				void *data, pubsub_error_t *error);

/*
 * Timeout callback.
 */
typedef int (*pubsub_timeout_callback_t)(pubsub_handle_t *handle,
					 struct pubsub_timeout *timeout,
					 void *data,
					 pubsub_error_t *error);

/*
 * I/O Handler callback
 */
typedef int (*pubsub_iohandler_callback_t)(pubsub_handle_t *handle,
					   struct pubsub_iohandler *iohandler,
					   int fd,
					   void *data,
					   pubsub_error_t *error);

/* API Prototypes */
int	pubsub_alloc_handle(pubsub_handle_t **);
int	pubsub_connect(char *server, int portnum, pubsub_handle_t **);
int	pubsub_sslconnect(char *server, int portnum, pubsub_handle_t **);
int	pubsub_disconnect(pubsub_handle_t *handle);
int	pubsub_error_get_code(pubsub_error_t *error);
int	pubsub_error_fprintf(FILE *fp, pubsub_error_t *error);
int     pubsub_error_error(pubsub_error_t *, int error, const char *fmt, ...);

pubsub_error_t *pubsub_error_alloc(pubsub_handle_t *handle);

pubsub_subscription_t *
pubsub_add_subscription(pubsub_handle_t *handle, char *expression,
			pubsub_notify_callback_t notify_cb, void *notify_arg,
			pubsub_error_t *error);

int
pubsub_add_subscription_async(pubsub_handle_t *handle, char *expression,
			      pubsub_notify_callback_t notify_cb,
			      void *notify_arg,
			      pubsub_subscribe_callback_t subscribe_cb,
			      void *subscribe_arg,
			      pubsub_error_t *myerror);
int
pubsub_rem_subscription(pubsub_handle_t *handle,
			pubsub_subscription_t *subscription,
			pubsub_error_t *error);

int
pubsub_rem_subscription_async(pubsub_handle_t *handle, 
			      pubsub_subscription_t *subscription,
			      pubsub_subscribe_callback_t subscribe_cb,
			      void *subscribe_arg,
			      pubsub_error_t *error);

int
pubsub_notify(pubsub_handle_t *handle,
			pubsub_notification_t *notification,
			pubsub_error_t *error);

pubsub_notification_t *
pubsub_notification_alloc(pubsub_handle_t *handle, pubsub_error_t *error);
void
pubsub_notification_clear(pubsub_notification_t *notification,
			  pubsub_error_t *myerror);
void
pubsub_notification_free(pubsub_handle_t *handle,
			 pubsub_notification_t *notification,
			 pubsub_error_t *error);
pubsub_notification_t *
pubsub_notification_clone(pubsub_handle_t *handle,
			  pubsub_notification_t *pn, pubsub_error_t *error);
int
pubsub_notification_add_opaque(pubsub_notification_t *notification,
			       const char *name, const char *value, int len,
			       pubsub_error_t *error);
int
pubsub_notification_remove(pubsub_notification_t *notification,
			   const char *name, pubsub_error_t *error);
int
pubsub_notification_add_string(pubsub_notification_t *notification,
			       const char *name, const char *value,
			       pubsub_error_t *error);
int
pubsub_notification_add_int32(pubsub_notification_t *notification,
			      const char *name, int32_t value,
			      pubsub_error_t *error);
int
pubsub_notification_add_int64(pubsub_notification_t *notification,
			      const char *name, int64_t value,
			      pubsub_error_t *error);
int
pubsub_notification_add_real64(pubsub_notification_t *notification,
			       const char *name, double value,
			       pubsub_error_t *error);
int
pubsub_notification_get_opaque(pubsub_notification_t *notification,
			       const char *name, char **val_out, int *len_out,
			       pubsub_error_t *error);
int
pubsub_notification_get_string(pubsub_notification_t *notification,
			       const char *name, char **val_out,
			       pubsub_error_t *error);
int
pubsub_notification_get_int32(pubsub_notification_t *notification,
			      const char *name, int32_t *value_out,
			      pubsub_error_t *error);
int
pubsub_notification_get_int64(pubsub_notification_t *notification,
			      const char *name, int64_t *value_out,
			      pubsub_error_t *error);
int
pubsub_notification_get_real64(pubsub_notification_t *notification,
			       const char *name, double *value_out,
			       pubsub_error_t *error);

typedef int (*pubsub_traverse_func_t)(void *arg,
				      const char *name,
				      pubsub_type_t type,
				      pubsub_value_t value,
				      pubsub_error_t *error);
int
pubsub_notification_traverse(pubsub_notification_t *notification,
			     pubsub_traverse_func_t func,
			     void *arg, pubsub_error_t *error);

int
pubsub_mainloop(pubsub_handle_t *handle,
			int *keep_looping, pubsub_error_t *error);
int
pubsub_dispatch(pubsub_handle_t *handle,
		int blocking, pubsub_error_t *error);

pubsub_timeout_t *
pubsub_add_timeout(pubsub_handle_t *handle,
		   void *context, unsigned int msdelay,
		   pubsub_timeout_callback_t callback,
		   void *callback_arg, pubsub_error_t *error);

int
pubsub_remove_timeout(pubsub_handle_t *handle,
		      pubsub_timeout_t *timeout, pubsub_error_t *error);


pubsub_iohandler_t *
pubsub_add_iohandler(pubsub_handle_t *handle,
		     void *context, int fd, int mask,
		     pubsub_iohandler_callback_t callback,
		     void *callback_arg, pubsub_error_t *error);

int
pubsub_remove_iohandler(pubsub_handle_t *handle,
			pubsub_iohandler_t *iohandler, pubsub_error_t *error);

int
pubsub_set_idle_period(pubsub_handle_t *handle,
		       int seconds, pubsub_error_t *error);

int
pubsub_set_failover(pubsub_handle_t *handle,
		    int failover, pubsub_error_t *error);

int
pubsub_set_connection_retries(pubsub_handle_t *handle,
			      int retries, pubsub_error_t *error);

int
pubsub_set_sockbufsizes(int sendsockbufsize, int recvsockbufsize);

/*
 * This is for the status event callback.
 */
typedef enum {
	PUBSUB_STATUS_CONNECTION_NOSTATUS = -1,
	/* Unable to connect to server after repeated attempts */
	PUBSUB_STATUS_CONNECTION_FAILED   = 0,
	/* Server shut down or went away */
	PUBSUB_STATUS_CONNECTION_LOST     = 1,
	/* Connect to server has been established */
	PUBSUB_STATUS_CONNECTION_FOUND    = 2,
	/* Internal failure in the client library */
	PUBSUB_STATUS_INTERNAL_FAILURE	  = 3,
} pubsub_status_t;

/*
 * call back.
 */
typedef void (*pubsub_status_callback_t)(pubsub_handle_t *handle,
					 pubsub_status_t status,
					 void *data,
					 pubsub_error_t *error);

int
pubsub_set_status_callback(pubsub_handle_t *handle, 
			   pubsub_status_callback_t status_cb,
			   void *arg,
			   pubsub_error_t *myerror);

/* Polling interface to get current status */
pubsub_status_t
pubsub_get_status(pubsub_handle_t *handle);

/* Global flag */
extern int pubsub_debug;
#define PUBSUB_DEBUG_CONNECT	0x01
#define PUBSUB_DEBUG_RECONNECT	0x02
#define PUBSUB_DEBUG_DETAIL	0x04
#define PUBSUB_DEBUG_CALLBACK	0x08
#define PUBSUB_DEBUG_DISPATCHER	0x10
#define PUBSUB_DEBUG_READTIMO	0x20
#define PUBSUB_DEBUG_WAYDETAIL	0x40
#define PUBSUB_DEBUG_OVERDUE	0x80
#define PUBSUB_DEBUG_BADPACKETS	0x100
#define PUBSUB_DEBUG_CLUSTERD	0x200
#define PUBSUB_DEBUG_SRVCONNECT	0x400
#define PUBSUB_DEBUG_SUBSCRIBE	0x800

#ifdef __cplusplus
}
#endif

#endif
